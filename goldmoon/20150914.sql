//112.223.37.244
/* Drop Tables */

DROP TABLE dealings_info CASCADE CONSTRAINTS;
DROP TABLE account_info CASCADE CONSTRAINTS;
DROP TABLE banking_info CASCADE CONSTRAINTS;
DROP TABLE paying CASCADE CONSTRAINTS;
DROP TABLE tab_comment CASCADE CONSTRAINTS;
DROP TABLE tab_board CASCADE CONSTRAINTS;
DROP TABLE tab_board_info CASCADE CONSTRAINTS;
DROP TABLE tab_user CASCADE CONSTRAINTS;
DROP TABLE user_level CASCADE CONSTRAINTS;



/* Drop Sequences */

DROP SEQUENCE ACCOUNT_NO_KEY;
DROP SEQUENCE BANKING_NO_KEY;
DROP SEQUENCE BOARD_INFO_NO_KEY;
DROP SEQUENCE BOARD_NO_KEY;
DROP SEQUENCE COMMENT_NO_KEY;
DROP SEQUENCE DEALINGS_NO_KEY;
DROP SEQUENCE LEVEL_NO_KEY;
DROP SEQUENCE PAYING_NO_KEY;




/* Create Sequences */

CREATE SEQUENCE ACCOUNT_NO_KEY INCREMENT BY 1 MINVALUE 50400101 START WITH 50400101 NOCACHE NOCYCLE;
CREATE SEQUENCE BANKING_NO_KEY INCREMENT BY 1 MINVALUE 0 START WITH 0 NOCACHE NOCYCLE;
CREATE SEQUENCE BOARD_INFO_NO_KEY INCREMENT BY 1 MINVALUE 0 START WITH 0 NOCACHE NOCYCLE;
CREATE SEQUENCE BOARD_NO_KEY INCREMENT BY 1 MINVALUE 0 START WITH 0 NOCACHE NOCYCLE;
CREATE SEQUENCE COMMENT_NO_KEY INCREMENT BY 1 MINVALUE 0 START WITH 0 NOCACHE NOCYCLE;
CREATE SEQUENCE DEALINGS_NO_KEY INCREMENT BY 1 MINVALUE 0 START WITH 0 NOCACHE NOCYCLE;
CREATE SEQUENCE LEVEL_NO_KEY INCREMENT BY 1 MINVALUE 0 START WITH 0 NOCACHE NOCYCLE;
CREATE SEQUENCE PAYING_NO_KEY INCREMENT BY 1 MINVALUE 0 START WITH 0 NOCACHE NOCYCLE;



/* Create Tables */

CREATE TABLE account_info
(
	account_no number NOT NULL,
	account_pw varchar2(100) NOT NULL,
	account_money number NOT NULL,
	open_date date NOT NULL,
	user_id varchar2(20) NOT NULL,
	PRIMARY KEY (account_no)
);


CREATE TABLE banking_info
(
	banking_no number NOT NULL,
	banking_name varchar2(30) NOT NULL,
	api_key varchar2(20) NOT NULL,
	PRIMARY KEY (banking_no)
);


CREATE TABLE dealings_info
(
	dealings_no number NOT NULL,
	deal_date date NOT NULL,
	money number NOT NULL,
	content varchar2(20) NOT NULL,
	remain_money number NOT NULL,
	customer varchar2(10) NOT NULL,
	cust_account_no number,
	banking_no number NOT NULL,
	account_no number NOT NULL,
	paying_no number NOT NULL,
	PRIMARY KEY (dealings_no)
);


CREATE TABLE paying
(
	paying_no number NOT NULL,
	deposit_withdrawal_info varchar2(30) NOT NULL,
	PRIMARY KEY (paying_no)
);


CREATE TABLE tab_board
(
	board_no number NOT NULL,
	title varchar2(100) NOT NULL,
	regdate date DEFAULT sysdate NOT NULL,
	content CLOB NOT NULL,
	readcount number DEFAULT 0 NOT NULL,
	ref_no number NOT NULL,
	re_step number NOT NULL,
	re_level number NOT NULL,
	state number DEFAULT 0 NOT NULL,
	user_ip varchar2(15),
	user_id varchar2(20) NOT NULL,
	bno number NOT NULL,
	PRIMARY KEY (board_no)
);


CREATE TABLE tab_board_info
(
	bno number NOT NULL,
	bname varchar2(100) NOT NULL,
	PRIMARY KEY (bno)
);


CREATE TABLE tab_comment
(
	cno number NOT NULL,
	board_no number NOT NULL,
	bno number NOT NULL,
	user_id varchar2(20) NOT NULL,
	bcomment varchar2(300) NOT NULL,
	regdate date DEFAULT sysdate NOT NULL,
	PRIMARY KEY (cno)
);


CREATE TABLE tab_user
(
	user_id varchar2(20) NOT NULL,
	user_name varchar2(20) NOT NULL,
	user_pw varchar2(100) NOT NULL,
	user_pwd_hint varchar2(200) NOT NULL,
	user_pwd_answer varchar2(100) NOT NULL,
	user_gender number(1) DEFAULT 1 NOT NULL,
	user_email varchar2(60) NOT NULL,
	user_phone varchar2(16) NOT NULL,
	user_zipcode varchar2(7) NOT NULL,
	user_addr1 varchar2(100) NOT NULL,
	user_addr2 varchar2(50) NOT NULL,
	user_regdate date DEFAULT sysdate NOT NULL,
	level_no number DEFAULT 1 NOT NULL,
	PRIMARY KEY (user_id)
);


CREATE TABLE user_level
(
	level_no number NOT NULL,
	lname varchar2(50),
	PRIMARY KEY (level_no)
);

/* Create Foreign Keys */

ALTER TABLE dealings_info
	ADD FOREIGN KEY (account_no)
	REFERENCES account_info (account_no)
;


ALTER TABLE dealings_info
	ADD FOREIGN KEY (banking_no)
	REFERENCES banking_info (banking_no)
;


ALTER TABLE dealings_info
	ADD FOREIGN KEY (paying_no)
	REFERENCES paying (paying_no)
;


ALTER TABLE tab_comment
	ADD FOREIGN KEY (board_no)
	REFERENCES tab_board (board_no)
;


ALTER TABLE tab_board
	ADD FOREIGN KEY (bno)
	REFERENCES tab_board_info (bno)
;


ALTER TABLE tab_comment
	ADD FOREIGN KEY (bno)
	REFERENCES tab_board_info (bno)
;


ALTER TABLE account_info
	ADD FOREIGN KEY (user_id)
	REFERENCES tab_user (user_id)
;


ALTER TABLE tab_board
	ADD FOREIGN KEY (user_id)
	REFERENCES tab_user (user_id)
;


ALTER TABLE tab_comment
	ADD FOREIGN KEY (user_id)
	REFERENCES tab_user (user_id)
;


ALTER TABLE tab_user
	ADD FOREIGN KEY (level_no)
	REFERENCES user_level (level_no)
;


INSERT INTO user_level (level_no, lname) VALUES (LEVEL_NO_KEY.NEXTVAL, 'ROLE_USER');
INSERT INTO user_level (level_no, lname) VALUES (LEVEL_NO_KEY.NEXTVAL, 'ROLE_RESTRICTED');
INSERT INTO user_level (level_no, lname) VALUES (LEVEL_NO_KEY.NEXTVAL, 'IS_AUTHENTICATED_FULLY');
INSERT INTO user_level (level_no, lname) VALUES (LEVEL_NO_KEY.NEXTVAL, 'IS_AUTHENTICATED_REMEMBERED	');
INSERT INTO user_level (level_no, lname) VALUES (LEVEL_NO_KEY.NEXTVAL, 'IS_AUTHENTICATED_ANONYMOUSLY');
INSERT INTO user_level (level_no, lname) VALUES (LEVEL_NO_KEY.NEXTVAL, 'ROLE_ADMIN');


INSERT INTO TAB_USER (user_id, user_name, user_pw, user_pwd_hint, user_pwd_answer, 
 user_email, user_phone, user_zipcode, user_addr1, user_addr2, level_no ) VALUES
( 'admin', '관리자', '21232f297a57a5a743894a0e4a801fc3', 'admin', 'admin', 'admin@admin.com', '010-1111-1111', '123-234', 
 'admin', 'admin', 6);
 
INSERT INTO TAB_BOARD_INFO (bno, bname) VALUES (BOARD_INFO_NO_KEY.NEXTVAL, '공지사항');
INSERT INTO TAB_BOARD_INFO (bno, bname) VALUES (BOARD_INFO_NO_KEY.NEXTVAL, '고객의소리');

INSERT INTO PAYING (paying_no, deposit_withdrawal_info) VALUES (PAYING_NO_KEY.NEXTVAL, '입금');
INSERT INTO PAYING (paying_no, deposit_withdrawal_info) VALUES (PAYING_NO_KEY.NEXTVAL, '출금');
INSERT INTO PAYING (paying_no, deposit_withdrawal_info) VALUES (PAYING_NO_KEY.NEXTVAL, '자사이체(입금)');
INSERT INTO PAYING (paying_no, deposit_withdrawal_info) VALUES (PAYING_NO_KEY.NEXTVAL, '자사이체(출금)');
INSERT INTO PAYING (paying_no, deposit_withdrawal_info) VALUES (PAYING_NO_KEY.NEXTVAL, '타사이체(입금)');
INSERT INTO PAYING (paying_no, deposit_withdrawal_info) VALUES (PAYING_NO_KEY.NEXTVAL, '타사이체(출금)');


INSERT INTO BANKING_INFO(banking_no, banking_name, api_key) VALUES (BANKING_NO_KEY.NEXTVAL, '골드문뱅크', 'BS230A16B');
INSERT INTO BANKING_INFO(banking_no, banking_name, api_key) VALUES (BANKING_NO_KEY.NEXTVAL, 'HBI은행', 'A235BS12Q');



SELECT dealings_no, deal_date, money, content, remain_money, customer, 
cust_account_no, banking_no, account_no, paying_no FROM dealings_info


SELECT account_no, account_pw, account_money, open_date, user_id FROM account_info

SELECT user_id, user_name, user_pwd_hint, user_pwd_answer, user_gender, user_email, user_phone, user_zipcode,
	user_addr1, user_addr2, user_regdate, level_no FROM tab_user

	
	
SELECT u.user_id, u.user_name 
FROM tab_user u JOIN account_info a 
ON u.user_id = a.user_id 
WHERE a.account_no = 50400107;	


-- 전체 계좌에게 이자 부여하기 위한 프로시저
CREATE PROCEDURE INTEREST 
	(VRATION NUMBER) 
IS 
	CURSOR AI1 IS
	SELECT ACCOUNT_NO, ACCOUNT_MONEY  
	FROM ACCOUNT_INFO;
	
	VMONEY NUMBER := 0;
	VREMAINMONEY NUMBER := 0;
BEGIN
	

	FOR AI2 IN AI1 LOOP
	
		VREMAINMONEY := AI2.ACCOUNT_MONEY*(1+VRATION/100);
		VMONEY := VREMAINMONEY-AI2.ACCOUNT_MONEY;
	
		UPDATE ACCOUNT_INFO  
		SET ACCOUNT_MONEY = VREMAINMONEY 
		WHERE ACCOUNT_NO = AI2.ACCOUNT_NO;

		INSERT INTO dealings_info 
		(dealings_no, deal_date, paying_no, money, content, 
		remain_money, customer, account_no, banking_no, cust_account_no ) 
		VALUES (DEALINGS_NO_KEY.NEXTVAL, SYSDATE, 1, VMONEY, 
			'이자정산', VREMAINMONEY, '골드문뱅크', AI2.ACCOUNT_NO, 
			1, 0 );
			
	END LOOP;
	
	
	

END;
/

-- 거래 횟수를 구하기 위한 함수
CREATE FUNCTION ACCOUNTCOUNT (VACCOUNT_NO ACCOUNT_INFO.ACCOUNT_NO%TYPE)
	RETURN NUMBER
IS
	VCOUNT NUMBER(10) := 0;
BEGIN
	
	SELECT COUNT(dealings_no) 
	INTO VCOUNT 
	FROM dealings_info 
	WHERE account_no = VACCOUNT_NO;

	RETURN VCOUNT;
END;
/
