package com.bank.goldmoon.vo;

public class TransferInfoVO {
	private String userId,content,youcontent,accountPw,userName;
	private int accountNo,bankingNo,custAccountNo,money;
	public TransferInfoVO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public TransferInfoVO(String userId, String content, String youcontent, String accountPw, String userName,
			int accountNo, int bankingNo, int custAccountNo, int money) {
		super();
		this.userId = userId;
		this.content = content;
		this.youcontent = youcontent;
		this.accountPw = accountPw;
		this.userName = userName;
		this.accountNo = accountNo;
		this.bankingNo = bankingNo;
		this.custAccountNo = custAccountNo;
		this.money = money;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getYoucontent() {
		return youcontent;
	}
	public void setYoucontent(String youcontent) {
		this.youcontent = youcontent;
	}
	public String getAccountPw() {
		return accountPw;
	}
	public void setAccountPw(String accountPw) {
		this.accountPw = accountPw;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public int getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(int accountNo) {
		this.accountNo = accountNo;
	}
	public int getBankingNo() {
		return bankingNo;
	}
	public void setBankingNo(int bankingNo) {
		this.bankingNo = bankingNo;
	}
	public int getCustAccountNo() {
		return custAccountNo;
	}
	public void setCustAccountNo(int custAccountNo) {
		this.custAccountNo = custAccountNo;
	}
	public int getMoney() {
		return money;
	}
	public void setMoney(int money) {
		this.money = money;
	}
	@Override
	public String toString() {
		return "TransferInfoVO [userId=" + userId + ", content=" + content + ", youcontent=" + youcontent
				+ ", accountPw=" + accountPw + ", userName=" + userName + ", accountNo=" + accountNo + ", bankingNo="
				+ bankingNo + ", custAccountNo=" + custAccountNo + ", money=" + money + "]";
	}
	
}
