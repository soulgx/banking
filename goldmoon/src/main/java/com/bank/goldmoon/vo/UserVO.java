package com.bank.goldmoon.vo;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

@XmlRootElement(name="test")
public class UserVO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@NotNull
	@Size(min=5, max=10)
	private String userId;
	
	@NotEmpty
	private String userName;
	
	@NotNull
	@Size(min=5, max=10)
	private String userPw;
	
	@NotNull
	private String userPwdHint;
	
	@NotNull
	private String userPwdAnswer;
	
	private int userGender;
	
	@NotNull
	@Email
	private String userEmail;
	

	private String userPhone;
	

	private String userZipcode;
	
	@NotNull
	private String userAddr1;
	
	@NotNull
	private String userAddr2;
	

	private Timestamp userRegDate;
	

	private int userLevel;
	
	
	public UserVO() {
	}
	
	
	
	public UserVO(String userId, String userName, String userPw, String userPwdHint, String userPwdAnswer,
			int userGender, String userEmail, String userPhone, String userZipcode, String userAddr1, String userAddr2,
			Timestamp userRegDate, int userLevel) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.userPw = userPw;
		this.userPwdHint = userPwdHint;
		this.userPwdAnswer = userPwdAnswer;
		this.userGender = userGender;
		this.userEmail = userEmail;
		this.userPhone = userPhone;
		this.userZipcode = userZipcode;
		this.userAddr1 = userAddr1;
		this.userAddr2 = userAddr2;
		this.userRegDate = userRegDate;
		this.userLevel = userLevel;
	}



	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserPw() {
		return userPw;
	}
	public void setUserPw(String userPw) {
		this.userPw = userPw;
	}
	public String getUserPwdHint() {
		return userPwdHint;
	}
	public void setUserPwdHint(String userPwdHint) {
		this.userPwdHint = userPwdHint;
	}
	public String getUserPwdAnswer() {
		return userPwdAnswer;
	}
	public void setUserPwdAnswer(String userPwdAnswer) {
		this.userPwdAnswer = userPwdAnswer;
	}
	public int getUserGender() {
		return userGender;
	}
	public void setUserGender(int userGender) {
		this.userGender = userGender;
	}
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public String getUserPhone() {
		return userPhone;
	}
	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}
	public String getUserZipcode() {
		return userZipcode;
	}
	public void setUserZipcode(String userZipcode) {
		this.userZipcode = userZipcode;
	}
	public String getUserAddr1() {
		return userAddr1;
	}
	public void setUserAddr1(String userAddr1) {
		this.userAddr1 = userAddr1;
	}
	public String getUserAddr2() {
		return userAddr2;
	}
	public void setUserAddr2(String userAddr2) {
		this.userAddr2 = userAddr2;
	}
	public Timestamp getUserRegDate() {
		return userRegDate;
	}
	public void setUserRegDate(Timestamp userRegDate) {
		this.userRegDate = userRegDate;
	}
	public int getUserLevel() {
		return userLevel;
	}
	public void setUserLevel(int userLevel) {
		this.userLevel = userLevel;
	}
	
	@Override
	public String toString() {
		return "UserVo [userId=" + userId + ", userName=" + userName + ", userPw=" + userPw + ", userPwdHint="
				+ userPwdHint + ", userPwdAnswer=" + userPwdAnswer + ", userGender=" + userGender + ", userEmail="
				+ userEmail + ", userPhone=" + userPhone + ", userZipcode=" + userZipcode + ", userAddr1=" + userAddr1
				+ ", userAddr2=" + userAddr2 + ", userRegDate=" + userRegDate + ", userLevel=" + userLevel + "]";
	}	
	
}
