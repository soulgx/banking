package com.bank.goldmoon.vo;

public class BankingInfoVO {
	
	private int bankingNo;
	private String bankingName;
	private String apiKey;
	public int getBankingNo() {
		return bankingNo;
	}
	public void setBankingNo(int bankingNo) {
		this.bankingNo = bankingNo;
	}
	public String getBankingName() {
		return bankingName;
	}
	public void setBankingName(String bankingName) {
		this.bankingName = bankingName;
	}
	public String getApiKey() {
		return apiKey;
	}
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
	
	@Override
	public String toString() {
		return "BankingInfoVo [bankingNo=" + bankingNo + ", bankingName=" + bankingName + ", apiKey=" + apiKey + "]";
	}
	
	

}
