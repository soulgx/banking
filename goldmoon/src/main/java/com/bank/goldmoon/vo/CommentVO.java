package com.bank.goldmoon.vo;

public class CommentVO {
	
	private int cno;		
	private int	boardNo;		
	private int bno;		
	private String userId;
	private String bcomment;
	private String regDate;
	private String userName;
	
	public CommentVO() {}

	public CommentVO(int cno, int boardNo, int bno, String userId, String bcomment, String regDate,
			String userName) {
		this.cno = cno;
		this.boardNo = boardNo;
		this.bno = bno;
		this.userId = userId;
		this.bcomment = bcomment;
		this.regDate = regDate;
		this.userName = userName;
	}

	public int getCno() {
		return cno;
	}

	public void setCno(int cno) {
		this.cno = cno;
	}

	public int getBoardNo() {
		return boardNo;
	}

	public void setBoardNo(int boardNo) {
		this.boardNo = boardNo;
	}

	public int getBno() {
		return bno;
	}

	public void setBno(int bno) {
		this.bno = bno;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getBcomment() {
		return bcomment;
	}

	public void setBcomment(String bcomment) {
		this.bcomment = bcomment;
	}

	public String getRegDate() {
		return regDate;
	}

	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public String toString() {
		return "BoardCommentVO [cno=" + cno + ", boardNo=" + boardNo + ", bno=" + bno + ", userId=" + userId
				+ ", bcomment=" + bcomment + ", regDate=" + regDate + ", userName=" + userName + "]";
	}
	
	
}