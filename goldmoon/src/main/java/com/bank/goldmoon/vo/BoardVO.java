package com.bank.goldmoon.vo;

import java.sql.Timestamp;

public class BoardVO {
	private int boardNo; 
    private String title;
    private String content;
    private Timestamp regDate;
    private int readcount;
    private int refNo;
    private int reStep;
    private int reLevel;
    private int state;
    private String userId;
    private int bno;
	public BoardVO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public BoardVO(int boardNo, String title, String content, Timestamp regDate, int readcount, int refNo, int reStep,
			int reLevel, int state, String userId, int bno) {
		super();
		this.boardNo = boardNo;
		this.title = title;
		this.content = content;
		this.regDate = regDate;
		this.readcount = readcount;
		this.refNo = refNo;
		this.reStep = reStep;
		this.reLevel = reLevel;
		this.state = state;
		this.userId = userId;
		this.bno = bno;
	}
	public int getBoardNo() {
		return boardNo;
	}
	public void setBoardNo(int boardNo) {
		this.boardNo = boardNo;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Timestamp getRegDate() {
		return regDate;
	}
	public void setRegDate(Timestamp regDate) {
		this.regDate = regDate;
	}
	public int getReadcount() {
		return readcount;
	}
	public void setReadcount(int readcount) {
		this.readcount = readcount;
	}
	public int getRefNo() {
		return refNo;
	}
	public void setRefNo(int refNo) {
		this.refNo = refNo;
	}
	public int getReStep() {
		return reStep;
	}
	public void setReStep(int reStep) {
		this.reStep = reStep;
	}
	public int getReLevel() {
		return reLevel;
	}
	public void setReLevel(int reLevel) {
		this.reLevel = reLevel;
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public int getBno() {
		return bno;
	}
	public void setBno(int bno) {
		this.bno = bno;
	}
	@Override
	public String toString() {
		return "BoardVO [boardNo=" + boardNo + ", title=" + title + ", content=" + content + ", regDate=" + regDate
				+ ", readcount=" + readcount + ", refNo=" + refNo + ", reStep=" + reStep + ", reLevel=" + reLevel
				+ ", state=" + state + ", userId=" + userId + ", bno=" + bno + "]";
	}
    
    
}
