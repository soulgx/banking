package com.bank.goldmoon.vo;

import java.sql.Timestamp;

public class DealingsInfoVO {

	
	private int dealingsNo;//거래번호
	private Timestamp dealDate;//거래시간
	private int payingNo;//입금:1 출금:2 자사입금:3 자사출금:4 타사입금:5 타사출금:6
	private int money;//거래금액
	private String content; //거래내용
	private int remainMoney; //남은 잔액
	private String customer;//거래처(상대방 이름 또는 회사명)
	private int custAccountNo; //거래처 계좌번호
	private int accountNo;//계좌번호
	private int bankingNo; //거래은행
	
	public DealingsInfoVO() {}
	
	
	public DealingsInfoVO(int dealingsNo, Timestamp dealDate, int payingNo, int money, String content,
			int remainMoney, String customer, int custAccountNo, int accountNo, int bankingNo) {
		this.dealingsNo = dealingsNo;
		this.dealDate = dealDate;
		this.payingNo = payingNo;
		this.money = money;
		this.content = content;
		this.remainMoney = remainMoney;
		this.customer = customer;
		this.custAccountNo = custAccountNo;
		this.accountNo = accountNo;
		this.bankingNo = bankingNo;
	}
	public int getDealingsNo() {
		return dealingsNo;
	}
	public void setDealingsNo(int dealingsNo) {
		this.dealingsNo = dealingsNo;
	}
	public Timestamp getDealDate() {
		return dealDate;
	}
	public void setDealDate(Timestamp dealDate) {
		this.dealDate = dealDate;
	}
	public int getPayingNo() {
		return payingNo;
	}
	public void setPayingNo(int payingNo) {
		this.payingNo = payingNo;
	}
	public int getMoney() {
		return money;
	}
	public void setMoney(int money) {
		this.money = money;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public int getRemainMoney() {
		return remainMoney;
	}
	public void setRemainMoney(int remainMoney) {
		this.remainMoney = remainMoney;
	}
	public String getCustomer() {
		return customer;
	}
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	public int getCustAccountNo() {
		return custAccountNo;
	}
	public void setCustAccountNo(int custAccountNo) {
		this.custAccountNo = custAccountNo;
	}
	public int getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(int accountNo) {
		this.accountNo = accountNo;
	}
	public int getBankingNo() {
		return bankingNo;
	}
	public void setBankingNo(int bankingNo) {
		this.bankingNo = bankingNo;
	}
	@Override
	public String toString() {
		return "DealingsInfoVO [dealingsNo=" + dealingsNo + ", dealDate=" + dealDate + ", payingNo="
				+ payingNo + ", money=" + money + ", content=" + content + ", remainMoney=" + remainMoney
				+ ", customer=" + customer + ", custAccountNo=" + custAccountNo + ", accountNo=" + accountNo
				+ ", bankingNo=" + bankingNo + "]";
	}
	
}
