package com.bank.goldmoon.vo;

import java.sql.Timestamp;

public class AccountInfoVO {
	
	private int accountNo;
	private String accountPw;
	private int accountMoney;
	private Timestamp openDate;	
	private String userId;
	
	public AccountInfoVO(){}
	
	public AccountInfoVO(int accountNo, String accountPw, int accountMoney, Timestamp openDate, String userId) {
		this.accountNo = accountNo;
		this.accountPw = accountPw;
		this.accountMoney = accountMoney;
		this.openDate = openDate;
		this.userId = userId;
	}
	public int getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(int accountNo) {
		this.accountNo = accountNo;
	}
	public String getAccountPw() {
		return accountPw;
	}
	public void setAccountPw(String accountPw) {
		this.accountPw = accountPw;
	}
	public int getAccountMoney() {
		return accountMoney;
	}
	public void setAccountMoney(int accountMoney) {
		this.accountMoney = accountMoney;
	}
	public Timestamp getOpenDate() {
		return openDate;
	}
	public void setOpenDate(Timestamp openDate) {
		this.openDate = openDate;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	@Override
	public String toString() {
		return "AccountInfoVo [accountNo=" + accountNo + ", accountPw=" + accountPw + ", accountMoney=" + accountMoney
				+ ", openDate=" + openDate + ", userId=" + userId + "]";
	}
	
	

}
