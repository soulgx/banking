package com.bank.goldmoon.dao;

import java.util.HashMap;
import java.util.List;

import com.bank.goldmoon.vo.UserVO;

public interface UserDAO {

	List<?> getNotice() throws Exception;

	HashMap<String, Object> getLogin(UserVO userVO) throws Exception;

	void getJoin(UserVO userVO) throws Exception;

	UserVO getInfoUser(String userId) throws Exception;

	void getUserUpdate(UserVO userVO) throws Exception;

	int idCheck(String userId) throws Exception;
}
