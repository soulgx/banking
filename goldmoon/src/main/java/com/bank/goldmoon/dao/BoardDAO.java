package com.bank.goldmoon.dao;

import java.util.HashMap;
import java.util.List;

import com.bank.goldmoon.vo.BoardVO;

public interface BoardDAO {
	void insertBoard(BoardVO boardVO) throws Exception;

	BoardVO boardDetail(int boardNo) throws Exception;

	List<?> boardList(HashMap<String, Object> param) throws Exception;

	int boardCount(int bno) throws Exception;

	BoardVO updateGetArticle(int boardNo) throws Exception;

	void updateArticle(BoardVO boardVO) throws Exception;

	void deleteArticle(BoardVO boardVO) throws Exception;

	int maxNumber(int bno) throws Exception;

	void updateRe(HashMap<String, Integer> update) throws Exception;

	int getSearchTitleCount(HashMap<String, Object> searching) throws Exception;

	int getSearchIdCount(HashMap<String, Object> searching) throws Exception;

	List<?> getSearchTitle(HashMap<String, Object> param) throws Exception;

	List<?> getSearchId(HashMap<String, Object> param) throws Exception;

	int selectAuthority(BoardVO boardVO) throws Exception;

	void deleteBoardComment(int boardNo) throws Exception;

	void statsBoard(BoardVO boardVO) throws Exception;

	void hitsUp(int boardNo) throws Exception;
}
