package com.bank.goldmoon.dao;

import java.util.HashMap;
import java.util.List;

import com.bank.goldmoon.vo.BoardVO;
import com.bank.goldmoon.vo.PageVO;

public interface AdminDAO {

	int dealingsAllCount() throws Exception;

	List<?> dealingsAllList(PageVO pageVO) throws Exception;

	int accountAllCount() throws Exception;

	List<?> accountAllList(PageVO pageVO) throws Exception;

	void balanceUp(double balance) throws Exception;

	int adminCheck(HashMap<String, Object> userInfo) throws Exception;

	List<String> userMailList() throws Exception;

	void boardDelete(HashMap<String, Object> boardNo) throws Exception;

	void commentDelete(HashMap<String, Object> boardNo) throws Exception;

	void deleteBoardComment(int boardNo) throws Exception;

	void statsBoard(BoardVO boardVO) throws Exception;

	void deleteArticle(BoardVO boardVO) throws Exception;


}
