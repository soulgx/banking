package com.bank.goldmoon.dao;

import com.bank.goldmoon.vo.UserVO;

public interface SearchDAO {
	
	String selectSearchid(UserVO userVo);

	String selectSearchpw(UserVO userVo);
	
	String selectPwdAns(UserVO userVo);
	
	void selectUpdatepw(UserVO userVO) throws Exception;
}
