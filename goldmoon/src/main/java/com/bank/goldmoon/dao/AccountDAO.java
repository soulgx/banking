package com.bank.goldmoon.dao;

import java.util.HashMap;
import java.util.List;

import com.bank.goldmoon.vo.AccountInfoVO;
import com.bank.goldmoon.vo.DealingsInfoVO;

public interface AccountDAO {

	void accountRegistOpen(AccountInfoVO aiVo) throws Exception;

	int accountCount(String username) throws Exception;

	List<?> accountList(String username) throws Exception;

	int getAccountMoney(int accountNo) throws Exception;

	void accountBreakDown(DealingsInfoVO dealingsInfoVO)  throws Exception;

	void updateInfoMoney(DealingsInfoVO dealingsInfoVO) throws Exception;

	AccountInfoVO flagCheck(AccountInfoVO accountInfoVO) throws Exception;

	int getDealingCount(int accountNo) throws Exception;

	List<?> dealingList(HashMap<String, Object> param) throws Exception;

	List<?> accountModal(DealingsInfoVO div) throws Exception;

	List<?> bankList() throws Exception;

	int getRemainMoney(int accountNo) throws Exception;

	HashMap<String, Object> getOther(int accountNo);

	String getApiKey(int bankingNo) throws Exception;

	int getKey(String apiKey) throws Exception;

	HashMap<String, Object> accountInfoNo(int accountNo) throws Exception;

	List<?> dealingList2(HashMap<String, Object> param) throws Exception;


}
