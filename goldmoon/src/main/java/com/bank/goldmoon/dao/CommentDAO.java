package com.bank.goldmoon.dao;

import java.util.HashMap;
import java.util.List;

import com.bank.goldmoon.vo.CommentVO;

public interface CommentDAO {
	
	int commentCount(int boardNo) throws Exception;
	
	List<?>listComment(HashMap<String, Object> param) throws Exception;
	
	void insertComment(CommentVO commentVO) throws Exception;
	
	void deleteComment(CommentVO commentVO) throws Exception;
	
	void updateComment(CommentVO commentVO) throws Exception;
	
	
	
}
