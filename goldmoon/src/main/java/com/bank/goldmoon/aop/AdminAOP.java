package com.bank.goldmoon.aop;

import java.util.HashMap;

import org.apache.commons.codec.digest.DigestUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bank.goldmoon.security.CustomUserDetails;
import com.bank.goldmoon.service.AdminService;

@Aspect
@Component
public class AdminAOP {
	
	@Autowired
	private AdminService adminService;
	
	@Pointcut("execution(* com.bank.goldmoon.controller.AdminController.procedure(..))")
	public void adminPw() {
	}
	
	@RequestMapping(headers="Accept=application/json;charset=UTF-8",
			produces=MediaType.APPLICATION_JSON_VALUE)
	@Around("adminPw()")
	@ResponseBody
	public HashMap<String,Object> procedure(ProceedingJoinPoint jp) throws Throwable {
		Object[] args = jp.getArgs();
		//CustomUserDetails userDetails = (CustomUserDetails)SecurityContextHolder.getContext().getAuthentication().getDetails();
		
		String userPw = (String) args[1];
		String userId = (String) args[2];
		//String userId = userDetails.getUsername();
		
		HashMap<String,Object> userInfo = new HashMap<String, Object>();
		userInfo.put("userId", userId);
		userInfo.put("userPw", DigestUtils.md5Hex(userPw));
		
		int count = adminService.adminCheck(userInfo);
		
		if (count == 0) {
			HashMap<String, Object> result = new HashMap<String, Object>();
			result.put("msg", "관리자 비밀번호가 틀립니다.");
			return result;
		}
			
		
		return (HashMap<String, Object>) jp.proceed();
	}

}
