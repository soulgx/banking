package com.bank.goldmoon.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;

import com.bank.goldmoon.dao.BoardDAO;
import com.bank.goldmoon.security.CustomUserDetails;
import com.bank.goldmoon.vo.BoardVO;

@Aspect
@Component
public class BoardAOP {
	
	@Autowired
	private BoardDAO boardDAO;

	@Pointcut("execution(* com.bank.goldmoon.controller.BoardController.board*(..))")
	public void permission() {
	}
	
	@Pointcut("execution(* com.bank.goldmoon.controller.BoardController.insertBoardForm(..))")
	public void insertBoard() {
	}
	
	
	

	@Around("permission()")
	public String permission(ProceedingJoinPoint jp) throws Throwable {
		
		Object[] args = jp.getArgs();
		CustomUserDetails userDetails = (CustomUserDetails)SecurityContextHolder.getContext().getAuthentication().getDetails();
		
		int boardNo = (Integer) args[0];
		Model model = (Model) args[1];
		
		BoardVO boardVO = new BoardVO();
		boardVO.setBoardNo(boardNo);
		boardVO.setUserId(userDetails.getUsername());
		
		int count = boardDAO.selectAuthority(boardVO);
		
		if (count == 0) {
			model.addAttribute("msg", "권한이 없습니다.");
			model.addAttribute("url", "javascript:history.back();");
			return "result";
		} else {
			return (String)jp.proceed();
		}
		
	}
	
	@Around("insertBoard()")
	public String insertBoard(ProceedingJoinPoint jp) throws Throwable {
		Object[] args = jp.getArgs();
		CustomUserDetails userDetails = (CustomUserDetails)SecurityContextHolder.getContext().getAuthentication().getDetails();
		int bno = (Integer) args[4];
		Model model = (Model) args[6];
		
		String authorize = userDetails.getAuthorize();
		
		if (bno == 1 && authorize.equals("ROLE_USER")) {
			model.addAttribute("msg", "권한이 없습니다.");
			model.addAttribute("url", "javascript:history.back();");
			return "result";
		}
		
		return (String)jp.proceed();
	
		
	}
	
	
}
