package com.bank.goldmoon.aop;

import java.util.HashMap;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;

import com.bank.goldmoon.dao.AccountDAO;
import com.bank.goldmoon.security.CustomUserDetails;
import com.bank.goldmoon.util.ParserAccount;
import com.bank.goldmoon.vo.AccountInfoVO;
import com.bank.goldmoon.vo.DealingsInfoVO;
import com.bank.goldmoon.vo.TransferInfoVO;

@Aspect
@Component
public class AccountAOP {
	
	@Autowired
	private AccountDAO accountDao;
	
	@Autowired
	private ParserAccount paserAccount;
	
	@Pointcut("execution(* com.bank.goldmoon.controller.AccountController.AccountRegistOk(..))")
	public void accountRegist() {
	}
	
	@Pointcut("execution(* com.bank.goldmoon.controller.AccountController.accountPayingOk(..))")
	public void transfer() {
	}
	
	@Pointcut("execution(* com.bank.goldmoon.controller.AccountController.aop*(..))")
	public void list() {
	}
	
	@Pointcut("execution(* com.bank.goldmoon.controller.AccountController.creditTransfer(..))")
	public void cutCreditTransfer() {
	}
	
	@Around("accountRegist()")
	public String accountRegist(ProceedingJoinPoint jp) throws Throwable {
		Object[] args = jp.getArgs();
		CustomUserDetails userDetails = (CustomUserDetails)SecurityContextHolder.getContext().getAuthentication().getDetails();
		
		// Object 배열로 args에 담아준다 AccountController에 AccountRegistOk의 객체들을
		Model model = (Model) args[0];
		
		int count = accountDao.accountCount(userDetails.getUsername());
		if( count >= 3){
			model.addAttribute("msg", "계좌를 4개이상 만드실수 없습니다.");
			model.addAttribute("url", "javascript:history.back();");
			return "result"; 
		}		
		return (String)jp.proceed();
	}
	
	@Around("transfer()")
	public String transfer(ProceedingJoinPoint jp) throws Throwable {
		Object[] args = jp.getArgs();
		CustomUserDetails userDetails = (CustomUserDetails)SecurityContextHolder.getContext().getAuthentication().getDetails();
		
		Model model = (Model) args[0];
		String accountPw = (String) args[1];
		DealingsInfoVO dealingsInfoVO = (DealingsInfoVO) args[2];
		
		int count = accountDao.accountCount(userDetails.getUsername());
		
		if( count == 0){
			model.addAttribute("msg", "계좌 생성 후 이용 가능 합니다");
			model.addAttribute("url", "../account/regist");
			return "result"; 
		}
		
		AccountInfoVO accountInfoVO = new AccountInfoVO();
		accountInfoVO.setAccountNo(dealingsInfoVO.getAccountNo());
		accountInfoVO.setAccountPw(DigestUtils.md5Hex(accountPw));
		
		accountInfoVO = accountDao.flagCheck(accountInfoVO);
		
		if(accountInfoVO==null){
			model.addAttribute("msg", "비밀번호가 일치하지않습니다.");
			model.addAttribute("url", "javascript:history.back();");
			return "result";
		}
		
		if(dealingsInfoVO.getPayingNo()==2 && accountInfoVO.getAccountMoney()<dealingsInfoVO.getMoney()){
			model.addAttribute("msg", "잔액이 부족합니다.");
			model.addAttribute("url", "javascript:history.back();");
			return "result";
		}
		
		return (String)jp.proceed();
	}
	
	@Around("list()")
	public String accList(ProceedingJoinPoint jp) throws Throwable {
		Object[] args = jp.getArgs();
		CustomUserDetails userDetails = (CustomUserDetails)SecurityContextHolder.getContext().getAuthentication().getDetails();
		
		Model model = (Model) args[0];
		
		int count = accountDao.accountCount(userDetails.getUsername());
		
		if( count == 0){
			model.addAttribute("msg", "계좌 생성 후 이용 가능 합니다");
			model.addAttribute("url", "../account/regist");
			return "result"; 
		}
		
		List<?> list=null;
		
		try {
			list = accountDao.accountList(userDetails.getUsername());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		model.addAttribute("accList",list);
		
		return (String)jp.proceed();
	}
	
	@Around("cutCreditTransfer()")
	public String creditTransfer(ProceedingJoinPoint jp) throws Throwable {
		Object[] args = jp.getArgs();
		CustomUserDetails userDetails = (CustomUserDetails)SecurityContextHolder.getContext().getAuthentication().getDetails();
		
		Model model = (Model) args[0];
		TransferInfoVO tiv = (TransferInfoVO) args[1];
		
		int count = accountDao.accountCount(userDetails.getUsername());
		
		if( count == 0){
			model.addAttribute("msg", "계좌 생성 후 이용 가능 합니다");
			model.addAttribute("url", "../account/regist");
			return "result"; 
		}
		
		AccountInfoVO accountInfoVO = new AccountInfoVO();
		accountInfoVO.setAccountNo(tiv.getAccountNo());
		accountInfoVO.setAccountPw(DigestUtils.md5Hex(tiv.getAccountPw()));
		
		accountInfoVO = accountDao.flagCheck(accountInfoVO);
		
		if(accountInfoVO == null){
			model.addAttribute("msg", "비밀번호가 일치하지않습니다.");
			model.addAttribute("url", "javascript:history.back();");
			return "result";
		}
		
		if(accountInfoVO.getAccountMoney()<tiv.getMoney()){
			model.addAttribute("msg", "잔액이 부족합니다.");
			model.addAttribute("url", "javascript:history.back();");
			return "result";
		}
		
		
		int bankingNo = tiv.getBankingNo();
		int custAccountNo = tiv.getCustAccountNo();
		
		
		if(bankingNo == 1){
			HashMap<String, Object> youAccount = accountDao.getOther(custAccountNo);
			
			if (youAccount == null) {
				model.addAttribute("msg", "존재하지 않는 계좌 입니다.");
				model.addAttribute("url", "javascript:history.back();");
				return "result";
			}
			
			if (tiv.getAccountNo() == custAccountNo) {
				model.addAttribute("msg", "같은 계좌로 이체 하실수 없습니다.");
				model.addAttribute("url", "javascript:history.back();");
				return "result";
			}
			
		} else if(bankingNo == 2) {
			String bankNo = accountDao.getApiKey(bankingNo);
			HashMap<String,Object> yai = paserAccount.youAccountInfo(custAccountNo+"", bankNo);
			
			if (yai == null) {
				model.addAttribute("msg", "존재하지 않는 계좌이거나 타은행 서버의 문제 입니다.");
				model.addAttribute("url", "javascript:history.back();");
				return "result";
			}
		}
		
		return (String)jp.proceed();
	}
	
	
	
	
}
