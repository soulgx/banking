package com.bank.goldmoon.security;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.bank.goldmoon.controller.UserController;
import com.bank.goldmoon.dao.UserDAO;
import com.bank.goldmoon.vo.UserVO;


public class CustomAuthenticationProvider implements AuthenticationProvider {  
 
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	
	private UserDAO userDAO;
	
	
    public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

	@Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

	@Override
	public Authentication authenticate(Authentication authentication)
			throws AuthenticationException {
		
		String user_id = (String)authentication.getPrincipal();		
		String user_pw = DigestUtils.md5Hex((String)authentication.getCredentials());
		
		UserVO vo = new UserVO();
		vo.setUserId(user_id);
		vo.setUserPw(user_pw);
		
		HashMap<String, Object> userInfo;
		
		try {
			userInfo = userDAO.getLogin(vo);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BadCredentialsException("Bad credentials");
		}
		
		// check whether user's credentials are valid.
		// if false, throw new BadCredentialsException(messages.getMessage("AbstractUserDetailsAuthenticationProvider.badCredentials", "Bad credentials"));
		
		
		if(userInfo != null){
			String authorize = (String) userInfo.get("LNAME");
			String name = (String) userInfo.get("USERNAME");
            logger.info("로그인 성공.");
            List<GrantedAuthority> roles = new ArrayList<GrantedAuthority>();
            roles.add(new SimpleGrantedAuthority(authorize));
            
            UsernamePasswordAuthenticationToken result = new UsernamePasswordAuthenticationToken(user_id, user_pw, roles);
            result.setDetails(new CustomUserDetails(user_id, user_pw, authorize, name));
            return result;         
        }else{
            logger.info("사용자 정보가 일치하지 않습니다.");
            throw new BadCredentialsException("Bad credentials");
        }
        
		
	}
}