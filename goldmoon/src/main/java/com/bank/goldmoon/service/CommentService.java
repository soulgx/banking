package com.bank.goldmoon.service;

import java.util.HashMap;
import java.util.List;

import com.bank.goldmoon.vo.CommentVO;

public interface CommentService {

	int commentCount(int boardNo) throws Exception;
	
	List<?>commnentList(HashMap<String, Object> param) throws Exception;
	
	void insertComment(CommentVO commentVO) throws Exception;
	
	void deleteComment(CommentVO commentVO) throws Exception;
	
	void updateComment(CommentVO commentVO) throws Exception;
	
	
}
