package com.bank.goldmoon.service;

import java.util.HashMap;
import java.util.List;

import com.bank.goldmoon.vo.AccountInfoVO;
import com.bank.goldmoon.vo.DealingsInfoVO;

public interface AccountService {
	void accountRegistOpen(AccountInfoVO aiVo) throws Exception;

	List<?> accountList(String username) throws Exception;

	void accountBreakDown(DealingsInfoVO dealingsInfoVO) throws Exception;

	int dealingCount(int accountNo) throws Exception;

	List<?> dealingList(HashMap<String, Object> param) throws Exception;

	List<?> accountModal(DealingsInfoVO div) throws Exception;

	List<?> bankList() throws Exception;

	int getRemainMoney(int accountNo) throws Exception;

	HashMap<String,Object> getOther(int accountNo);

	void creditTransfer(DealingsInfoVO myDiv, DealingsInfoVO youDiv) throws Exception;

	String paserAccount(DealingsInfoVO youDiv);

	int getKey(String apiKey) throws Exception;

	HashMap<String, Object> accountInfoNo(int accountNo) throws Exception;

	List<?> dealingList2(HashMap<String, Object> param) throws Exception;
	
	
}
