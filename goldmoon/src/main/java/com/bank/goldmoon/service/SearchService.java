package com.bank.goldmoon.service;

import com.bank.goldmoon.vo.UserVO;

public interface SearchService {
	
	String selectSearchid(UserVO userVo);

	String selectSearchpw(UserVO userVo);
	
	String selectPwdAns(UserVO uservo);
	
	void selectUpdatepw(UserVO uservo) throws Exception;
	
}
