package com.bank.goldmoon.service;

import java.util.HashMap;
import java.util.List;

import com.bank.goldmoon.vo.BoardVO;

public interface BoardService {
	void insertBoard(BoardVO boardVO) throws Exception;

	BoardVO boardDetail(int boardNo) throws Exception;
	
	List<?> boardList(HashMap<String, Object> param) throws Exception;

	int getBoardCount(int bno) throws Exception;

	BoardVO updateGetArticle(int boardNo) throws Exception;

	void updateArticle(BoardVO boardVO) throws Exception;

	void deleteArticle(BoardVO boardVO) throws Exception;

	int getSeachCount(HashMap<String, Object> searching) throws Exception;

	List<?> getSearchBoardList(HashMap<String, Object> param) throws Exception;
}
