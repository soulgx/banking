package com.bank.goldmoon.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.bank.goldmoon.dao.AccountDAO;
import com.bank.goldmoon.util.ParserAccount;
import com.bank.goldmoon.vo.AccountInfoVO;
import com.bank.goldmoon.vo.DealingsInfoVO;

@Service
public class AccountServiceImpl implements AccountService{
	@Autowired
	private AccountDAO accountDao;
	
	@Autowired
	@Qualifier("transactionManager")
	private PlatformTransactionManager transactionManager;
	
	DefaultTransactionDefinition def = null;
	TransactionStatus status = null;
	
	@Autowired
	private ParserAccount paserAccount;
	
	@Override
	public void accountRegistOpen(AccountInfoVO aiVo) throws Exception {
		accountDao.accountRegistOpen(aiVo);
	}

	@Override
	public List<?> accountList(String username) throws Exception {
		return accountDao.accountList(username);
	}

	@Override
	public void accountBreakDown(DealingsInfoVO dealingsInfoVO) throws Exception {
		int accountMoney=accountDao.getAccountMoney(dealingsInfoVO.getAccountNo());
		
		if(dealingsInfoVO.getPayingNo()==1 || dealingsInfoVO.getPayingNo()==5){
			int money=dealingsInfoVO.getMoney()+accountMoney;
			dealingsInfoVO.setRemainMoney(money);
		}else if(dealingsInfoVO.getPayingNo()==2){
			int money = accountMoney-dealingsInfoVO.getMoney();
			dealingsInfoVO.setRemainMoney(money);
		} 
		accountDao.accountBreakDown(dealingsInfoVO);
		accountDao.updateInfoMoney(dealingsInfoVO);
		
	}

	@Override
	public int dealingCount(int accountNo) throws Exception {
		return accountDao.getDealingCount(accountNo);
	}

	@Override
	public List<?> dealingList(HashMap<String, Object> param) throws Exception {
		return accountDao.dealingList(param);
	}

	@Override
	public List<?> accountModal(DealingsInfoVO div) throws Exception {
		return accountDao.accountModal(div);
	}

	@Override
	public List<?> bankList() throws Exception {
		return accountDao.bankList();
	}

	@Override
	public int getRemainMoney(int accountNo) throws Exception {
		return accountDao.getRemainMoney(accountNo);
	}

	@Override
	public HashMap<String, Object> getOther(int accountNo) {
		return accountDao.getOther(accountNo);
	}

	@Override
	@Transactional
	public void creditTransfer(DealingsInfoVO myDiv, DealingsInfoVO youDiv) throws Exception {
		
		def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		
		status = transactionManager.getTransaction(def);
		
		String bankNo = null;
		int count = 0;
		
		if(myDiv.getBankingNo()==1){
			accountDao.accountBreakDown(myDiv);		
			accountDao.accountBreakDown(youDiv);
			accountDao.updateInfoMoney(myDiv);	
			accountDao.updateInfoMoney(youDiv);
		}else{
			bankNo = accountDao.getApiKey(youDiv.getBankingNo());
			accountDao.accountBreakDown(myDiv);
			accountDao.updateInfoMoney(myDiv);
			
			count = paserAccount.transfer(youDiv, bankNo);
			
			if (count == 1) {
				transactionManager.commit(status);
			} else if (count == 0) {
				transactionManager.rollback(status);
			}
			System.out.println(count);
		}
	}

	@Override
	public String paserAccount(DealingsInfoVO youDiv) {
		
		String bankNo = null;
		try {
			bankNo = accountDao.getApiKey(youDiv.getBankingNo());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		HashMap<String,Object> yai = paserAccount.youAccountInfo(youDiv.getAccountNo()+"", bankNo);
		
		
		return (String) yai.get("USERNAME");
	}


	@Override
	public int getKey(String apiKey) throws Exception {
		return accountDao.getKey(apiKey);
	}

	@Override
	public HashMap<String, Object> accountInfoNo(int accountNo) throws Exception {
		return accountDao.accountInfoNo(accountNo);
	}

	@Override
	public List<?> dealingList2(HashMap<String, Object> param) throws Exception {
		return accountDao.dealingList2(param);
	}


}
