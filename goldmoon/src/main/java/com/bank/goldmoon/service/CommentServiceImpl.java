package com.bank.goldmoon.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.bank.goldmoon.dao.CommentDAO;
import com.bank.goldmoon.vo.CommentVO;

@Service
public class CommentServiceImpl implements CommentService {
	
	@Autowired
	@Qualifier("commentDAO")
	private CommentDAO commentDAO;

	@Override
	public int commentCount(int boardNo) throws Exception {
		return commentDAO.commentCount(boardNo);
	}

	@Override
	public List<?> commnentList(HashMap<String, Object> param) throws Exception {
		return commentDAO.listComment(param);
	}

	@Override
	public void insertComment(CommentVO uservo) throws Exception {
		commentDAO.insertComment(uservo);
	}

	@Override
	public void deleteComment(CommentVO commentVO) throws Exception {
		commentDAO.deleteComment(commentVO);
	}

	@Override
	public void updateComment(CommentVO uservo) throws Exception {
		commentDAO.updateComment(uservo);
	}

}
