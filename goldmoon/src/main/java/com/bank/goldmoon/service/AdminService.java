package com.bank.goldmoon.service;

import java.util.HashMap;

import com.bank.goldmoon.vo.BoardVO;

public interface AdminService {

	HashMap<String, Object> dealingsAllList(int pageNo) throws Exception;

	HashMap<String, Object> accountAllList(int pageNo) throws Exception;

	void balanceUp(double balance) throws Exception;

	int adminCheck(HashMap<String, Object> userInfo) throws Exception;

	void boardDelete(HashMap<String, Object> boardNo) throws Exception;

	void boardDeleteOne(BoardVO boardVO) throws Exception;

}
