package com.bank.goldmoon.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bank.goldmoon.dao.BoardDAO;
import com.bank.goldmoon.vo.BoardVO;

@Service
public class BoardServiceImpl implements BoardService{

	@Autowired
	@Qualifier("boardDAO")
	private BoardDAO boardDAO;
	
	@Override
	@Transactional
	public void insertBoard(BoardVO boardVO) throws Exception {
		int boardNo = boardVO.getBoardNo();
		int refNo= boardVO.getRefNo();
		int reStep = boardVO.getReStep();
		int reLevel = boardVO.getReLevel();
		int bno = boardVO.getBno();
		
		int number = boardDAO.maxNumber(bno);
		
		if(boardNo != 0){
			HashMap<String,Integer> update = new HashMap<String, Integer>();
			update.put("refNo",refNo);
			update.put("reStep",reStep);
			update.put("bno",bno);
			
			boardDAO.updateRe(update);
			reStep=reStep+1;
			reLevel=reLevel+1;
		}else{
			refNo =number+1;
			reStep=0;
			reLevel=0;
		}
		
		boardVO.setRefNo(refNo);
		boardVO.setReStep(reStep);
		boardVO.setReLevel(reLevel);
		
		boardDAO.insertBoard(boardVO);
	}

	@Override
	public BoardVO boardDetail(int boardNo) throws Exception {
		boardDAO.hitsUp(boardNo);
		return boardDAO.boardDetail(boardNo);
	}

	@Override
	public List<?> boardList(HashMap<String, Object> param) throws Exception {
		return boardDAO.boardList(param);
	}

	@Override
	public int getBoardCount(int bno) throws Exception {
		return boardDAO.boardCount(bno);
	}

	@Override
	public BoardVO updateGetArticle(int boardNo) throws Exception {
		return boardDAO.boardDetail(boardNo);
	}

	@Override
	public void updateArticle(BoardVO boardVO) throws Exception {
		boardDAO.updateArticle(boardVO);
	}

	@Override
	public void deleteArticle(BoardVO boardVO) throws Exception {
		boardDAO.deleteBoardComment(boardVO.getBoardNo());
		boardDAO.statsBoard(boardVO);
		boardDAO.deleteArticle(boardVO);
	}

	@Override
	public int getSeachCount(HashMap<String, Object> searching) throws Exception {
		
		int no = (Integer)searching.get("searchNo");
		int count = -1;
		
		if (no == 1){
			count = boardDAO.getSearchTitleCount(searching);
		} else {
			count = boardDAO.getSearchIdCount(searching);
		}
		
		return count;
	}

	@Override
	public List<?> getSearchBoardList(HashMap<String, Object> param) throws Exception {
		
		int no = (Integer)param.get("searchNo");
		
		if (no == 1){
			return boardDAO.getSearchTitle(param);
		} else {
			return boardDAO.getSearchId(param);
		}
	}

	

}
