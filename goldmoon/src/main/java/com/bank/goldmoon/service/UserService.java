package com.bank.goldmoon.service;

import java.util.List;

import com.bank.goldmoon.vo.UserVO;


public interface UserService {

	List<?> getNotice() throws Exception;

	void getJoin(UserVO userVO) throws Exception;

	UserVO getInfoUser(String userId) throws Exception;

	void getUserUpdate(UserVO userVO) throws Exception;

	int idCheck(String userId) throws Exception;

}
