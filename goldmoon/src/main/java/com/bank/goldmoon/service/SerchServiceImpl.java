package com.bank.goldmoon.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.bank.goldmoon.dao.SearchDAO;
import com.bank.goldmoon.vo.UserVO;

@Service
public class SerchServiceImpl implements SearchService {
	
	@Autowired
	@Qualifier("searchDAO")
	SearchDAO searchDAO;

	@Override
	public String selectSearchid(UserVO userVo) {
		return searchDAO.selectSearchid(userVo);
	}

	@Override
	public String selectSearchpw(UserVO userVo) {
		return searchDAO.selectSearchpw(userVo);
	}

	@Override
	public String selectPwdAns(UserVO userVo) {
		return searchDAO.selectPwdAns(userVo);
	}

	@Override
	public void selectUpdatepw(UserVO userVO) throws Exception {
		searchDAO.selectUpdatepw(userVO);
	}

	

}
