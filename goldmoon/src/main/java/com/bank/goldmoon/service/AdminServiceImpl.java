package com.bank.goldmoon.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bank.goldmoon.dao.AdminDAO;
import com.bank.goldmoon.vo.BoardVO;
import com.bank.goldmoon.vo.PageVO;

@Service
public class AdminServiceImpl implements AdminService {
	
	@Autowired
	private AdminDAO adminDAO;
	

	@Override
	public HashMap<String, Object> dealingsAllList(int pageNo) throws Exception {
		int totalCount = adminDAO.dealingsAllCount();

		PageVO pageVO = new PageVO(10, pageNo, totalCount);
		List<?> list = adminDAO.dealingsAllList(pageVO);
		
		HashMap<String, Integer> page = new HashMap<String, Integer>();
		page.put("pageNo", pageVO.getCurrentPage());
		page.put("pageSize", pageVO.getEndNo());
		page.put("pageCount", pageVO.getNumberEnd());
		page.put("listCount", pageVO.getRecordPerPage());
		
		HashMap<String,Object> daalingsAllList = new HashMap<String, Object>();
		daalingsAllList.put("list", list);
		daalingsAllList.put("result", "ok");
		daalingsAllList.put("page", page);
		
		return daalingsAllList;
	}


	@Override
	public HashMap<String, Object> accountAllList(int pageNo) throws Exception {
		int totalCount = adminDAO.accountAllCount();
		PageVO pageVO = new PageVO(10, pageNo, totalCount);
		
		List<?> list = adminDAO.accountAllList(pageVO);
		
		HashMap<String, Integer> page = new HashMap<String, Integer>();
		page.put("pageNo", pageVO.getCurrentPage());
		page.put("pageSize", pageVO.getEndNo());
		page.put("pageCount", pageVO.getNumberEnd());
		page.put("listCount", pageVO.getRecordPerPage());
		
		HashMap<String,Object> accountAllList = new HashMap<String, Object>();
		accountAllList.put("list", list);
		accountAllList.put("result", "ok");
		accountAllList.put("page", page);
		
		return accountAllList;
	}


	@Override
	@Transactional
	public void balanceUp(double balance) throws Exception {
		adminDAO.balanceUp(balance);
	}


	@Override
	public int adminCheck(HashMap<String, Object> userInfo) throws Exception {
		return adminDAO.adminCheck(userInfo);
	}


	@Override
	@Transactional
	public void boardDelete(HashMap<String, Object> boardNo) throws Exception {
		adminDAO.commentDelete(boardNo);
		adminDAO.boardDelete(boardNo);
		
	}


	@Override
	@Transactional
	public void boardDeleteOne(BoardVO boardVO) throws Exception {
		adminDAO.deleteBoardComment(boardVO.getBoardNo());
		adminDAO.statsBoard(boardVO);
		adminDAO.deleteArticle(boardVO);
	}

	

}
