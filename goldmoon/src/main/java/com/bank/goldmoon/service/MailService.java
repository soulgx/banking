package com.bank.goldmoon.service;

import java.util.List;

import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.bank.goldmoon.dao.AdminDAO;

@Service("mailService")
public class MailService {
	
	@Autowired
	private JavaMailSender mailSender;
	
	@Autowired
	private AdminDAO adminDAO;
	
	@Value("soulgx@daum.net")
	private String from;
	
	@Async
	public void sendMail(String to, String subject, String text) {
		try {
		    SimpleMailMessage message = new SimpleMailMessage();
		    message.setFrom(from);
		    message.setTo(to);
		    message.setSubject(subject);
		    message.setText(text);
		    mailSender.send(message);
		} catch (MailException e) {
			e.printStackTrace();
			return;
		} catch (Throwable e) {
			e.printStackTrace();
			return;
		}
	}  
	
	@Async
	public void adminMail(String subject, String text) throws Exception {
		List<String> mailList = null;
		
		try {
			mailList = adminDAO.userMailList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		MimeMessage message = mailSender.createMimeMessage();
		for (String userMail : mailList) {
			
			try {
	            MimeMessageHelper messageHelper = new MimeMessageHelper(message, true, "UTF-8");
	            messageHelper.setSubject(subject);
	            messageHelper.setText(text, true);
	            messageHelper.setFrom(from);
	            messageHelper.setTo(userMail);
	            mailSender.send(message);
			} catch (MailException e) {
				e.printStackTrace();
				return;
			} catch (Throwable e) {
				e.printStackTrace();
				return;
			}
				
		}
	}  
	
	

}
