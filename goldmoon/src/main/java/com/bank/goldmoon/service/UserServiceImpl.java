package com.bank.goldmoon.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.bank.goldmoon.dao.UserDAO;
import com.bank.goldmoon.vo.UserVO;



@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	@Qualifier("userDAO")
	private UserDAO userDAO;
	
	
	@Override
	public List<?> getNotice() throws Exception {
		return userDAO.getNotice();
	}


	@Override
	public void getJoin(UserVO userVO) throws Exception {
		userDAO.getJoin(userVO);
	}


	@Override
	public UserVO getInfoUser(String userId) throws Exception {
		return userDAO.getInfoUser(userId);
	}


	@Override
	public void getUserUpdate(UserVO userVO) throws Exception {
		userDAO.getUserUpdate(userVO);
	}


	@Override
	public int idCheck(String userId) throws Exception {
		return userDAO.idCheck(userId);
	}

}
