package com.bank.goldmoon.controller;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bank.goldmoon.service.CommentService;
import com.bank.goldmoon.vo.CommentVO;
import com.bank.goldmoon.vo.PageVO;

@Controller
public class CommentController {
	
	private static final Logger logger = LoggerFactory.getLogger(CommentController.class);

	
	@Autowired
	private CommentService commentService;
	
	@RequestMapping(value="/comment/list", 
			headers="Accept=application/json;charset=UTF-8", 
			produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public HashMap<String, Object> list(int boardNo,
			@RequestParam(defaultValue="1") int pageNumber) throws Exception{
		
		int totalCount = commentService.commentCount(boardNo);
		
		PageVO page = new PageVO(10, pageNumber, totalCount);
		
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("boardNo", boardNo);
		param.put("startNo", page.getStartNo());
		param.put("endNo", page.getEndNo());
		
		
		List<?> list = commentService.commnentList(param);
		
		HashMap<String, Object> commentList = new HashMap<String, Object>();
		commentList.put("item", list);
		commentList.put("currentPage", page.getCurrentPage());
		commentList.put("numberStart", page.getNumberStart());
		commentList.put("numberEnd", page.getNumberEnd());
		commentList.put("totalPage", page.getTotalPage());
		
		
		
		return commentList;
		
	}
	
	@Secured({"ROLE_ADMIN", "ROLE_USER"})
	@RequestMapping(value="comment/insert", method=RequestMethod.POST)
	public String insertComment(CommentVO commentVO){
		
		logger.info(commentVO.toString());
		
		try {
			commentService.insertComment(commentVO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return "redirect:../comment/list?boardNo="+commentVO.getBoardNo();
		
	}
	
	@Secured({"ROLE_ADMIN", "ROLE_USER"})
	@RequestMapping(value="comment/update", method=RequestMethod.POST)
	public String updateComment(CommentVO commentVO, 
			@RequestParam(defaultValue="1") int pageNumber){
		
		logger.info(commentVO.toString());
		
		try {
			commentService.updateComment(commentVO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "redirect:../comment/list?boardNo="+commentVO.getBoardNo()+"&pageNumber="+pageNumber;
	}
	
	@Secured({"ROLE_ADMIN", "ROLE_USER"})
	@RequestMapping(value="comment/delete", method=RequestMethod.POST)
	public String deleteComment(CommentVO commentVO, 
			@RequestParam(defaultValue="1") int pageNumber){
		logger.info(commentVO.toString());
		
		try {
			commentService.deleteComment(commentVO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "redirect:../comment/list?boardNo="+commentVO.getBoardNo()+"&pageNumber="+pageNumber;
		
	}
	
}
