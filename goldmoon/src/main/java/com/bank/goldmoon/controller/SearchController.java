package com.bank.goldmoon.controller;

import java.util.Random;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bank.goldmoon.service.MailService;
import com.bank.goldmoon.service.SearchService;
import com.bank.goldmoon.vo.UserVO;

@Controller
public class SearchController {
	
	private static final Logger logger = LoggerFactory.getLogger(SearchController.class);
	
	@Autowired
	private SearchService searchService;
	
	@Autowired
	private MailService mailService;
	
	@RequestMapping(value="idsearch", method=RequestMethod.GET)
	public String idsearch(){
		
		return "user/idsearch";
			
	}

	@RequestMapping(value="idcheck", method=RequestMethod.POST)
	public String idsearch(UserVO userVO, Model model){
		
				
		
		logger.info(userVO.toString());
		String id = searchService.selectSearchid(userVO);
		
		
		model.addAttribute("id",id);
		
		logger.info(id);
		return "user/idcheck";
	}
	
	@RequestMapping(value="pwcheck", method=RequestMethod.GET)
	public String pwsearch(){
		return "user/pwcheck";
	}
	
	@RequestMapping(value="pwsearch", method=RequestMethod.POST)
	public String pwsearch(Model model, UserVO vo){
		logger.info(vo.toString());
		
		String pwHint = searchService.selectSearchpw(vo);
		
		model.addAttribute("pwhint",pwHint);
		model.addAttribute("userId", vo.getUserId());
		model.addAttribute("userEmail", vo.getUserEmail());
		return "user/pwcheckok";
	}

	@RequestMapping(value="pwsearchOk", method=RequestMethod.POST)
	public String pwsearchOk(Model model, UserVO vo){
		logger.info(vo.toString());
		
		String pw = searchService.selectPwdAns(vo);
		
		
		
		if (pw != null) {
			Random rnd =new Random();
			StringBuffer buf =new StringBuffer();
			 
			for(int i=0;i<10;i++){
			    if(rnd.nextBoolean()){
			        buf.append((char)((int)(rnd.nextInt(26))+97));
			    }else{
			        buf.append((rnd.nextInt(10))); 
			    }
			}
			
			String password =buf.toString();
			
			//암호화
			vo.setUserPw(DigestUtils.md5Hex(password));
			
			
			try {
				searchService.selectUpdatepw(vo);				
				mailService.sendMail(vo.getUserEmail(), "골드문 임시 비밀번호 입니다.", "당신의 임시 비밀번호는 " + password + "입니다.");
			} catch (Exception e) {
				e.printStackTrace();
			}
			model.addAttribute("msg", "메일로 임시비밀번호를 발송 처리하였습니다.");
		} else {
			model.addAttribute("msg", "정보가 부적합 합니다.");
		}
		
		
		
				
		return "user/OK";
	}

	
	
}
