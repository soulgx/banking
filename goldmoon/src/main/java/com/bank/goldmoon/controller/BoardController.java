package com.bank.goldmoon.controller;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.bank.goldmoon.security.CustomUserDetails;
import com.bank.goldmoon.service.BoardService;
import com.bank.goldmoon.vo.BoardVO;
import com.bank.goldmoon.vo.PageVO;

@Controller
@RequestMapping("/board")
public class BoardController {
	
	
	private static final Logger logger = LoggerFactory.getLogger(BoardController.class);
	
	@Autowired
	private BoardService boardService;
	
	
	@Secured({"ROLE_ADMIN", "ROLE_USER"})
	@RequestMapping(value="/insert",method=RequestMethod.GET)
	public String insertBoardForm(
			@RequestParam(defaultValue="0") int boardNo,
			@RequestParam(defaultValue="1") int refNo,
			@RequestParam(defaultValue="0") int reStep,
			@RequestParam(defaultValue="0") int reLevel,
			@RequestParam(defaultValue="1") int bno,
			@RequestParam(defaultValue="1") int pageNum,
			Model model){
	
		BoardVO boardVO = new BoardVO();
		boardVO.setBoardNo(boardNo);
		boardVO.setReLevel(reLevel);
		boardVO.setReStep(reStep);
		boardVO.setRefNo(refNo);
		
		model.addAttribute("boardVO", boardVO);
		model.addAttribute("bno", bno);
		model.addAttribute("pageNum", pageNum);
		
		
		return "board/write";
	}
	
	@Secured({"ROLE_ADMIN", "ROLE_USER"})
	@RequestMapping(value="/insert",method=RequestMethod.POST)
	public String insertBoard(Model model,BoardVO boardVO,
			@RequestParam(defaultValue="1") int pageNum,
			@RequestParam(defaultValue="1") int bno ){
		boardVO.setRegDate(new Timestamp(System.currentTimeMillis()));
		boardVO.setBno(bno);
		
		logger.info(boardVO.toString());
		
		try {
			boardService.insertBoard(boardVO);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "게시판 글작성 실패");
			model.addAttribute("url", "javascript:history.back()");
			return "result";
		}
		
		return "redirect:../board/list?bno="+boardVO.getBno();
	}
	
	@RequestMapping(value="/content",method=RequestMethod.GET)
	public String detailBoard(
			@RequestParam(defaultValue="1") int bno,
			@RequestParam(defaultValue="1") int pageNum,
			int boardNo, String search, String searchNo, Model model){
		
		BoardVO boardVO;
		
		try {
			 boardVO = boardService.boardDetail(boardNo);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "조회 실패");
			model.addAttribute("url", "javascript:history.back()");
			return "result";
		}
		
		
		model.addAttribute("boardVO", boardVO);
		model.addAttribute("pageNum", pageNum);
		model.addAttribute("bno", bno);
		
		return "board/content";
	}
	
	@RequestMapping(value="list",method=RequestMethod.GET)
	public String listBoard(@RequestParam(defaultValue="1") int pageNum,
			@RequestParam(defaultValue="1") int bno, 
			@RequestParam(defaultValue="1",name="searchno") int searchNo,
			String search,Model model){
		
		
		int totalCount = 0;
		int recordPerPage = 12;
		
		String keyword=null;
		if(search == null || search.trim().equals("")){
			try {
				totalCount = boardService.getBoardCount(bno);
			} catch (Exception e1) {
				e1.printStackTrace();
			}			
		} else {		
			keyword = "%"+search+"%";
			HashMap<String,Object> searching = new HashMap<String, Object>();
			searching.put("keyword", keyword);
			searching.put("searchNo", searchNo);
			searching.put("bno", bno);
			
			try {
				totalCount = boardService.getSeachCount(searching);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		
		PageVO pageInfo = new PageVO(recordPerPage, pageNum, totalCount);
		
		List<?> boardList=null;
		
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("startNo", pageInfo.getStartNo());
		param.put("endNo", pageInfo.getEndNo());
		param.put("bno", bno);
		
		if(search == null || search.trim().equals("")){
			try {
				boardList = boardService.boardList(param);
			} catch (Exception e) {
				e.printStackTrace();
			}			
		} else {
			param.put("keyword", keyword);
			param.put("searchNo", searchNo);
			try {
				boardList = boardService.getSearchBoardList(param);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		
		
		model.addAttribute("boardList",boardList);
		model.addAttribute("pageInfo",pageInfo);
		model.addAttribute("bno",bno);
		model.addAttribute("search", search);
		model.addAttribute("searchno", searchNo);
		
		
		return "board/list";
	}
	
	@Secured({"ROLE_ADMIN", "ROLE_USER"})
	@RequestMapping(value="update",method=RequestMethod.GET)
	public String boardUpdateForm(int boardNo, Model model,
			@RequestParam(defaultValue="2") int bno,
			@RequestParam(defaultValue="1") int pageNum ){
		
		
		BoardVO boardVO = null;
		
		try {
			boardVO = boardService.updateGetArticle(boardNo);
		} catch (Exception e) {
			e.printStackTrace();
			return "result";
		}
		
		model.addAttribute("boardNo",boardNo);
		model.addAttribute("pageNum",pageNum);
		model.addAttribute("article",boardVO);
		model.addAttribute("bno",bno);
		
		return "board/write";
	}
	
	@Secured({"ROLE_ADMIN", "ROLE_USER"})
	@RequestMapping(value="update",method=RequestMethod.POST)
	public String updateBoard(int boardNo, Model model,
			@RequestParam(defaultValue="2") int bno,
			@RequestParam(defaultValue="1") int pageNum,
			BoardVO boardVO){
		
		try {
			boardService.updateArticle(boardVO);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return "redirect:../board/list?bno="+boardVO.getBno()+"&pageNum="+pageNum;
	}
	
	@Secured({"ROLE_ADMIN", "ROLE_USER"})
	@RequestMapping(value="delete",method=RequestMethod.GET)
	public String boardDelete(int boardNo, Model model,
			@RequestParam(defaultValue="1") int pageNum,
			BoardVO boardVO){
		CustomUserDetails userDetails = (CustomUserDetails)SecurityContextHolder.getContext().getAuthentication().getDetails();
		
		int reLevel = boardVO.getReLevel()+1;
		boardVO.setReLevel(reLevel);
		boardVO.setState(1);
		boardVO.setUserId(userDetails.getUsername());
		
		try {
			boardService.deleteArticle(boardVO);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "redirect:../board/list?bno="+boardVO.getBno()+"&pageNum"+pageNum;
	}
	
}
