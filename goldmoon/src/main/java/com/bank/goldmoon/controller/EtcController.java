package com.bank.goldmoon.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartRequest;
import org.springframework.web.servlet.ModelAndView;

import com.bank.goldmoon.service.UserService;
import com.bank.goldmoon.util.FileUpload;
import com.bank.goldmoon.vo.UserVO;


@Controller
public class EtcController {

	private static final Logger logger = LoggerFactory.getLogger(EtcController.class);

	@Autowired
	private UserService userService;

	@Value("${upload.url}")
	private String uploadUrl;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView main() {
		ModelAndView mav = new ModelAndView();

		List<?> list = null;

		try {
			list = userService.getNotice();
			logger.info(list.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		mav.addObject("list", list);
		mav.setViewName("user/main");
		return mav;
	}

	@RequestMapping(value = "/company/info", method = RequestMethod.GET)
	public ModelAndView company(@RequestParam(defaultValue="1") int no) {
		ModelAndView mav = new ModelAndView();
		mav.addObject("no", no);
		mav.setViewName("etc/info");
		return mav;
	}

	@RequestMapping(value = "/security/info", method = RequestMethod.GET)
	public ModelAndView security(@RequestParam(defaultValue="1") int num) {
		ModelAndView mav = new ModelAndView();
		mav.addObject("sno", num);
		mav.setViewName("etc/info");
		return mav;
	}
	
	@RequestMapping(value = "file/fileUpload", method = RequestMethod.POST)
	public String fileUpload(Model model, MultipartRequest multipartRequest, HttpServletRequest request) throws IOException{
		MultipartFile imgfile = multipartRequest.getFile("Filedata");
		Calendar cal = Calendar.getInstance();
		String fileName = imgfile.getOriginalFilename();
		String fileType = fileName.substring(fileName.lastIndexOf("."), fileName.length());
		String replaceName = cal.getTimeInMillis() + fileType;  
		
		//String path = request.getSession().getServletContext().getRealPath("/")+File.separator+"resources/upload";
		String path = uploadUrl;
		
		FileUpload.fileUpload(imgfile, path, replaceName);
		System.out.println(imgfile);
		logger.info(path);
		logger.info(replaceName);
		model.addAttribute("path", path);
		model.addAttribute("filename", replaceName);
		
		return "file_upload";
	}
	
	@RequestMapping(value = "download")
	public void download(
		String fileName, HttpServletResponse response) throws IOException {
		
		File file = new File(uploadUrl, fileName);
		
		response.setContentType("application/octet-stream");
		response.setContentLength((int) file.length());
		response.setHeader(
			"Content-Disposition", 
			"attachment; fileName=\"" + URLEncoder.encode(fileName,"UTF-8") + "\"");
		InputStream is = null;
		OutputStream os = response.getOutputStream();
		
		is = new FileInputStream(file);
		FileCopyUtils.copy(is, os);
		
		try {
			is.close();
			os.flush();
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(
			value="/json",
			headers="Accept=application/json;charset=UTF-8",
			produces={MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public HashMap<String, Object> jsonTest() throws Exception {
		HashMap<String, Object> test = new HashMap<String, Object>();
		
		test.put("id", "admin");
		test.put("pw", "admin");
		test.put("name", "관리자");
		
		return test;
	}
	

	
	@RequestMapping(
			value="/xml",
			headers="Accept=application/xml",
			produces=MediaType.APPLICATION_XML_VALUE)
	@ResponseBody
	public UserVO xmlTest() throws Exception {
		UserVO vo = new UserVO();
		vo.setUserAddr1("addr1");
		vo.setUserAddr2("addr2");
		vo.setUserEmail("admin@admin.com");
		vo.setUserGender(1);
		vo.setUserId("admin");
		vo.setUserLevel(6);
		vo.setUserName("관리자");
		vo.setUserPhone("010-1111-2222");
		vo.setUserPw("admin");
		vo.setUserPwdAnswer("admin");
		vo.setUserPwdHint("admin");
		vo.setUserZipcode("123-456");
		vo.setUserRegDate(new Timestamp(System.currentTimeMillis()));
		
		return vo;
	}
	

}
