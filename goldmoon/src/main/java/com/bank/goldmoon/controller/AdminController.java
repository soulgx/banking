package com.bank.goldmoon.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bank.goldmoon.service.AdminService;
import com.bank.goldmoon.service.MailService;
import com.bank.goldmoon.vo.BoardVO;

@Controller
@RequestMapping("/admin")
@Secured("ROLE_ADMIN")
@EnableAsync
public class AdminController {
	
	@Autowired
	private AdminService adminService;
	
	@Autowired
	private MailService mailService;
	
	private static final Logger logger = LoggerFactory.getLogger(AdminController.class);
	
	@RequestMapping(value = "/indexPage", method = RequestMethod.GET)
	public String adminPage() { 
		return "admin/main";
	}
	
	@RequestMapping(value="/dealingsAllList", 
			headers="Accept=application/json;charset=UTF-8", 
			produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public HashMap<String,Object> daalingsAllList(int pageNo) throws Exception{
		return adminService.dealingsAllList(pageNo);
	}
	
	
	@RequestMapping(value="/accountAllList", 
			headers="Accept=application/json;charset=UTF-8", 
			produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public HashMap<String,Object> accountAllList(int pageNo) throws Exception{		
		return adminService.accountAllList(pageNo);
	}
	
	@RequestMapping(value="/procedure", headers="Accept=application/json;charset=UTF-8",
			produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public HashMap<String,Object> procedure(
			@RequestParam(name="user_balance") double balance,
			@RequestParam(name="adminPw") String adminPw,
			@RequestParam(name="adminId") String adminId) throws Exception{
		
		logger.info("넘어온 관리자 아이디 : " + adminId);
		logger.info("넘어온 관리자 암호 : " + adminPw);
		
		logger.info("넘어온 이자율 : " + balance);
		adminService.balanceUp(balance);
		
		HashMap<String, Object> result = new HashMap<String, Object>();
		result.put("msg", "반영되었는지 확인 바랍니다.");
		
		return result;
	}
	
	
	@RequestMapping(value="/mail", headers="Accept=application/json;charset=UTF-8",
			produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public HashMap<String,Object> mail(
			@RequestParam(name="emailTitle") String title,
			@RequestParam(name="emailContent") String Content) throws Exception{
		
		logger.info("넘어온 제목: " + title);
		
		logger.info("넘어온 내용 : " + Content);
		
		mailService.adminMail(title, Content);
		
		
		HashMap<String, Object> result = new HashMap<String, Object>();
		result.put("msg", "전송 요청 처리 하였습니다.");
		
		return result;
	}
	@RequestMapping(value="/boardDelete", method={RequestMethod.GET,RequestMethod.POST})
	public String boardDelete(String[] boardNos) {
		
		List<String> list = new ArrayList<String>();

		for (String a : boardNos) {
			list.add(a);
		}
		
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("boardNo", list);
		
		
		try {
			adminService.boardDelete(param);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "redirect:../board/list";
	}
	
	@RequestMapping(value="/boardDeleteOne", method={RequestMethod.GET,RequestMethod.POST})
	public String boardDeleteOne(int boardNo, Model model,
			@RequestParam(defaultValue="1") int pageNum,
			BoardVO boardVO) {
		
		int reLevel = boardVO.getReLevel()+1;
		boardVO.setReLevel(reLevel);
		boardVO.setState(1);
		
		try {
			adminService.boardDeleteOne(boardVO);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return "redirect:../board/list?bno="+boardVO.getBno()+"&pageNum"+pageNum;
	}
}
