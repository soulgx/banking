package com.bank.goldmoon.controller;

import java.io.PrintWriter;
import java.sql.Timestamp;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.bank.goldmoon.security.CustomUserDetails;
import com.bank.goldmoon.service.UserService;
import com.bank.goldmoon.vo.UserVO;


@Controller
@RequestMapping("/user")
public class UserController {

	private static final Logger logger = LoggerFactory.getLogger(UserController.class);

	@Autowired
	private UserService userService;

	@RequestMapping("/login")
	public void login(HttpSession session, String login, Model model) {
        logger.info("login", session.getId());
        model.addAttribute("login", login);
    }

	@RequestMapping(value = "/login_success", method = RequestMethod.GET)
    public String loginSuccess(HttpSession session) {
        CustomUserDetails userDetails = (CustomUserDetails)SecurityContextHolder.getContext().getAuthentication().getDetails();
         
        logger.info("login success. ", session.getId(), userDetails.getUsername() + "/" + userDetails.getPassword());
        session.setAttribute("userId", userDetails.getUsername());
        session.setAttribute("authorize", userDetails.getAuthorize());  
        session.setAttribute("userName", userDetails.getName());
        return "redirect:../";
    }
	
	@RequestMapping(value = "/denied", method = RequestMethod.GET)
	public ModelAndView denied() {
		logger.info("권한에 만족하지 못하는 사용자");
        ModelAndView mav = new ModelAndView();
        mav.addObject("msg", "접근 불가능한 페이지 입니다");
		mav.addObject("url", "javascript:history.back();");
		mav.setViewName("result");
		return mav;
	}
	
	@RequestMapping(value = "/login_duplicate", method = RequestMethod.GET)
    public ModelAndView loginDuplicate(HttpSession session) {    
        logger.info("login_duplicate.");
        session.removeAttribute("userInfo");
        ModelAndView mav = new ModelAndView();
        mav.addObject("msg", "중복로그인 되어 로그아웃 되었습니다.");
		mav.addObject("url", "../");
		mav.setViewName("result");
		return mav;
    }

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(HttpSession session) {
		//session.removeAttribute("userInfo");
		session.invalidate();
		return "redirect:../";
	}
	
	@Secured("ROLE_ANONYMOUS")
	@RequestMapping(value = "/join", method = RequestMethod.GET)
	public void join() {
	}

	@Secured("ROLE_ANONYMOUS")
	@RequestMapping(value = "/join", method = RequestMethod.POST)
	public ModelAndView join(@Valid UserVO userVO, BindingResult br, 
			String user_zipcode1, String user_zipcode2, 
			String phone1, String phone2, String phone3) {
		ModelAndView mav = new ModelAndView();

		userVO.setUserPw(DigestUtils.md5Hex(userVO.getUserPw()));
		userVO.setUserPhone(phone1 + "-" + phone2 + "-" + phone3);
		userVO.setUserZipcode(user_zipcode1 + "-" + user_zipcode2);
		userVO.setUserRegDate(new Timestamp(System.currentTimeMillis()));
		logger.info(userVO.toString());
		
		if(br.hasErrors()) {
			mav.setViewName("user/join");
			return mav;
		}

		try {
			userService.getJoin(userVO);
			mav.addObject("msg", "가입성공");
			mav.addObject("url", "login");
			mav.setViewName("result");
		} catch (Exception e) {
			e.printStackTrace();
			mav.addObject("msg", "가입실패");
			mav.addObject("url", "javascript:history.back();");
			mav.setViewName("result");
		}

		return mav;
	}
	
	@Secured({"ROLE_ADMIN", "ROLE_USER"})
	@RequestMapping(value = "/update", method = RequestMethod.GET)
	public ModelAndView update(HttpSession session) {
		ModelAndView mav = new ModelAndView();
		CustomUserDetails userDetails = (CustomUserDetails)SecurityContextHolder.getContext().getAuthentication().getDetails();

		try {
			UserVO userInfo = userService.getInfoUser(userDetails.getUsername());

			String[] tel = userInfo.getUserPhone().split("-");
			String[] zipcode = userInfo.getUserZipcode().split("-");

			mav.addObject("userInfo", userInfo);
			mav.addObject("tel", tel);
			mav.addObject("zipcode", zipcode);
			mav.setViewName("user/update");
		} catch (Exception e) {
			e.printStackTrace();
		}

		return mav;
	}
	
	@Secured({"ROLE_ADMIN", "ROLE_USER"})
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ModelAndView update(String user_zipcode1, String user_zipcode2, String phone1, String phone2, String phone3,
			UserVO userVO) {
		CustomUserDetails userDetails = (CustomUserDetails)SecurityContextHolder.getContext().getAuthentication().getDetails();
		userVO.setUserId(userDetails.getUsername());
		userVO.setUserPw(DigestUtils.md5Hex(userVO.getUserPw()));
		userVO.setUserPhone(phone1 + "-" + phone2 + "-" + phone3);
		userVO.setUserZipcode(user_zipcode1 + "-" + user_zipcode2);
		logger.info(userVO.toString());

		ModelAndView mav = new ModelAndView();
		mav.setViewName("result");

		try {
			userService.getUserUpdate(userVO);
			mav.addObject("msg", "수정성공");
			mav.addObject("url", "../");
		} catch (Exception e) {
			mav.addObject("msg", "수정실패");
			mav.addObject("url", "javascript:history.back();");
		}

		return mav;
	}
	
	@RequestMapping(value="idcount")
	@ResponseBody
	public void idCheck(String userId,PrintWriter pw){
		int count=0;
		try {
			count = userService.idCheck(userId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(count==0){
			pw.println("true");
		}else{
			pw.println("false");
		}
	}

}
