package com.bank.goldmoon.controller;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bank.goldmoon.security.CustomUserDetails;
import com.bank.goldmoon.service.AccountService;
import com.bank.goldmoon.vo.AccountInfoVO;
import com.bank.goldmoon.vo.DealingsInfoVO;
import com.bank.goldmoon.vo.PageVO;
import com.bank.goldmoon.vo.TransferInfoVO;

@Controller
@RequestMapping("/account")
public class AccountController {
	
	@Autowired
	private AccountService accountService;
	
	private static final Logger logger = LoggerFactory.getLogger(AccountController.class);

		
	@Secured({"ROLE_ADMIN", "ROLE_USER"}) 
	@RequestMapping("/regist")
	public String AccountRegist(){
		return "account/regist";
	}
	
	@Secured({"ROLE_ADMIN", "ROLE_USER"})
	@RequestMapping(value="/regist",method=RequestMethod.POST)
	public String AccountRegistOk(Model model,AccountInfoVO aiVo){
		CustomUserDetails userDetails = (CustomUserDetails)SecurityContextHolder.getContext().getAuthentication().getDetails();

		aiVo.setAccountPw(DigestUtils.md5Hex(aiVo.getAccountPw()));
		aiVo.setOpenDate(new Timestamp(System.currentTimeMillis()));
		aiVo.setUserId(userDetails.getUsername());
		
		try {
			accountService.accountRegistOpen(aiVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "redirect:../account/accountList";
	}
	
	@Secured({"ROLE_ADMIN", "ROLE_USER"})
	@RequestMapping(value="accountList")
	public String aopAccountList(Model model){
		return "account/accountList";
	}
	
	@Secured({"ROLE_ADMIN", "ROLE_USER"})
	@RequestMapping(value="paying")
	public String aopAccountPaying(Model model){
		return "account/paying";
	}
	
	@Secured({"ROLE_ADMIN", "ROLE_USER"})
	@RequestMapping(value="paying",method=RequestMethod.POST)
	public String accountPayingOk(Model model, String accountPw, DealingsInfoVO dealingsInfoVO){
		
		dealingsInfoVO.setDealDate(new Timestamp(System.currentTimeMillis()));
		dealingsInfoVO.setCustomer("ATM기기");
		
		try {
			accountService.accountBreakDown(dealingsInfoVO);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:../account/accountList";
	}
	
	@Secured({"ROLE_ADMIN", "ROLE_USER"})
	@RequestMapping(value="choice",method=RequestMethod.GET)
	public String aopDealingList(Model model){
		return "account/choice";
	}
	
	/*
	@Secured({"ROLE_ADMIN", "ROLE_USER"})
	@RequestMapping(value="choice",method=RequestMethod.POST)
	public String dealingListOk(Model model,int accountNo,
			@RequestParam(defaultValue="1") int pageNum){
		int totalCount = 0;
		
		try {
			totalCount=accountService.dealingCount(accountNo);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		List<?> list = null;
		
		PageVO pageVO = new PageVO(10, pageNum, totalCount);
		
		HashMap<String,Object> param = new HashMap<String,Object>();
		param.put("startNum", pageVO.getStartNo());
		param.put("endNum", pageVO.getEndNo());
		param.put("accountNo", accountNo);
		try {
			list = accountService.dealingList(param);
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("dealInfo",list);
		model.addAttribute("accountNo",accountNo);
		model.addAttribute("pageInfo",pageVO);
		
		return "account/list";
	}
	*/
	
	@Secured({"ROLE_ADMIN", "ROLE_USER"})
	@RequestMapping(value="choice",method=RequestMethod.POST)
	public String dealingListOk(Model model,int accountNo,
			@RequestParam(defaultValue="1") int pageNum){
		int totalCount = 0;
		
		try {
			totalCount=accountService.dealingCount(accountNo);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		List<?> list = null;
		
		PageVO pageVO = new PageVO(10, pageNum, totalCount);
		
		HashMap<String,Object> param = new HashMap<String,Object>();
		param.put("startNum", pageVO.getStartNo());
		param.put("endNum", pageVO.getEndNo());
		param.put("accountNo", accountNo);
		try {
			list = accountService.dealingList2(param);
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("dealInfo",list);
		model.addAttribute("accountNo",accountNo);
		model.addAttribute("pageInfo",pageVO);
		
		return "account/list";
	}
	
	
	
	@Secured({"ROLE_ADMIN", "ROLE_USER"})
	@RequestMapping(value="/modal",
			headers="Accept=application/json;charset=UTF-8", 
			produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<?> accountModal(DealingsInfoVO div) throws Exception{
		return accountService.accountModal(div);
	}
	
	@Secured({"ROLE_ADMIN", "ROLE_USER"})
	@RequestMapping(value="CreditTransfer")
	public String aopCreditTransfer(Model model){
		
		List<?> biv = null;
		try {
			biv = accountService.bankList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("bankingList",biv);
		
		return "account/creditTransfer";
	}
	
	@Secured({"ROLE_ADMIN", "ROLE_USER"})
	@RequestMapping(value="CreditTransfer",method=RequestMethod.POST)
	public String creditTransfer(Model model, TransferInfoVO tiv){
		int remainMoney = 0;
		
		try {
			remainMoney = accountService.getRemainMoney(tiv.getAccountNo());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		DealingsInfoVO myDiv = new DealingsInfoVO(0, new Timestamp(System.currentTimeMillis()), 0, tiv.getMoney(), tiv.getContent(), remainMoney-tiv.getMoney(), "", tiv.getCustAccountNo(), tiv.getAccountNo(), tiv.getBankingNo());
		DealingsInfoVO youDiv = new DealingsInfoVO(0, new Timestamp(System.currentTimeMillis()), 0, tiv.getMoney(), tiv.getContent(), 0, tiv.getUserName(), tiv.getAccountNo(), tiv.getCustAccountNo(), tiv.getBankingNo());
		if(myDiv.getBankingNo()==1){
			myDiv.setPayingNo(4);
			youDiv.setPayingNo(3);
			
			HashMap<String,Object> youInfo = accountService.getOther(youDiv.getAccountNo());
			youInfo.get("USERNAME");
			int youRemainMoney=Integer.parseInt(youInfo.get("ACCOUNTMONEY").toString());
			
			myDiv.setCustomer((String)youInfo.get("USERNAME"));
			youDiv.setRemainMoney(youRemainMoney+myDiv.getMoney());
			
		}else{
			myDiv.setPayingNo(6);
			youDiv.setPayingNo(5);
			
			String youUserName = accountService.paserAccount(youDiv);
			myDiv.setCustomer(youUserName);
		}
		
		try {
			accountService.creditTransfer(myDiv,youDiv);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "redirect:../account/accountList";
	}
	
	@RequestMapping(value="/actselect",
			headers="Accept=application/json;charset=UTF-8", 
			produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public HashMap<String,Object> accountInfo(int accountNo,String apiKey){
		HashMap<String,Object> ain = null;
		int keyCount = 0;
		
		try {
			keyCount = accountService.getKey(apiKey);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		if(keyCount==1){
			try {
				ain=accountService.accountInfoNo(accountNo);
				ain.put("ACCOUNTNO", accountNo);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}else{
			ain = new HashMap<String, Object>();
			ain.put("ACCOUNTNO", 0);
			ain.put("USERID", "errer");
			ain.put("USERNAME", "errer");
		}
		
		return ain;
	}
	
	@RequestMapping(value="/transfer",
			headers="Accept=application/json;charset=UTF-8", 
			produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public HashMap<String,Object> tansfer(int accountNo, int youaccount, String name,
			int money, String msg, int banknum, String apiKey){
		
		HashMap<String,Object> ain= new HashMap<String, Object>();
		int keyCount = 0;
		
		try {
			keyCount = accountService.getKey(apiKey);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		if(keyCount==1){
			DealingsInfoVO div = new DealingsInfoVO(0, new Timestamp(System.currentTimeMillis()),
					5, money, msg, 0, name, youaccount, accountNo, banknum);
			try {
				accountService.accountBreakDown(div);
				ain.put("COUNT", 1);
				ain.put("msg", "success");
			} catch (Exception e){ 
				e.printStackTrace();
				ain.put("COUNT", 0);
				ain.put("msg", "failed");				
			}
		}		
		return ain;
		
	}
	
	
	
}
