package com.bank.goldmoon.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.bank.goldmoon.vo.DealingsInfoVO;


@Component
public class ParserAccount {
	// ----------- 싱글톤 객체 생성 시작 ----------
	private static ParserAccount httpJson = null;

	public static ParserAccount getInstance() {
		
		if (httpJson == null) {
			httpJson = new ParserAccount();
		}
		return httpJson;
	}

	public static void freeInstance() {
		httpJson = null;
	}

	private ParserAccount() {
		super();
	}


	// ----------- 싱글톤 객체 생성 끝 ----------
	
	public HashMap<String, Object> youAccountInfo(String param1, String param2) {
		
		InputStream is = null;
		BufferedReader reader = null;
		String source = null;
		String line = null;
		StringBuilder sb = new StringBuilder();
		HashMap<String, Object> youAccountInfo = null;
		try {
			int timeout = 10;
			RequestConfig config = RequestConfig.custom()
			  .setConnectTimeout(timeout * 1000)
			  .setConnectionRequestTimeout(timeout * 1000)
			  .setSocketTimeout(timeout * 1000).build();
			
			CloseableHttpClient httpclient = 
			  HttpClientBuilder.create().setDefaultRequestConfig(config).build();
			
			
			HttpPost httpPost = new HttpPost("http://192.168.0.120:8080/newworld/account/actselect");
			// 전달하고자 하는 PARAMETER를 List객체에 담는다
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			nvps.add(new BasicNameValuePair("accountNo", param1));
			nvps.add(new BasicNameValuePair("apiKey", param2));
			// UTF-8은 한글
			httpPost.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));
			CloseableHttpResponse response = httpclient.execute(httpPost);
			
			
			try {

				int resultCode = response.getStatusLine().getStatusCode();
				
				if (resultCode == HttpURLConnection.HTTP_OK) {
					HttpEntity entity = response.getEntity();
					BufferedHttpEntity buffer = new BufferedHttpEntity(entity);
					is = buffer.getContent();
					reader = new BufferedReader(new InputStreamReader(is, "utf-8"));

					while ((line = reader.readLine()) != null) {
						sb.append(line + "\n");
					}
					source = sb.toString();
					
					JSONObject json = new JSONObject(source);
					
					int accountNo = json.getInt("ACCOUNTNO");
					String userName = json.getString("USERNAME");
					//String userId = json.getString("USERID");
					//String message = json.getString("message");
					
					youAccountInfo = new HashMap<String, Object>();
					youAccountInfo.put("ACCOUNTNO", accountNo);
					youAccountInfo.put("USERNAME", userName);
					//youAccountInfo.put("MESSAGE", message);
					
				} else {
					
				}
				
				
			} finally {
				
				if (response != null) {
					try {
						response.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				
				
				if (reader != null) {
					try {
						reader.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

				if (is != null) {
					try {
						is.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				
			}
		} catch (ConnectTimeoutException cte) {
			//cte.printStackTrace();
			System.out.println("제한 시간 초과");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return youAccountInfo;
	}
	
	public int transfer(DealingsInfoVO youDiv, String bankNo) {
		InputStream is = null;
		BufferedReader reader = null;
		String source = null;
		String line = null;
		StringBuilder sb = new StringBuilder();
		int count = -1;
		try {
			CloseableHttpClient httpclient = HttpClients.createDefault();
			HttpPost httpPost = new HttpPost("http://192.168.0.120:8080/newworld/account/transfer");
			// 전달하고자 하는 PARAMETER를 List객체에 담는다
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			nvps.add(new BasicNameValuePair("accountNo", youDiv.getAccountNo()+""));
			nvps.add(new BasicNameValuePair("youaccount", youDiv.getCustAccountNo()+""));
			nvps.add(new BasicNameValuePair("name", youDiv.getCustomer()));
			nvps.add(new BasicNameValuePair("money", youDiv.getMoney()+""));
			nvps.add(new BasicNameValuePair("msg", youDiv.getContent()+""));
			nvps.add(new BasicNameValuePair("banknum", youDiv.getBankingNo()+""));
			nvps.add(new BasicNameValuePair("apiKey", bankNo));
			
			// UTF-8은 한글
			httpPost.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));
			CloseableHttpResponse response = httpclient.execute(httpPost);

			try {

				int resultCode = response.getStatusLine().getStatusCode();

				if (resultCode == HttpURLConnection.HTTP_OK) {
					HttpEntity entity = response.getEntity();
					BufferedHttpEntity buffer = new BufferedHttpEntity(entity);
					is = buffer.getContent();
					reader = new BufferedReader(new InputStreamReader(is, "utf-8"));

					while ((line = reader.readLine()) != null) {
						sb.append(line + "\n");
					}
					source = sb.toString();
					
					JSONObject json = new JSONObject(source);
					
					count = json.getInt("COUNT");
					//String message = json.getString("msg");

					
				} else {
					count = 0;
				}
				
				
			} finally {
				
				if (response != null) {
					try {
						response.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				
				
				if (reader != null) {
					try {
						reader.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

				if (is != null) {
					try {
						is.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return count;
	}
}
