<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!doctype html>
<html>
<head>
	<meta charset="utf-8" />
	<title>My JSP Page</title>
	<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap.css" />" />
	<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/main.css" />" />
	<script src="<c:url value="/resources/js/jquery-1.11.3.js" />"></script>
	<script src="<c:url value="/resources/js/bootstrap.js" />"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$("#total_btn").on("click",function(){
			$("#total_menu").slideToggle("normal");
			return false;
		});
	
		$("#total_close a").on("click",function(){
			$("#total_menu").slideUp("fast");
			return false;
		});
		
		$("#login").on("click", function(){
			location.href='<c:url value="/user/login" />';
		})
		
	});
	</script>
</head>
<body>

	<c:import url="/resources/base/nav.jsp"></c:import>
	
	<c:if test="${no == 1}">
		<c:import url="/resources/etc/ceo.jsp"></c:import>
	</c:if>
	<c:if test="${no == 2}">
		<c:import url="/resources/etc/map.jsp"></c:import>
	</c:if>
	<c:if test="${no == 3}">
		<c:import url="/resources/etc/talented.jsp"></c:import>
	</c:if>
	<c:if test="${no == 4}">
		<c:import url="/resources/etc/history.jsp"></c:import>
	</c:if>
	<c:if test="${sno == 1}">
		<c:import url="/resources/etc/securitykind.jsp"></c:import>
	</c:if>
	<c:if test="${sno == 2}">
		<c:import url="/resources/etc/securityinfo.jsp"></c:import>
	</c:if>

    <hr/>  
    <c:import url="/resources/base/footer.jsp"></c:import>

	
</body>
</html>