<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ page session="true" %>
<html>
<head>
<c:import url="/resources/base/js.jsp"></c:import>
<style type="text/css">
#modalBack {
	background:url(<c:url value="/resources/img/goldmoonlogo.png" />);
	background-repeat: no-repeat;
	background-size : 100% 100%;
	width : 530px;
	height: 400px;
	line-height: 40px;
}


</style>
<script type="text/javascript">
var info = "";
$("document").ready(function(){
	
	$("#total_btn").on("click",function(){
		$("#total_menu").slideToggle("normal");
		return false;
	});

	$("#total_close a").on("click",function(){
		$("#total_menu").slideUp("fast");
		return false;
	});
	
	$("#login").on("click", function(){
		location.href='<c:url value="/user/login" />';
	});
	
	
	

	$('.paging').click(function() {
		$('#pageNum')[0].value =  $(this).attr('href');
		$('#accountNo')[0].value = ${accountNo};
		document.frm.method="POST";
		document.frm.submit();
		return false;
	});
	
	
	$('button[data-toggle="modal"]').on('click', function(){
		
		var userId = '${sessionScope.userId}';
		var dealingsNo = $(this)[0].value;
		var accountNo = ${accountNo};
		var csrf= "${_csrf.token}";
		 $.ajax({
			url:'<c:url value="/account/modal" />',
			type:'POST',
			data:{
				'userId':userId,
				'dealingsNo':dealingsNo,
				'accountNo'	 :accountNo,
				'_csrf' : csrf
			},
			dataType: "json",
			success:function(data){
				console.dir(data);
				
				info='<div id="modalBack">';
				$.each(data, setModal);
				info+='</div>';
				$(".modal-body").html(info);
			}
		}); 
		 
		 
		
	});
	
		 
});

function setModal(){
	var date = new Date(this['DEALDATE']);
	var result = dateFormat(date);
	
	
	
	info += '<p class="modalfont">계좌번호 : '+ this['ACCOUNTNO'] +'</p>';						
	info += '<p class="modalfont">거래내용 : '+ this['CONTENT'] +'</p>';
	info += '<p class="modalfont">거래유형 : '+ this['DEPOSITWITHDRAWALINFO'] +'</p>'; 
	info += '<p class="modalfont">거래금액 : '+ this['MONEY'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") +'원</p>';						
	info += '<p class="modalfont">잔액 : '+ this['REMAINMONEY'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") +'원</p>';
	info += '<p class="modalfont">거래처 : '+ this['CUSTOMER'] +'</p>';
	info += '<p class="modalfont">상대은행 : '+ this['BANKINGNAME'] +'</p>';						
	info += '<p class="modalfont">상대계좌번호 : '+ this['CUSTACCOUNTNO'] +'</p>';
	info += '<p class="modalfont">거래일시 : '+ result +'</p>';
	info += '<p id="modalText" class="modalfont">골드문 은행장 이자성</p>';
	
}

function dateFormat(date) {
	var yy = date.getFullYear();
	var mm = date.getMonth()+1;
	var dd = date.getDate();
	var hh = date.getHours();
	var mi = date.getMinutes();
	var ss = date.getSeconds();
	if (mm < 10) {
		mm = "0"+mm;
	}
	if (dd < 10) {
		dd = "0"+dd;
	}
	if (hh < 10) {
		hh = "0"+hh;
	}
	if (mi < 10) {
		mi = "0"+mi;
	}
	if (ss < 10) {
		ss = "0"+ss;
	}
	
	return yy + "년 "+ mm + "월 "+ dd + "일 " + hh + "시 "+ mi +"분 " + ss +"초";
}




</script>
<style type="text/css">


.info {
	font-weight: bold;
}

.info > td {
	text-align: center;
	background-color: #ededed;
}
</style>
</head>

<body>
	<c:import url="/resources/base/nav.jsp"></c:import>
	<div id=dbody>
	<c:import url="/resources/base/leftnav.jsp"></c:import>
	<div id="container">
	<h2 id="transferTitle">거래 상세 내역</h2>
	<hr />
	<table class="table table-bordered table-hover" id=accountTable>
		<thead>
		<tr class="info">
			<td>거래처</td>
			<td>거래내용</td>
			<td>거래유형</td>
			<td>입금액</td>
			<td>출금액</td>
			<td>남은금액</td>
			<td>시간</td>
			<td>상세내역</td>
		</tr>
		</thead>
		<tbody>
		<c:choose>
	
			<c:when test="${dealInfo.size() != 0}">
				<c:set var="sumDMoney" value="0" />
				<c:set var="sumWMoney" value="0" />
				<c:set var="sumRemainMoney" value="0" />
				<c:forEach items="${dealInfo}" var="map" varStatus="st">
				<c:set var="sumDMoney" value="${sumDMoney=sumDMoney+map.DMONEY}" />
				<c:set var="sumWMoney" value="${sumWMoney=sumWMoney+map.WMONEY}" />
				<c:set var="ACCOUNTMONEY" value="${map.ACCOUNTMONEY}" />
					<tr>
						<td>${map.CUSTOMER}</td>
						<td>${map.CONTENT}</td>
						<td>${map.DEPOSITWITHDRAWALINFO}</td>
						<td><fmt:formatNumber value="${map.DMONEY}" pattern="##,###.##" /></td>
						<td><fmt:formatNumber value="${map.WMONEY}" pattern="##,###.##" /></td>
						<td><fmt:formatNumber value="${map.REMAINMONEY}" pattern="##,###.##" /></td>
						<td><fmt:formatDate value="${map.DEALDATE}" pattern="yyyy-MM-dd HH:mm"></fmt:formatDate></td>
						<td style="text-align: center;"><button type="button" class="btn" data-toggle="modal" data-target="#myModal" value="${map.DEALINGSNO}"><i class="icon-list"></i></button></td>
					</tr>		
				</c:forEach>
					<tr>
						<td colspan="3" style="text-align: center;">합계</td>
						<td><fmt:formatNumber value="${sumDMoney}" pattern="##,###.##" /></td>
						<td><fmt:formatNumber value="${sumWMoney}" pattern="##,###.##" /></td>
						<td colspan="2" style="text-align: center;">남은 금액</td>
						<td><fmt:formatNumber value="${ACCOUNTMONEY}" pattern="##,###.##" /></td>
						
					</tr>
			</c:when>
			<c:otherwise>
				<tr>
					<td colspan="7" style="text-align: center;">거래 내역이 없습니다.</td>
				</tr>
			</c:otherwise>
		</c:choose>
		
		
		
		
		</tbody>
	</table>
	
	<div class="pagination pagination-centered">
	<form name="frm">
	<sec:csrfInput/>
	<input type="hidden" id="pageNum" name="pageNum" />
	<input type="hidden" id="accountNo" name="accountNo" />
	<input type="hidden" id="userId" name="userId" value="${sessionScope.userId}" />
		<c:if test="${dealInfo.size() != 0}">
		<ul>
		<c:choose>
		
			<c:when test="${pageInfo.currentPage <= 1}">
        	<li class="disabled"><span>&laquo;</span></li>
 			</c:when>
 			
 			<c:otherwise>
 			<li><a href="${pageInfo.currentPage-1}" class="paging">&laquo;</a></li>
 			</c:otherwise>
 		 </c:choose>
 		

		 
		<c:forEach var="i" begin="${pageInfo.numberStart}" end="${pageInfo.numberEnd}">
			
		<c:choose>
			
        	<c:when test="${i==pageInfo.currentPage}">

        	<li class="active"><span >${i}</span></li>
 			
 			</c:when>
 			
 			<c:otherwise>
 			<li><a href="${i}" class="paging">${i}</a></li>
 			</c:otherwise>
 		 	
 		 </c:choose>
 		 
 		 </c:forEach>
 		 
 		 <c:choose>
        <c:when test="${pageInfo.currentPage == pageInfo.totalPage}">
        	<li><span>&raquo;</span></li>
 		</c:when>
 		
 		<c:otherwise>
 			<li><a href="${pageInfo.currentPage+1}" class="paging" >&raquo;</a></li>
 		</c:otherwise>
 		 </c:choose>
		</ul>
		</c:if>
	</form>	
	</div>
	
	
	
	<!-- Modal -->
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">상세내역</h3>
  </div>
  <div class="modal-body">
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">닫기</button>
  </div>
</div>
	
	
	<br />
	</div>
	</div>
	<div class="clear"></div>
<hr/>	
<c:import url="/resources/base/footer.jsp"></c:import>	
</body>
</html>