<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ page session="true" %>
<!doctype html>
<html>
<head>
<c:import url="/resources/base/js.jsp"></c:import>
<script src="<c:url value="/resources/js/jquery.validate.js" />"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$("#total_btn").on("click", function() {
			$("#total_menu").slideToggle("normal");
			return false;
		});

		$("#total_close a").on("click", function() {
			$("#total_menu").slideUp("fast");
			return false;
		});

		$("#login").on("click", function() {
			location.href = '<c:url value="/user/login" />';
		})
		
		
		
		$("#signupForm").validate({
			rules : {
				money : {
					required : true,
					digits: true,
					minlength: 1
				},
				content : { 
					required : true
				},
				accountPw : {
					required : true,
					minlength : 4,
					maxlength : 6
				}
			},
			messages : {
				money : {
					required : "거래금액을 입력하시오",
					digits : "숫자로만 입력가능 합니다.",
					minlength : "최소 {0}글자 입니다."
				},
				content : {
					required : "내용을 입력하시오",
				},
				accountPw : {
					required : "비밀번호를 입력해 주세요.",
					minlength : "비밀번호는 최소 {0}글자 입니다.",
					maxlength : "비밀번호는 최대 {0}글자 입니다.",
				}
			}
		});
	});
</script>
</head>
<body>
	<c:import url="/resources/base/nav.jsp"></c:import>
	<div id=dbody>
		<c:import url="/resources/base/leftnav.jsp"></c:import>

		<div id="container">
			<h2 id="transferTitle">계좌 예금/출금 서비스 안내</h2>
			<hr />
			<div id="transferInfo">
				<ul id="unstyled">
					<li>1년간 계좌 거래 내역이 없을 시 해당 계좌는 중지됩니다. 따라서 기간이 1년 경과하기 전에 본 서비스로
						유효기간을 연장할 경우 거래기능을 계속 이용하실 수 있습니다.</li>
					<li>계좌가 중지되신 고객은 본 서비스의 등록이 불가능합니다.</li>
					<li>거래를 이용할 경우 계좌 유효기간이 자동으로 연장되므로 본서비스에 별도로 등록하실 필요는 없습니다.</li>
				</ul>
			</div>
			<hr />
			<div id="transfer">
				<!-- 가입폼 시작 -->
				<form id="signupForm" name="paying" method="post" action="paying" role="form">
				<sec:csrfInput/>
					<input type="hidden" id="bankingNo" name="bankingNo" value="1" />
					<input type="hidden" id="custAccountNo" name="custAccountNo" value="0" />
					<table>
						<tr>
							<th>계좌 번호</th>
							<td><select name="accountNo" id="accountNo" class="span3">

									<c:forEach items="${accList}" var="map" varStatus="st">
										<option value="${map.ACCOUNTNO}">${map.ACCOUNTNO}
											/ 잔액 : <fmt:formatNumber value="${map.ACCOUNTMONEY}" pattern="##,###.##" /></option>
									</c:forEach>

							</select></td>
						</tr>

						<tr>
							<th>입/출금</th>
							<td><select name="payingNo"
								id="payingNo" class="span3">
									<option value="1">입금</option>
									<option value="2">출금</option>
							</select></td>
						</tr>

						<tr>
							<th>거래금액</th>
							<td><input type="number" name="money" id="money"
								class="span3" placeholder="숫자로만 입력하세요"/></td>
						</tr>

						<tr>
							<th>내용</th>
							<td><input type="text" name="content" id="content"
								class="span3" /></td>
						</tr>

						<tr>
							<th>계좌비밀번호</th>
							<td><input type="password" name="accountPw" id="accountPw"
								class="span3" /></td>
						</tr>




						<tr>
							<td colspan="2">
								<input type="submit" class="submit" value="확인" />
							</td>
						</tr>

					</table>
				</form>
			</div>
		</div>
	</div>
	<!-- 가입폼 끝 -->
	<hr/>
	<c:import url="/resources/base/footer.jsp"></c:import>
</body>
</html>