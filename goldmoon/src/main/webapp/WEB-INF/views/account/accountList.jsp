<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page session="true" %>
<!doctype html>
<html>
<head>
	<c:import url="/resources/base/js.jsp"></c:import>
	<script type="text/javascript">
	$(document).ready(function(){
		$("#total_btn").on("click",function(){
			$("#total_menu").slideToggle("normal");
			return false;
		});
	
		$("#total_close a").on("click",function(){
			$("#total_menu").slideUp("fast");
			return false;
		});
		
		$("#login").on("click", function(){
			location.href='<c:url value="/user/login" />';
		})
		
	});
	</script>	
</head>
<body>
	<c:import url="/resources/base/nav.jsp"></c:import>
	<div id=dbody>
		<c:import url="/resources/base/leftnav.jsp"></c:import>
		<div id="container">
			<h2 id="transferTitle">계좌 상세 내역 안내</h2>
			<hr />
			<div id="transferInfo">
				<ul id="unstyled">
					<li>현재 사용중인 계좌의 상세내역을 보여줍니다.</li>
					<li>거래중지계좌의 경우 현재 화면에서 보이지 않습니다.</li>
					<li>거래중지계좌 및 한도소액계좌는 해당 계좌 장기/ 미사용에 따른 "거래중지계좌"로 거래재개를 위해서는 창구에 방문하여 "금융거래목적 확인용 증빙서류"제출 및 서류 작성 하시기 바랍니다.</li>
				</ul>
			</div>
			
			<hr />
			<div>
			
			<c:forEach items="${accList}" var="map" varStatus="st">
				<table class="table">
					<tbody>
					<tr>
						<th>고객명</th>
						<td>${map.USERNAME}</td>
						<th>계좌 관리 아이디</th>
						<td>${map.USERID}</td>
					</tr>
					<tr>
						<th>계좌번호</th>
						<td>${map.ACCOUNTNO}</td>
						<th>거래횟수</th>
						<td>${map.ACCOUNTCOUNTS}건</td>
					</tr>
					<tr>
						<th>계좌잔액</th>
						<td><fmt:formatNumber value="${map.ACCOUNTMONEY}" pattern="##,###.##" />원</td>
						<th>계좌 생성일</th>
						<fmt:parseDate value="${map.OPENDATE}" pattern="yyyy-MM-dd HH:mm" var="date"></fmt:parseDate>
						<td><fmt:formatDate value="${date}" pattern="yyyy-MM-dd HH:mm"></fmt:formatDate></td>
					</tr>
					</tbody>
				</table>
				<hr />
			</c:forEach>

			</div>
		</div>	
	</div>
<div class="clear"></div>	
<hr />	
<c:import url="/resources/base/footer.jsp"></c:import>
</body>
</html>