<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<%@ page session="true" %>
<!doctype html>
<html>
<head>
<c:import url="/resources/base/js.jsp"></c:import>
<script type="text/javascript">
	$(document).ready(function() {
		$("#total_btn").on("click", function() {
			$("#total_menu").slideToggle("normal");
			return false;
		});

		$("#total_close a").on("click", function() {
			$("#total_menu").slideUp("fast");
			return false;
		});

		$("#login").on("click", function() {
			location.href = '<c:url value="/user/login" />';
		})
		
	});
</script>
</head>
<body>
	<c:import url="/resources/base/nav.jsp"></c:import>
	<div id=dbody>
		<c:import url="/resources/base/leftnav.jsp"></c:import>

		<div id="container">
			<h2 id="transferTitle">거래 내역 조회 안내</h2>
			<hr />
			<div id="transferInfo">
				<ul id="unstyled">
					<li>조회할 계좌를 선택하신 후 확인 버튼을 누르시면 빠른 서비스 조회가 가능합니다.</li>
					<li>계좌가 존재하더라도 거래내역이 1건이라도 없을 경우 이 서비스는 사용이 불가능합니다.</li>
					<li>해지된 계좌의 조회는 불가능 하므로 고객센터로 문의하시기 바랍니다.</li>
					<li>보안계좌서비스를 이용하시면 인터넷뱅킹/폰뱅킹/VM뱅킹/USIM뱅킹/스마트 폰뱅킹 등 이용시 조회 및 출금거래가 제한됩니다.</li>
					<li>입금가능시간 : 05:00 ~ 23:30 / 출금가능시간 : 05:00 ~ 23:30</li>
					<li>거래중지계좌 및 한도소액계좌는 해당 계좌 장기/ 미사용에 따른 "거래중지계좌"로 거래재개를 위해서는 창구에 방문하여 "금융거래목적 확인용 증빙서류"제출 및 서류 작성 하시기 바랍니다.</li>
				</ul>
			</div>
			<hr />
			<div id="transfer">
				<form name="listinfo" method="post" action="choice" role="form">
				<sec:csrfInput/>
					<div class="input-append">
						<input type="hidden" id="userId" name="userId" value="${sessionScope.userId}" />
						<input type="hidden" id="pageNum" name="pageNum" value="1" />
						<label class="control-label" for="appendedInputButton">계좌 번호 선택</label>
						<select class="span3" id="appendedInputButton" name="accountNo" id="accountNo">
								<c:forEach items="${accList}" var="map" varStatus="st">
									<option value="${map.ACCOUNTNO}">${map.ACCOUNTNO} / 잔액 : <fmt:formatNumber value="${map.ACCOUNTMONEY}" pattern="##,###.##" /></option>
								</c:forEach>
						</select>
						<button type="submit" class="btn">상세 조회</button>
					</div>	
				</form>
			</div>
		</div>
	</div>
	
	<!-- 가입폼 끝 -->
	<hr/>
	<c:import url="/resources/base/footer.jsp"></c:import>
</body>
</html>