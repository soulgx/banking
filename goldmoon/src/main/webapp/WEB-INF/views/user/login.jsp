<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false" %>
<!doctype html>
<html>
<head>

<c:import url="/resources/base/js.jsp"></c:import>
<script type="text/javascript">
	$(document).ready(function() {
		$("#total_btn").on("click", function() {
			$("#total_menu").slideToggle("normal");
			return false;
		});

		$("#total_close a").on("click", function() {
			$("#total_menu").slideUp("fast");
			return false;
		});

		$("#login").on("click", function() {
			location.href = '<c:url value="/user/login" />';
		})
	
		
	});
</script>
<style type="text/css">
	.alert-danger, .alert-error{
		width:808px;
	}
</style>
</head>
<body>

	<c:import url="/resources/base/nav.jsp"></c:import>
	<div id="loginDiv">
		<div class="imgBox">
			<img src="<c:url value="/resources/img/loginbanner.jpg" />" alt="" />
		</div>
		
		<div id="loginBox">
			<form name="myform" method="post" action='<c:url value="/loginProcess" />' role="form" class='form-inline'>
					<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
					<div id="loginId" class="form-group">
						<div class="inlineblock">
							<span>아이디</span>
						</div>
						<input type="text" name="id" id="userId" class="form-control" placeholder='user id' />
						<!-- <button id="loginBtn" type="submit" class="btn btn-primary">로그인</button>  -->
					</div>
					
					<div class="form-group">
						<div class="inlineblock">
							<span>사용자 암호</span>
						</div>
						<input type="password" name="pw" id="userPw" class="form-control" placeholder='password' />
						<button id="loginBtn" type="submit" class="btn btn-primary">로그인</button>
					</div>
					<br />
					<a id="joinBtn" href='<c:url value="/user/join" />' class="btn btn-success">회원가입</a>
			</form>
		</div>
		
		
		
		<div id="confirm">
			<div id="confirmBox">
					<a href="#myModal" role="button" class="btn btn-large btn-warning" data-toggle="modal">공인인증서 로그인</a>
			</div>
			<br />
			<div id="confirmInfo">
				<a href='#' onclick="window.open('<c:url value="/idsearch" />','small','width=350,height=400,scrollbars=yes,menubar=yes')" class="btn btn-danger" target="_blank">아이디 찾기</a>
				<a href='#' class="btn btn-info" onclick="window.open('<c:url value="/pwcheck" />','small','width=350,height=400,scrollbars=yes,menubar=yes')" target="_blank">비밀번호 찾기</a>
			</div>
		</div>
		
		<div class="clear"></div>
		
		<c:if test="${login != null}">
		<div class="alert alert-error">
              <button type="button" class="close" data-dismiss="alert">&times;</button>
              <strong>로그인 실패</strong> 아이디나 비밀번호를 확인해주세요.
        </div>
        </c:if>
		
		<div class="imgBox">
			<img src="<c:url value="/resources/img/loginbanner2.png" />" alt="" />
		</div>
		<!-- Modal -->
		<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		  <div class="modal-header">
		    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		    <h3 id="myModalLabel">공인인증서 로그인</h3>
		  </div>
		  <div class="modal-body">
		  <h3>안내</h3>
		  <p>공인인증서 시스템이 미구현 상태이므로 아이디와 패스워드를 입력하여 로그인 하시기 바랍니다.</p>
		    <div id="loginBox">
			<form name="myform" method="post" action="<c:url value="/loginProcess" />" role="form" class='form-inline'>
					<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
					<div id="loginId" class="form-group">
						<div class="inlineblock">
							<span>아이디</span>
						</div>
						<input type="text" name="id" id="userId" class="form-control" placeholder='user id' />
						<!-- <button id="loginBtn" type="submit" class="btn btn-primary">로그인</button>  -->
					</div>
					
					<div class="form-group">
						<div class="inlineblock">
							<span>사용자 암호</span>
						</div>
						<input type="password" name="pw" id="userPw" class="form-control" placeholder='password' />
						<button id="loginBtn" type="submit" class="btn btn-primary">로그인</button>
					</div>
					<br />
					<a id="joinBtn" href='<c:url value="/user/join" />' class="btn btn-success">회원가입</a>
			</form>
		</div>
		
		  </div>
		  <div class="modal-footer">
		    <button class="btn" data-dismiss="modal" aria-hidden="true">닫기</button>
		  </div>
		</div>
	</div>
	<hr/>
	<c:import url="/resources/base/footer.jsp"></c:import>

</body>
</html>