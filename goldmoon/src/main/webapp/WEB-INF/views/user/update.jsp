﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page session="true" %>
<!DOCTYPE html>
<html>
<head>
<c:import url="/resources/base/js.jsp"></c:import>
<script src="<c:url value="/resources/js/jquery.validate.js" />"></script>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script src="<c:url value="/resources/js/openDaumPostcode.js" />"></script>
<script type="text/javascript">
	
	
	/* $.validator.addMethod("password", function(value, element){
		return this.optional(element) || /^(?=.*\d)(?=.*[a-zA-Z]).{6,15}$/i.test(value);
	}, "비밀번호는 6자리~15자리 영문 숫자 조합으로 입력하세요.");

	$.validator.addMethod("userid", function(value, element){
		return this.optional(element) || /^(?=.*\d)(?=.*[a-zA-Z]).{5,15}$/i.test(value);
	}, "아이디는 5자리~15자리 영문 숫자 조합으로 입력하세요.");  */
 
	$(document).ready(function(){
		
		$("#total_btn").on("click",function(){
			$("#total_menu").slideToggle("normal");
			return false;
		});
	
		$("#total_close a").on("click",function(){
			$("#total_menu").slideUp("fast");
			return false;
		});
		
		$("#login").on("click", function(){
			location.href='<c:url value="/user/login" />';
		})
		
		
		
		
		$('input[value="취소"]').on('click', function(){
			history.back();
		});
		
		
		/* $("#updateForm").validate({
			rules : {
				
				user_pwd : {
					required : true,
					password: true
				},
				user_re_pwd : {
					required : true,
					equalTo : "#user_pwd"
				},
				user_pwd_hint : "required",
				user_pwd_answer : "required",
				user_email : {
					required : true,
					email : true
				},
				phone2 : {
					required : true,
					digits : true,
					minlength : 3,
					maxlength : 4
				},
				phone3 : {
					required : true,
					digits : true,
					minlength : 4,
					maxlength : 4
				},
				user_zipcode1 : {
					required : true
				},
				user_zipcode2 : {
					required : true
				},
				user_addr1 : {
					required : true
				},
				user_addr2 : {
					required : true
				}
			},
			messages : {
				
				user_pwd : {
					required : "비밀번호를 입력해 주세요."
				},
				user_re_pwd : {
					required : "비밀번호 확인값을 입력해 주세요.",
					equalTo : "비밀번호 확인이 잘못되었습니다."
				},
				user_pwd_hint : "비밀번호 찾기 질문은 필수 입력 항목 입니다.",
				user_pwd_answer : "비밀번호 찾기 답은 필수 입력 항목 입니다.",
				user_email : {
					required : "이메일을 입력해 주세요.",
					email : "이메일이 형식에 맞지 않습니다."
				},
				phone2 : {
					required : null,
					digits : null,
					minlength : null,
					maxlength : null
				},
				phone2 : {
					required : null,
					digits : null,
					minlength : null,
					maxlength : null
				},
				phone3 : {
					required : "연락처를 입력하시오",
					digits : "숫자로만 입력가능 합니다.",
					minlength : "최소 {0}글자 입니다.",
					maxlength : "최대 {0}글자 입니다."
				},
				user_zipcode1 : {
					required : ""
				},
				user_zipcode2 : {
					required : "우편번호 입력하시오"
				},
				user_addr1 : {
					required : "주소를 입력하세요"
				},
				user_addr2 : {
					required : "상세주소를 입력하세요"
				}
			}
		}); */
	});

	</script>
</head>
<body>
	<c:import url="/resources/base/nav.jsp"></c:import>
	<div id=dbody>
		<c:import url="/resources/base/leftnav.jsp"></c:import>
		<div id=container>
		
			<h2>회원 정보 수정</h2>
		
			<!-- 가입폼 시작 -->
			<form name="myform" id="updateForm" method="post" action="update" role="form">
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
				<table class="table table-striped">
					<tr>
						<th>이름</th>
						<td><input type="hidden" id="userName" name="userName"value="${userInfo.userName}" />${userInfo.userName}</td>
					</tr>
	
					<tr>
						<th>비밀번호</th>
						<td><input type="password" id="userPw" name="userPw" class="input-small"/></td>
					</tr>
					
					<tr>
						<th>비밀번호 확인</th>
						<td><input type="password" id="user_re_pwd" name="user_re_pwd" class="input-small" /></td>
					</tr>
					
					<tr>
						<th>비밀번호 찾기 질문</th>
						<td><input type="text" name="userPwdHint" id="userPwdHint"  value="${userInfo.userPwdHint}" /></td>
					</tr>
					
					<tr>
						<th>비밀번호 찾기 답</th>
						<td><input type="text" name="userPwdAnswer" id="userPwdAnswer" value="${userInfo.userPwdAnswer}" /></td>
					</tr>
					
					<tr>
						<th>이메일</th>
						<td><input type="email" name="userEmail" id="userEmail" value="${userInfo.userEmail}" /><br /></td>
					</tr>
					
					<tr>
						<th>전화번호</th>
						<td>
							<select name="phone1" id="phone1" class="input-small">
								<option value="010" <c:if test="${tel[0] == 010}">selected="selected"</c:if>>010</option>
								<option value="011" <c:if test="${tel[0] == 011}">selected="selected"</c:if>>011</option>
								<option value="016" <c:if test="${tel[0] == 016}">selected="selected"</c:if>>016</option>
								<option value="017" <c:if test="${tel[0] == 017}">selected="selected"</c:if>>017</option>
								<option value="019" <c:if test="${tel[0] == 019}">selected="selected"</c:if>>019</option>
							</select>-
							<input type="text" name="phone2" id="phone2" class="input-mini" value="${tel[1]}" /> -
							<input type="text" name="phone3" id="phone3" class="input-mini" value="${tel[2]}" /></td>
					</tr>
					
					<tr>
						<th>우편번호</th>
						<td>
							<input type="number" name="user_zipcode1" id="post1" class="input-mini" value="${zipcode[0]}" readonly /> - 
							<input type="number" name="user_zipcode2" id="post2" class="input-mini" value="${zipcode[1]}" readonly /> 
							<input type="button" id="user_zipcodes" name="user_zipcodes" onclick='openDaumPostcode()' value="검색" />
						</td>
					</tr>
					
					<tr>
						<th>주소</th>
						<td><input type="text" name="userAddr1" id="addr" value="${userInfo.userAddr1}" readonly /></td>
					</tr>
					
					<tr>
						<th>상세주소</th>
						<td><input type="text" name="userAddr2" id="addr2" value="${userInfo.userAddr2}" /></td>
					</tr>
				</table>
			
				<input type="submit" class="btn"  value="수정 완료" />
				<input type="button" class="btn" value="취소" />
			</form>
		</div>
	</div>
	<div class="clear"></div>
	<hr />
	<c:import url="/resources/base/footer.jsp"></c:import>
</body>
</html>