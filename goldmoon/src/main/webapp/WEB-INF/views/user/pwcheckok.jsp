<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript">
	$(function(){
		$('#btn1').on('click', function(){
			location.href= '<c:url value="/pwcheck" />';
		});
		
	});

</script>
<style type="text/css">
#hint {
	margin-left: 55px;
}

#pw {
	margin-top: 30px;
	text-align: center;
}

#btn {
	margin-left: 40px;
}

#wronhid {
	margin-left: 40px;
}
#btn1{
	margin-left: 130px;
}

</style>

</head>
<c:import url="/resources/base/js.jsp"></c:import>
<body>
	<h4 id="pw">비밀번호 답찾기</h4>
	<hr />
	<c:if test="${pwhint == null }">
		<div id="wronhid">
			<address class="address">
				<p>
					<strong>GM Bank.</strong>입력한정보가 일치하지 않습니다.
				</p>
				<br> 문제가 지속될시<br> 아래에 있는 메일이나 전화번호로 <br> 문의해 주시길
				바랍니다. <br> <abbr title="Phone">P:</abbr> (123) 456-7890
			</address>

			<address class="address">
				<strong>GoldMoon</strong><br> <a href="mailto:#">first.last@example.com</a>
			</address>
		</div>
		<div>
			<input type="button" value="돌아가기" class = "btn btn-success" id="btn1" />
		</div>
	</c:if>


	<div id="hint">
		<c:if test="${pwhint != null }">
			<form action="pwsearchOk" method="POST">
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" /> <input type="hidden" name="userId"
					value="${userId}" /> <input type="hidden" name="userEmail"
					value="${userEmail}" />
				<fieldset>
					<label>비밀번호찾기 질문 <br />
					<h3>${pwhint}</h3></label> <input type="text" name="userPwdAnswer"
						placeholder="hint)사는곳,직업,전화번호"> <span class="help-block">질문에
						대한 답을 입력해주세요</span>

				</fieldset>
				<div id="btn">
					<button type="submit" class="btn btn-primary"  id="btn">입력</button>
				</div>
			</form>
		</c:if>
	</div>



</body>
</html>