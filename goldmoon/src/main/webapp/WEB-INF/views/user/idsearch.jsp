<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript">
	
</script>

<style type="text/css">
#head {
	text-align: center;
}

#id {
	margin-left: 50px;
}
#btn{
	margin-left: 25px;
}
</style>


</head>
<c:import url="/resources/base/js.jsp"></c:import>
<body>
	<div>
		<!-- head div  -->
		<div id="head">
			<h3>ID 찾기</h3>
			<hr />
		</div>
		<!-- head div end -->

		<div id="id">
			<form action="idcheck" method="POST" id="frm"  class="form-horizontal">
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
				<div class="control-group">
					<label class="control-label" for="inputEmail">Name</label>
					<div class="controls">
						<input type="text" id="userName" name="userName"
							placeholder="Name">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="inputPassword">Email</label>
					<div class="controls">
						<input type="email" id="userEmail" name="userEmail"
							placeholder="Email">
					</div>
				</div>
				<div id="btn" class="control-group">
					<button type="submit"  id="btn" class="btn btn-primary">전송</button>
					<button type="button"  id="btn1" class="btn btn-danger"  onClick='window.close()' >취소</button>
				</div>
			</form>
		</div>
	</div>
</body>
</html>