<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<c:import url="/resources/base/js.jsp"></c:import>
<style type="text/css">
#msg {
	text-align: center;
	margin-top: 30px;
}
#gm{
	margin-left: 80px;
	margin-top: 10px;
}
#btn{
	margin-left: 130px;
}
</style>
</head>
<body>
	<form>
		<div >
			<p id="msg"><font size="3" style="font-weight: bold">${msg}</font></p>
			<hr />
		</div>
		<div id="gm">
			<address>
				<strong>GM Bank.</strong><br> 아이디나 비밀번호가 <br> 확인되지 않을시에<br/>아래
				연락처로 연락바랍니다.<br> <abbr title="Phone">P:</abbr> (123) 456-7890
			</address>

			<address>
				<strong>GoldMoon</strong><br> <a href="mailto:#">first.last@example.com</a>
			</address>
		</div>
	</form>
	
	<input type="button" value="닫기"  id="btn"class="btn btn-danger" onClick='window.close()' />

</body>
</html>