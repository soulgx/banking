<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="false" %>
<!doctype html>
<html>
<head>
	<c:import url="/resources/base/js.jsp"></c:import>
	<script src="<c:url value="/resources/js/jquery.validate.js" />"></script>
	<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
	<script src="<c:url value="/resources/js/openDaumPostcode.js" />"></script>
	<script type="text/javascript">
	
	
	$.validator.addMethod("password", function(value, element){
		return this.optional(element) || /^(?=.*\d)(?=.*[a-zA-Z]).{6,15}$/i.test(value);
	}, "비밀번호는 6자리~15자리 영문 숫자 조합으로 입력하세요.");

	$.validator.addMethod("userid", function(value, element){
		return this.optional(element) || /^(?=.*\d)(?=.*[a-zA-Z]).{5,15}$/i.test(value);
	}, "아이디는 5자리~15자리 영문 숫자 조합으로 입력하세요.");  
 
	$(document).ready(function(){
		
		$("#total_btn").on("click",function(){
			$("#total_menu").slideToggle("normal");
			return false;
		});
	
		$("#total_close a").on("click",function(){
			$("#total_menu").slideUp("fast");
			return false;
		});
		
		$("#login").on("click", function(){
			location.href='<c:url value="/user/login" />';
		})
		
		
		$('input[value="취소"]').on('click', function(){
			history.back();
		});
		
		
		$("#joinForm").validate({
			rules : {
				userId : {
					required : true,
					userid: true,
					remote : {
						url: "<c:url value='/user/idcount' />",
						type : "get"
					}
				},
				userName : {
					required : true,
					minlength : 2,
					maxlength : 6
				},
				userPw : {
					required : true,
					password: true
				},
				user_re_pw : {
					required : true,
					equalTo : "#userPw"
				},
				userPwdHint : "required",
				userPwdAnswer : "required",
				userEmail : {
					required : true,
					email : true
				},
				phone2 : {
					required : true,
					digits : true,
					minlength : 3,
					maxlength : 4
				},
				phone3 : {
					required : true,
					digits : true,
					minlength : 4,
					maxlength : 4
				},
				user_zipcode1 : {
					required : true
				},
				user_zipcode2 : {
					required : true
				},
				userAddr1 : {
					required : true
				},
				userAddr2 : {
					required : true
				}
			},
			messages : {
				userId : {
					required : "아이디를 입력해 주세요.",
					remote : "이미 있는 아이디입니다."
				},
				userName : {
					required : "이름을 입력해 주세요.",
					minlength : "이름은 최소 {0}글자 입니다.",
					maxlength : "이름은 최대 {0}글자 입니다."
				},
				userPw : {
					required : "비밀번호를 입력해 주세요."
				},
				user_re_pw : {
					required : "비밀번호 확인값을 입력해 주세요.",
					equalTo : "비밀번호 확인이 잘못되었습니다."
				},
				userPwdHint : "비밀번호 찾기 질문은 필수 입력 항목 입니다.",
				userPwdAnswer : "비밀번호 찾기 답은 필수 입력 항목 입니다.",
				userEmail : {
					required : "이메일을 입력해 주세요.",
					email : "이메일이 형식에 맞지 않습니다."
				},
				phone2 : {
					required : null,
					digits : null,
					minlength : null,
					maxlength : null
				},
				phone3 : {
					required : "연락처를 입력하시오",
					digits : "숫자로만 입력가능 합니다.",
					minlength : "최소 {0}글자 입니다.",
					maxlength : "최대 {0}글자 입니다."
				},
				user_zipcode1 : {
					required : ""
				},
				user_zipcode2 : {
					required : "우편번호 입력하시오"
				},
				userAddr1 : {
					required : "주소를 입력하세요"
				},
				userAddr2 : {
					required : "상세주소를 입력하세요"
				}
			}
		}); 
	});

	</script>	
</head>
<body>
<c:import url="/resources/base/nav.jsp"></c:import>
	<div id=dbody>
		<c:import url="/resources/base/leftnav.jsp"></c:import>
		<div id=container>
	<h2>회원가입</h2>
	
		<!-- 가입폼 시작 -->
		<form:form name="myform" id="joinForm" method="post" action="join" role="form" commandName="userVO">
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
			<table class="table table-striped">
<tr>
	<th>아이디</th>
	<td>
		<input type="text" name="userId" id="userId" class="input-small" placeholder="Id"/>
		<form:errors path="userId" cssClass="msg_warn" />
	</td>
</tr>
<tr>
	<th>이름</th>
	<td><input type="text" name="userName" id="userName" class="input-small" placeholder="Name" /><br />
		<form:errors path="userName" cssClass="msg_warn" />	
	</td>
</tr>
<tr>
	<th>성별</th>
	<td>
		<input type="radio" id="optionsRadios1" name="userGender" value="1" checked /> 남
		<input type="radio" id="optionsRadios1" name="userGender" value="2" /> 여
	</td>
</tr>
<tr>
	<th>비밀번호</th>
	<td><input type="password" id="userPw" name="userPw" class="input-small" placeholder="Password" /><br/>
	<form:errors path="userPw" cssClass="msg_warn" />
	</td>
	
</tr>
<tr>
	<th>비밀번호 확인</th>
	<td><input type="password" id="user_re_pw" name="user_re_pw" class="input-small" placeholder="Password" />
	</td>
</tr>
<tr>
	<th>비밀번호 찾기 질문</th>
	<td><input type="text" name="userPwdHint" id="userPwdHint"/><br/>
	<form:errors path="userPwdHint" cssClass="msg_warn" />
	</td>
</tr>
<tr>
	<th>비밀번호 찾기 답</th>
	<td><input type="text" name="userPwdAnswer" id="userPwdAnswer" /><br/>
	<form:errors path="userPwdAnswer" cssClass="msg_warn" />
	</td>
</tr>
<tr>
	<th>이메일</th>
	<td>

		<input type="email" class="span3" name="userEmail" id="userEmail" required/><br/>
		<form:errors path="userEmail" cssClass="msg_warn" />
	</td>
</tr>
<tr>
	<!-- 
	<th>전화번호</th>
	<td><input type="tel" name="user_phone" id="user_phone" /><br/>
	</td>
	 -->
	<th>전화번호</th>
	<td>
		<select name="phone1" id="phone1" class="input-small">
			<option value="010">010</option>
			<option value="011">011</option>
			<option value="016">016</option>
			<option value="017">017</option>
			<option value="019">019</option>
		</select>-
		<input type="text" name="phone2" id="phone2" class="input-mini" /> -
		<input type="text" name="phone3" id="phone3" class="input-mini" />
	</td>
</tr>
<tr>
	<th>우편번호</th>
	<td>
		<input type="number"  class="input-mini" name="user_zipcode1" id="post1" size="3" readonly /> -
		<div class="input-append">
			<input type="number"  class="input-mini" name="user_zipcode2" id="post2" size="3" readonly />
			<button class="btn" type="button" onclick='openDaumPostcode()'>검색</button>
		</div>
	</td>
</tr>
<tr>
	<th>주소</th>
	<td>
		<input type="text" class="input-xlarge" name="userAddr1" id="addr" readonly />
		<form:errors path="userAddr1" cssClass="msg_warn" />
	</td>
</tr>
<tr>
	<th>상세주소</th>
	<td>
		<input type="text" class="input-xlarge" name="userAddr2" id="addr2" />
		<form:errors path="userAddr2" cssClass="msg_warn" />
	</td>
</tr>
</table>
<input type="submit" class="btn"  value="가입 완료" />
		<input type="reset" class="btn" value="다시 작성" />
		<input type="button" class="btn" value="취소" />
		<!-- 
		<button type="button" class="btn" data-loading-text="loading" >로딩</button>
		 -->
		</form:form>
		<!-- 가입폼 끝 -->
		</div>
</div>
<div class="clear"></div>
<hr />
<c:import url="/resources/base/footer.jsp"></c:import>
</body>
</html>