<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript">
	
</script>
<c:import url="/resources/base/js.jsp"></c:import>
<style type="text/css">
#form{
	margin-left: 50px;
	margin-top: 15px;
}
#btn{
	margin-left: 60px;
}
#pass{
	text-align: center;

}

</style>
</head>



<body>
	<div id="pass">
		<h3>비밀번호 찾기</h3>
	</div>
	<hr />
	<div id="form">
		<form action="pwsearch" method="POST" class="form-horizontal">
			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
			<div class="control-group">
				<label class="control-label" for="inputEmail">아이디</label>
				<div class="controls">
					<input type="text" id="Id" name="userId" placeholder="Id">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="Name">이름</label>
				<div class="controls">
					<input type="text" id="Name" name="userName" placeholder="Name">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="Email">Email</label>
				<div class="controls">
					<input type="email" id="Email" name="userEmail" placeholder="Email">
				</div>
			</div>
			<div >
				<button type="submit" id="btn" class="btn btn-primary">제출</button>
				<button type="button" class="btn btn-danger" onClick='window.close()'>닫기</button>
			</div>

		</form>
	</div>
</body>
</html>