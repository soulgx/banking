<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript">
	$(function() {
		$('#btn').on('click', function() {
			location.href= '<c:url value="/idsearch" />';
		});
	});
</script>

<style type="text/css">
#scrip {
	text-align: center;
	/* margin-left: 10px; */
	margin-top: 130px;
}

.address {
	margin-top: 10px;
	margin-left: 55px;
}

#sub {
	margin-left: 60px;
}

#return {
	margin-left: 125px;
}	

#strong {
	text-align: center;
	margin-top: 30px;
}

#ids{
	margin-top: 30px;
}
</style>

<c:import url="/resources/base/js.jsp"></c:import>
</head>
<body>

	<form action="pwcheck" method="get">
		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />

		<c:if test="${id == null }">
			<div id="strong">
				<strong>비밀번호 찾기에 실패 하셨습니다.</strong>
			</div>
			<hr>
			<address class="address">
				<br> 정확한 이름과 Email을 입력해주세요<br>문제가 발생하셨을시에<br> 아래에 있는
				메일이나 전화번호로 <br> 문의해 주시길 바랍니다. <br> <abbr title="Phone">P:</abbr>
				(123) 456-7890
			</address>

			<address class="address">
				<strong>아이디 관리자</strong><br> <a href="mailto:#">first.last@example.com</a>
			</address>

			<div id="return">
				<input type="button" value="돌아가기" id="btn" class="btn btn-danger" />
			</div>
		</c:if>



		<c:if test="${id != null }">
			<div id="ids">
			<address class="address">
				<p>귀하의 ID는<font size="5" style="font-weight: bold;" >'${id}'</font>입니다.</p>
				<br> <br> 정확한정보가 아니라면<br>
				아래에 있는 메일이나 전화번호로 <br> 문의해 주시길 바랍니다. <br> <abbr
					title="Phone">P:</abbr> (123) 456-7890
			</address>

			<address class="address">
				<strong>GoldMoon</strong><br> <a href="mailto:#">first.last@example.com</a>
			</address>
			</div>



			<div id="sub">
				<input type="submit" value="비밀번호찾기" class="btn btn-primary" /> <input
					type="button" value="돌아가기" id="btn" class="btn btn-danger" />
			</div>

		</c:if>

	</form>
</body>
</html>