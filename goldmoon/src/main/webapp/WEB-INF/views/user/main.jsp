<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!doctype html>
<html>
<head>
	<c:import url="/resources/base/js.jsp"></c:import>
	<script type="text/javascript">
	$(document).ready(function(){
		
		var popupOtp = $.cookie("popup_otp");
		

		if (popupOtp) {
			$("#topbar").css("display", "none");
			$("#navigation").css("marginTop", "15px");
		}  else {
			$("#navigation").css("marginTop", "0px");
		}
		
		window.setInterval(function(){
			$("#caroNextBtn").click();
		}, 5000);
		
		$("#popbtnColor2").on("click", function() {
			$("#topbar").slideUp(1000);
			$("#navigation").stop().animate({marginTop: "15px"}, 1000);
		});
		
		$("#popbtnColor").on("click", function() {
			$("#navigation").stop().animate({marginTop: "15px"}, 1000);
			$("#topbar").slideUp(1000, function() {
				$.cookie("popup_otp", "Y", {"expire": 1, "path": "/"});
			});
		});


		
		$("#total_btn").on("click",function(){
			$("#total_menu").stop().slideToggle("normal");
			return false;
		});
	
		$("#total_close a").on("click",function(){
			$("#total_menu").stop().slideUp("fast");
			return false;
		});
		
		$("#login").on("click", function(){
			location.href='<c:url value="/user/login" />';
		})
		
	});
	</script>
</head>
<body>

	<c:import url="/resources/base/popup.jsp"></c:import>
	<c:import url="/resources/base/nav.jsp"></c:import>
	<c:import url="/resources/base/mainBanner.jsp"></c:import>
	
	<div id="side" class="row-fluid marketing">
        <div id="sideLeft" class="span6">
		 <table class="table table-condensed">
			<thead>
          		<tr>
					<th id="noticeNo">글번호</th>
          			<th id="noticeTitle">제목</th>
                </tr>
           </thead>
           
           <tbody>
           
					<c:forEach items="${list}" var="map" varStatus="st">
						<tr>
                 			<td>${map.RN}</td>
                  			<td>
                  				<a href="board/content?bno=1&boardNo=${map.BNO}&pageNum=1">
                  					<c:if test="${fn:length(map.TITLE) > 18}">
    									<c:out value="${fn:substring(map.TITLE,0,15)}" />...                    
									</c:if>
									<c:if test="${fn:length(map.TITLE) <= 18}">
		    							${map.TITLE}
 									</c:if>
                  				</a>
                  			</td>
                		</tr>
					</c:forEach>
				
			</tbody>
		</table> 
	</div>

        <div id="sideRight" class="span6">
          <div id="topBtn">
	          <a class="btn btn-large btn-warning " href="account/paying"><i class="icon-check"></i><br /> 계좌 예금</a>
	          <a class="btn btn-large btn-warning " href="account/paying"><i class="icon-folder-open"></i><br /> 계좌 출금</a>
	          <a class="btn btn-large btn-warning " href="account/CreditTransfer"><i class="icon-retweet"></i><br /> 계좌 이체</a><br />
          </div>
          <div id="BottomBtn">
	          <a class="btn btn-large btn-warning " href="account/regist"><i class="icon-pencil"></i><br /> 계좌 생성</a>
	          <a class="btn btn-large btn-warning " href="account/accountList"><i class="icon-list"></i><br /> 계좌 조회</a>
	          <a class="btn btn-large btn-warning " href="account/choice"><i class="icon-align-justify"></i><br /> 내역 조회</a>
		  </div>        
        </div>
        
      </div>
      
      <c:import url="/resources/base/footermemu.jsp"></c:import>
      <c:import url="/resources/base/footer.jsp"></c:import>

	
</body>
</html>