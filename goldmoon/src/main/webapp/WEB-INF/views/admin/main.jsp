<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<%@ page session="true" %>    
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/bootstrap-3.3.5/css/bootstrap.min.css" />" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/bootstrap-3.3.5/css/bootstrap-theme.min.css" />" />
<script type="text/javascript" src="<c:url value="/resources/axisj/jquery/jquery.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/bootstrap-3.3.5/js/bootstrap.js" />"></script>


<script type="text/javascript" src="<c:url value="/resources/axisj/lib/AXJ.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/axisj/lib/AXGrid.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/axisj/lib/AXSelect.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/axisj/lib/AXInput.js" />"></script>


<link rel="stylesheet" type="text/css" href="<c:url value="/resources/axisj/ui/arongi/AXJ.css" />" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/axisj/ui/arongi/AXGrid.css" />" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/axisj/ui/arongi/AXInput.css" />" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/axisj/ui/arongi/AXSelect.css" />" />
<script src="//cdn.ckeditor.com/4.5.4/standard/ckeditor.js"></script>

<style type="text/css">
	
	#myTab{
		margin: 10px 0px 10px 0px;
	}
	
	.footer {
		margin-top : 30px;
		width: 100%;
		height: 100px;
		background-color: #f5f5f5;
	}
	
	
</style>

<script type="text/javascript">
	
	$(document).ready(function(){
		
		var loading_mask = new AXMask(); // 새로운 인스턴스 생성
		loading_mask.setConfig(); // 초기화
		loading_mask.setContent({ // 마스크 컨텐츠 정의
			width:200, height:200,
			html: '<div class="loading" style="color:#ffffff;">' +
			'<i class="axi axi-spinner axi-spin" style="font-size:100px;"></i>' +
			'<div>Loading..</div>' +
			'</div>'
		});
		
		
		$("a[href='#dealings']").on('click', function(){
			
			loading_mask.open();
			toast.push("데이터를 조회합니다.");
			setTimeout(function(){
				loading_mask.close();
			}, 1000);
			
            setTimeout(function(){
            	var myGrid = new AXGrid();
        		// 그리드 설정
        		
        		myGrid.setConfig({
        			targetID: "ui-grid-ajax",
        			
        			height:"380",
        			/* 컬럼 정의 */
        			colGroup: [
        				{key:"DEALINGSNO", label:"거래번호", width:"80", editor:{type:"text"}},
        				{key:"ACCOUNTNO", label:"계좌번호", width:"100", editor:{type:"text"}},
        				{key:"DOOR", label:"입출", width:"50", editor:{type:"text"}},
        				{key:"DEPOSITWITHDRAWALINFO", label:"거래구분", width:"120", editor:{type:"text"}},
        				{key:"CUSTACCOUNTNO", label:"거래처 계좌번호", width:"120", editor:{type:"text"}},
        				{key:"CUSTOMER", label:"거래처", width:"100", editor:{type:"text"}},
        				{key:"BANKINGNAME", label:"거래은행", width:"100", editor:{type:"text"}},
        				{key:"DMONEY", label:"입금액", width:"150", formatter:"money"},
        				{key:"WMONEY", label:"출금액", width:"150", formatter:"money"},
        				{key:"REMAINMONEY", label:"거래후 잔액", width:"150", formatter:"money"},
        				{key:"CONTENT", label:"거래내용", width:"200"},
        				{key:"DEALDATE", label:"거래일", width:"200", formatter:function(){
        					var date = new Date(this.item.DEALDATE);
        					var result = dateFormat(date);
        					return result;
        				}}
        			],
        			
        			foot : {
        				rows: [
        					[ 
        						{ colspan: 7, formatter: function () { return "계"; }, align: "center" },
        						{ key:"DMONEY",
        							formatter: function(){
        								var sum = 0;
        								for(var i=0, l=this.list.length;i<l;i++){
        									sum += this.list[i].DMONEY;
        								}
        								return sum.money();
        							}
        						},
        						{ key:"WMONEY",
        							formatter: function(){
        								
        								var sum = 0;
        								for(var i=0, l=this.list.length;i<l;i++){
        									sum += this.list[i].WMONEY;
        								}
        								return sum.money();
        							}
        						},
        						{ key:"REMAINMONEY",
        							formatter: function(){
        								var sum = 0;
        								for(var i=0, l=this.list.length;i<l;i++){
        									sum += this.list[i].REMAINMONEY;
        								}
        								return sum.money();
        							}
        						},
        						{ colspan: 2}
        					]
        				]
        			},
        			
        			body:{
        				onclick: function(){
        					/* toast.push( Object.toJSON(this.item) ); */
        					toast.push("거래번호 : " + this.item.DEALINGSNO + 
        							"<br/>" + "거래구분 : " + this.item.DEPOSITWITHDRAWALINFO +
        							"<br/>" + "입금액 : " + this.item.DMONEY.money() +
        							"<br/>" + "출금액 : " + this.item.WMONEY.money() +
        							"<br/>" + "잔액 : " + this.item.REMAINMONEY.money() );
        				}
        			},
        			/* 페이징 처리 유무 */
        			page:{
        				paging:true,
        				pageNo:1,
        				pageSize:100
        			}
        			
        		});
        		
        		/* AJAX 통신 처리 */
        		myGrid.setList({
        			method:"GET",
        			ajaxUrl:'<c:url value="/admin/dealingsAllList" />', 
        			ajaxPars:"", 
        			onLoad:function(){
        				
                		myGrid.setDataSet({
                			DMONEY : 0,
                			WMONEY : 0,
        	        		ACCOUNTMONEY: 0	        			
        	        	});
            			
        			}
        		});
        		
        		
            	
            	
               /*  myGrid.target.reset(); */
            }, 1500);

		});
		
		
		
		$("a[href='#userInfo']").on('click', function(){
			
			loading_mask.open();
			toast.push("데이터를 조회합니다.");
			setTimeout(function(){
				loading_mask.close();
			}, 1000);
			
			
            setTimeout(function(){
            	var userInfoGrid = new AXGrid();
        		// 그리드 설정
        		userInfoGrid.setConfig({
        			targetID: "ui-grid-ajax-user",
        			height:"380",
        			/* 컬럼 정의 */
        			colGroup: [
        				{key:"ACCOUNTNO", label:"계좌번호", width:"150"},
        				{key:"USERNAME", label:"계좌소유자", width:"200", editor:{type:"text"}},
        				{key:"ACCOUNTMONEY", label:"계좌 잔액", width:"200", formatter:"money"},
        				{key:"ACCOUNTCOUNTS", label:"거래 건수", width:"100"},        				
        				{key:"USERID", label:"아이디", width:"150"},
        				{key:"OPENDATE", label:"계좌 개설일", width:"*", formatter:function(){
        					var date = new Date(this.item.OPENDATE);
        					var result = dateFormat(date);
        					return result;
        				}}
        			],
        			foot : {
        				rows: [
        					[
        						{ colspan: 2, formatter: function () { return "계"; }, align: "center" },
        						{ key:"ACCOUNTMONEY",
        							formatter: function(){
        								var sum = 0;
        								for(var i=0, l=this.list.length;i<l;i++){
        									sum += this.list[i].ACCOUNTMONEY;
        								}
        								return sum.money();
        							}
        						},
        						{ key:"ACCOUNTCOUNTS",
        							formatter: function(){
        								var sum = 0;
        								for(var i=0, l=this.list.length;i<l;i++){
        									sum += this.list[i].ACCOUNTCOUNTS;
        								}
        								return sum.money();
        							}
        						},
        						{ colspan: 2}
        					]
        				]
        			},
        			
        			body:{
        				onclick: function(){
        					toast.push("계좌번호 : " + this.item.ACCOUNTNO + "<br />" +
        							"아이디 : " + this.item.USERID + "<br />" + 
        							"소유주 : " + this.item.USERNAME +"<br />" +
        							"계좌잔액 : " + this.item.ACCOUNTMONEY.money() +"<br />" +
        							"계좌 개설일 : " + dateFormat(new Date(this.item.OPENDATE)));
         					/* toast.push( Object.toJSON(this.item) ); */
        				}
        				
        			},
        			/* 페이징 처리 유무 */
        			page:{
        				paging:true,
        				pageNo:1,
        				pageSize:100
        			}
        			
        		});
        		
        		/* AJAX 통신 처리 */
        		userInfoGrid.setList({
        			method:"GET",
        			ajaxUrl:'<c:url value="/admin/accountAllList" />', 
        			ajaxPars:"", 
        			onLoad:function(){
        				userInfoGrid.setDataSet({
    	        			ACCOUNTMONEY: 0,
    	        			ACCOUNTCOUNTS: 0
    	        		});
        			}
        		});
        		
            	
               /*  myGrid.target.reset(); */
               
            }, 1500);
            	
		});
		
		
		 $("#user_balance, #adminPw").bind({
             "focus" : function() {
                 $(this).addClass("active");
             },
             "blur" : function() {
                 $(this).removeClass("active");
             }
         });
		 
         // 폼에 대한 submit 이벤트 처리 --> 입력값 여부 검사를 수행한다.
         $("#bankBalance").bind("submit", function() {
             /** 입력여부 검사 */
             if (!$("#user_balance").val()) {
            	 dialog.push({type:"Warning", body:"이자율을 입력하세요"});
                 $("#balance").focus();
                 return false;
             }
             if (!$("#adminPw").val()) {
            	 dialog.push({type:"Warning", body:"관리자 비밀번호를 입력하세요"});
                 $("#adminPw").focus();
                 return false;
             }
             var url = '<c:url value="/admin/procedure" />';
             

             /*** ajax 로그인처리 ***/
             var ajax = $.post(url, $(this).serialize(), function(data) {
             	console.log("success");
             }).done(function(data) {
             	var msg = data.msg;
             	dialog.push(msg);
             }).fail(function() {
            	 dialog.push({type:"Caution", body:"서버와의 통신이 실패하였습니다."});
             });
             return false;
         });
         
		 $("#emailTitle, #emailContent").bind({
             "focus" : function() {
                 $(this).addClass("active");
             },
             "blur" : function() {
                 $(this).removeClass("active");
             }
         });
		 
         // 폼에 대한 submit 이벤트 처리 --> 입력값 여부 검사를 수행한다.
         $("#emailForm").bind("submit", function() {
             /** 입력여부 검사 */
             if (!$("#emailTitle").val()) {
            	 dialog.push({type:"Warning", body:"제목을 입력하세요"});
                 $("#balance").focus();
                 return false;
             }
             if (!$("#emailContent").val()) {
            	 dialog.push({type:"Warning", body:"내용을 입력하세요"});
                 $("#adminPw").focus();
                 return false;
             }
             var url = '<c:url value="/admin/mail" />';
             

             /*** ajax 로그인처리 ***/
             var ajax = $.post(url, $(this).serialize(), function(data) {
             	console.log("success");
             }).done(function(data) {
             	var msg = data.msg;
             	dialog.push(msg);
             }).fail(function() {
            	 dialog.push({type:"Caution", body:"서버와의 통신이 실패하였습니다."});
             });
             return false;
         });

		
		
	});// ready End
	
	
	
	
	
	
	
	/* 날자형태로 포맷하기 위한 함수 */
	function dateFormat(date) {
		var yy = date.getFullYear();
		var mm = date.getMonth()+1;
		var dd = date.getDate();
		var hh = date.getHours();
		var mi = date.getMinutes();
		var ss = date.getSeconds();
		if (mm < 10) {
			mm = "0"+mm;
		}
		if (dd < 10) {
			dd = "0"+dd;
		}
		if (hh < 10) {
			hh = "0"+hh;
		}
		if (mi < 10) {
			mi = "0"+mi;
		}
		if (ss < 10) {
			ss = "0"+ss;
		}
		
		return yy + "-"+ mm + "-"+ dd + " " + hh + ":"+ mi +":" + ss;
	}
	
	
	
</script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>Insert title here</title>
</head>
<body>
<div class="container">

	<!-- 토글형 탭 메뉴 -->
	<div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
		<ul id="myTab" class="nav nav-tabs" role="tablist">
			<li role="presentation" class="active"><a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">Main</a></li>
			<li role="presentation"><a href="#userInfo" role="tab" id="userInfo-tab" data-toggle="tab" aria-controls="userInfo">사용자 계좌 정보</a></li>
			<li role="presentation"><a href="#dealings" role="tab" id="dealings-tab" data-toggle="tab" aria-controls="dealings">전체 거래 내역 정보</a></li>
			<li role="presentation"><a href="#balance" role="tab" id="balance-tab" data-toggle="tab" aria-controls="balance">이자 정산</a></li>
			<li role="presentation"><a href="#emailSubmit" role="tab" id="emailSubmit-tab" data-toggle="tab" aria-controls="emailSubmit">메일 전송</a></li>
	    </ul>
	    
	    <!-- 탭 메뉴의 컨텐츠 내용 구성 -->
		<div id="myTabContent" class="tab-content">
			<!-- 1번탭 HOME의 구성 -->
			<div role="tabpanel" class="tab-pane fade in active" id="home" aria-labelledBy="home-tab">
				<div class="jumbotron">
					<h1>관리자 페이지</h1>
					<p><a class="btn btn-primary btn-lg" href="<c:url value="/" />" role="button">은행페이지로 이동</a></p>
				</div>
			
				<div class="row marketing" id="slid">
					<div class="col-lg-6">
						<h3>사용자 계좌 정보</h3>
						<p>골드문 뱅크를 이용하고 있는 고객들의 계좌 정보를 모니터링 하는 페이지 입니다.</p>
						<p>서버단 응답 : JSON Type / 브라우져 출력 : AXISJ Guide</p>
		
						<h3>전체 거래 내역 정보</h3>
						<p>고객들의 거래 내역을 모니터링 하는 페이지 입니다. 모든 계좌 거래 정보를 가지고 옵니다.</p>
						<p>서버단 응답 : JSON Type / 브라우져 출력 : AXISJ Guide</p>
					</div>
					
					<div class="col-lg-6">
						<h3>이자 정산</h3>
						<p>골드문 뱅크를 이용하는 고객들에게 이자 정산 하기 위해 사용될 페이지 입니다.</p>
		          		<p>서버단 응답 : JSON Type / 브라우져 요청 : Ajax</p>
		          		
						<h3>메일 전송</h3>
						<p>관리자에게 제공되는 E-Mail 전송 페이지 입니다.</p>
					</div>
				</div>
			</div>
	      
			<!-- 2번 탭의 구성 계좌 거래 내역 전체 조회 -->
			<div role="tabpanel" class="tab-pane fade" id="userInfo" aria-labelledBy="userInfo-tab">
				<div class="page-header">
					<h1>전체 사용자 계좌 내역 정보</h1>
					<br />
					<p>AXISJ에서 지원하는 Guide를 이용하여 구축된 페이지</p>
					<p>서버와의 통신은 Ajax기반 이나 AXISJ에서 구현된 Ajax를 이용하여 그리드를 그려준다.</p>
					<p>서버에서의 응답은 JSON Type 이다.</p>
				</div>
				<div id="ui-grid-ajax-user"></div>
			</div>
	      
			<!-- 3번 탭 구성 -->
			<div role="tabpanel" class="tab-pane" id="dealings" aria-labelledBy="dealings-tab">
				<div class="page-header">
					<h1>골드문 뱅크 전체 거래 내역</h1>
					<br />
					<p>AXISJ에서 지원하는 Guide를 이용하여 구축된 페이지</p>
					<p>서버와의 통신은 Ajax기반 이나 AXISJ에서 구현된 Ajax를 이용하여 그리드를 그려준다.</p>
					<p>서버에서의 응답은 JSON Type 이다.</p>
				</div>
				<div id="ui-grid-ajax"></div>
			</div>
	      
			<!-- 4번 탭 구성 -->
			<div role="tabpanel" class="tab-pane fade" id="balance" aria-labelledBy="balance-tab">
				<div class="page-header">
					<h1>이자 정산 하기</h1>
					<br />
					<p>오라클 프로시저를 이용하여 고객들의 계좌에 이자 정산 시스템을 구축 한 페이지</p>
					<p>서버와의 통신은 Ajax를 이용하여 요청 및 응답한다.</p>
					<p>프로시저를 통해 데이터 UPDATE 및 INSERT 작업이 N개 이상 일어나므로 트랜잭션 처리를 하였다.</p>
					<p>이전으로 돌릴 수 없기 때문에 신중하게 작업하기 바란다.</p>
				</div>
				
				<form id="bankBalance" name="frm" class="form-horizontal">
					<sec:csrfInput/>
					<div class="form-group">
						<label for="user_balance" class="col-sm-2 control-label">이자율</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" name="user_balance" id="user_balance" placeholder="이자 %를 입력하세요.">
						</div>
					</div>
					
  					<div class="form-group">
    					<label for="adminId" class="col-sm-2 control-label">관리자 아이디</label>
    					<div class="col-sm-4">
      						<input type="text" class="form-control" name="adminId" id="adminId" placeholder="관리자 아이디를 입력하세요.">
    					</div>
  					</div>
  					
  					<div class="form-group">
    					<label for="adminPw" class="col-sm-2 control-label">관리자 암호</label>
    					<div class="col-sm-4">
      						<input type="password" class="form-control" name="adminPw" id="adminPw" placeholder="관리자 비밀번호를 입력하세요.">
    					</div>
  					</div>
  					
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<button type="submit" class="btn btn-success">반영하기</button>
						</div>
					</div>
				</form>
				
			</div>
	      
			<!-- 5번 탭 구성 -->
			<div role="tabpanel" class="tab-pane fade" id="emailSubmit" aria-labelledBy="emailSubmit-tab">
				<div class="page-header">
					<h1>회원 전체 메일 작성</h1>
					<br />
					<p>골드문 뱅크 회원들에게 메일을 보낼 수 페이지</p>
					<p>회원들의 메일주소는 데이터베이스에서 리스트로 받아오며 리스트에서 받은 메일주소로 MailService 클래스를 통해 메일 발송 처리를 한다.</p>
					<p>서버로 데이터 전달은 Ajax 통신을 이용하며 메일 발송이 N개 이상이므로 서버 컨트롤러단에서는 비동기 통신으로 처리하였다. </p>
				</div>
				
			<form name="emailForm" id="emailForm" method="post" role="form" class="form-horizontal">
				<sec:csrfInput/>
				<fieldset>
					<div class="control-group">
	            		<label class="control-label" for="emailTitle">제목</label>
	            		<div class="controls">
	              			<input type="text" class="form-control" name="emailTitle" id="emailTitle" placeholder="제목을 입력하세요">
	            		</div>
	          		</div>
					
					<br />
					
					<div class="control-group">
	            		<label class="control-label" for="emailContent">내용입력</label>
	            		<div class="controls">
	              			<textarea name="emailContent" id="emailContent" class="ckeditor"></textarea>
	            		</div>
	          		</div>
	          		
	          		<br />
	          		
					<div class="form-group">
						<div class="col-sm-offset-5 col-sm-10">
							<button type="submit" class="btn btn-primary">전송하기</button>
						</div>
					</div>
				</fieldset>
			</form>
			</div>
		</div>
	</div>
</div>

<footer class="footer">
	<div class="container">
		<p class="text-muted">Copyriget &copy; GoldMoon Bank 2015 All rights reserved. </p>
		<p class="text-muted">GoldMoon Administrator Page</p>		
	</div>
</footer>

</body>
</html>