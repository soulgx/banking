<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<html>
<head>
<c:import url="/resources/base/js.jsp"></c:import>
<script src="<c:url value="/resources/js/jquery.validate.js" />"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$("#total_btn").on("click", function() {
			$("#total_menu").slideToggle("normal");
			return false;
		});

		$("#total_close a").on("click", function() {
			$("#total_menu").slideUp("fast");
			return false;
		});

		$("#login").on("click", function() {
			location.href = '<c:url value="/user/login" />';
		});
		
	
		
		/* $("#signupForm").validate({
			rules : {
				search : {
					required : true,
					minlength : 2
				}
			},
			messages : {
				search : {
					required : "검색할 단어를 입력해 주세요.",
					minlength : "검색 단어는 최소 {0}글자 입니다."
				}
			},
			// 키보드에 의한 검사 해제
			onkeyup : false,
			// 체크박스나 라디오 버튼은 클릭시마다 검사 해제
			onclick : false,
			// 포커스가 빠져나올 경우의 검사 해제
			onfocusout : false,
			// 에러 발생시 이벤트를 가로 챔
			showErrors : function(errorMap, errorList) {
				// 에러메시지 출력
				alert(errorList[0].message);
			}
		}); */
		
	});
</script>
</head>

<body>
	<c:import url="/resources/base/nav.jsp"></c:import>
	<div id="dbody">
		<c:import url="/resources/base/leftnav.jsp"></c:import>
		<div id="container">
			
			<c:if test="${bno == 1}">
				<h2 id="transferTitle">공지사항</h2>
			</c:if>
			<c:if test="${bno == 2}">
				<h2 id="transferTitle">고객의 소리</h2>
			</c:if>
			<br />
			<sec:authorize access="hasRole('ROLE_ADMIN')">
				<form action='<c:url value="/admin/boardDelete" />' method="post">
				<sec:csrfInput/>
			</sec:authorize>
			<table id="boardList" class="table table-hover">
				<thead>
					<tr>
						<sec:authorize access="hasRole('ROLE_ADMIN')">
						<th>box</th>
						</sec:authorize>
						<th id="boardNo">No</th>
						<th id="boardTitle">제목</th>
						<th id="boardName">이름</th>
						<th id="boardDate">작성일</th>
						<th id="boardCount">조회수</th>
					</tr>
				</thead>
				<tbody>
					<c:choose>
						<c:when test="${pageInfo.totalCount == 0}">
							<tr>
								<td id="textcenter" colspan="5">게시글 없음</td>
							</tr>
						</c:when>
						<c:otherwise>
						
							<c:forEach items="${boardList}" var="map" varStatus="st">
								<tr>
									<sec:authorize access="hasRole('ROLE_ADMIN')">
									<td><input type="checkbox" name="boardNos" value="${map.BOARDNO}"/></td>
									</sec:authorize>
									
									<td class="boardlistfont">${pageInfo.number = pageInfo.number - 1 }</td>
									<td class="boardlistfont">
										<c:choose>
											<c:when test="${map.RELEVEL > 0}">
												<a href='<c:url value="/board/content?bno=${bno}&boardNo=${map.BOARDNO}&pageNum=${pageInfo.currentPage}" /><c:if test="${keep == 1}">&searchno=${searchno}&search=${search}</c:if>'>
													<img src="<c:url value="/resources/img/level.gif" />" width="${map.RELEVEL * 10 }" height="16">
													<img src="<c:url value="/resources/img/re.gif" />" /> 
														<c:if test="${map.STATE == 1}">
															[원본이 삭제된 답글]
														</c:if> ${map.TITLE}
												</a>
											</c:when>
											<c:otherwise>
													<a href='<c:url value="/board/content?bno=${bno}&boardNo=${map.BOARDNO}&pageNum=${pageInfo.currentPage}" /><c:if test="${keep == 1}">&searchno=${searchno}&search=${search}</c:if>'>${map.TITLE}</a>
											</c:otherwise>
										</c:choose></td>

									<c:set var="id" value="admin" />
									<c:choose>
										<c:when test="${map.USERID == id}">
										<td class="boardlistfont">${map.USERNAME}</td>
										</c:when>
										<c:otherwise>
										<td class="boardlistfont">${map.USERNAME}(${map.USERID})</td>
										</c:otherwise>
									</c:choose>
									
									
									<fmt:parseDate value="${map.REGDATE}" pattern="yyyy-MM-dd HH:mm" var="date">
									</fmt:parseDate>
									
									<td class="boardlistfont"><fmt:formatDate value="${date}"
											pattern="yyyy-MM-dd HH:mm"></fmt:formatDate></td>
									<td class="boardlistfont">${map.READCOUNT}</td>
								</tr>
							</c:forEach>
						</c:otherwise>
					</c:choose>

				</tbody>
			</table>
			
			<sec:authorize access="hasRole('ROLE_ADMIN')">
				<button type="submit" class="btn">삭제</button>
				</form>
			</sec:authorize>
				
				<sec:authorize access="hasRole('ROLE_ADMIN')">
					<a id="boardInsert" class="btn btn-info" href="<c:url value="/board/insert?bno=${bno}&pageNum=${pageInfo.currentPage}" />" class="btn btn-info">게시판 글작성</a>
				</sec:authorize>
				
				<sec:authorize access="hasRole('ROLE_USER')">
					<c:if test="${bno != 1 }">
						<a id="boardInsert" class="btn btn-info" href="<c:url value="/board/insert?bno=${bno}&pageNum=${pageInfo.currentPage}" />" class="btn btn-info">게시판 글작성</a>
					</c:if>
				</sec:authorize>
				
			
			<div class="clear"></div>
			
					<form name="myform" id="signupForm" method="GET">
						<input type="hidden" id="bno" name="bno" value="${bno}" /> 
						
					 	<select name="searchno" id="searchno" class="input-medium" >
							<option value="1" <c:if test="${searchno == 1}">selected="selected"</c:if> >[제목+내용]</option>
							<option value="2" <c:if test="${searchno == 2}">selected="selected"</c:if> >[아이디]</option>
						</select>
					 	
					 	
					 <div class="input-prepend">
  						<span class="add-on">검색어</span>
  						<input class="span2" id="prependedInput" type="text" placeholder="search" name="search" value="${search}">
						<button type="submit" class="btn">검색</button>
					</div>	
				

  					</form>
			
			<div class="pagination pagination-centered">
				<ul>
					<c:choose>

						<c:when test="${pageInfo.currentPage <= 1}">
							<li class="disabled"><span>&laquo;</span></li>
						</c:when>

						<c:otherwise>
							<li><a href='<c:url value="/board/list?bno=${bno}&pageNum=${pageInfo.currentPage-1}" /><c:if test="${keep == 1}">&searchno=${searchno}&search=${search}</c:if>'>&laquo;</a></li>														
						</c:otherwise>
					</c:choose>



					<c:forEach var="i" begin="${pageInfo.numberStart}" end="${pageInfo.numberEnd}">

						<c:choose>

							<c:when test="${i==pageInfo.currentPage}">
								<li class="active"><span>${i}</span></li>
							</c:when>

							<c:otherwise>
								<li><a href='<c:url value="/board/list?bno=${bno}&pageNum=${i}" /><c:if test="${keep == 1}">&searchno=${searchno}&search=${search}</c:if>'>${i}</a></li>
							</c:otherwise>

						</c:choose>

					</c:forEach>

					<c:choose>
						<c:when test="${pageInfo.currentPage >= pageInfo.totalPage}">
							<li><span>&raquo;</span></li>
						</c:when>

						<c:otherwise>
							<li><a href='<c:url value="/board/list?bno=${bno}&pageNum=${pageInfo.currentPage+1}" /><c:if test="${keep == 1}">&searchno=${searchno}&search=${search}</c:if>'>&raquo;</a></li>
						</c:otherwise>
					</c:choose>
				</ul>

			</div>
		</div>
	</div>
	<div class="clear"></div>
	<hr />
	<c:import url="/resources/base/footer.jsp"></c:import>
</body>
</html>