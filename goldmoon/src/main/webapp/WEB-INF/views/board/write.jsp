<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page session="true" %>    
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<c:import url="/resources/base/js.jsp"></c:import>

<script src="http://malsup.github.com/jquery.form.js"></script>
<script type="text/javascript" src='<c:url value="/resources/se/js/HuskyEZCreator.js" />' charset="utf-8"></script>
<script type="text/javascript" src='<c:url value="/resources/se/js/jindo.min.js" />' charset="utf-8"></script>
<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery("#total_btn").on("click",function(){
		jQuery("#total_menu").slideToggle("normal");
			return false;
		});
	
	jQuery("#total_close a").on("click",function(){
		jQuery("#total_menu").slideUp("fast");
			return false;
		});
		
	jQuery("#login").on("click", function(){
			location.href='<c:url value="/user/login" />';
		})
		
	});
	</script>
</head>
<body>
<c:import url="/resources/base/nav.jsp"></c:import>
	<div id="dbody">
	<c:import url="/resources/base/leftnav.jsp"></c:import>
	<div id="container">
	<c:if test="${bno == 1}">
				<h2 id="transferTitle">공지사항</h2>
			</c:if>
			<c:if test="${bno == 2}">
				<h2 id="transferTitle">고객의 소리</h2>
			</c:if>
			
	<form id="boardWriteForm" method="post"  >
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	<input type="hidden" id="userId" name="userId" value="${sessionScope.userId}" />
	<input type="hidden" id="bno" name="bno" value="${bno}" />
	<c:choose>
		
			<c:when test="${boardVO.boardNo == null}">
			<input type="hidden" name="boardNo" value="${article.boardNo}">
 			</c:when>
 			
 			<c:otherwise>
 			<input type="hidden" name="boardNo" value="${boardVO.boardNo}">
			<input type="hidden" name="refNo" value="${boardVO.refNo}">
			<input type="hidden" name="reStep" value="${boardVO.reStep}">
			<input type="hidden" name="reLevel" value="${boardVO.reLevel}">
 			</c:otherwise>
 	</c:choose>
	
	
	
	
		<div>
			<div class="control-group">
    			 <label class="control-label" for="title">제목</label>
   				 <div class="controls">
     			<input type="text" id="title" name="title" value="" maxlength="100" style="width: 98%" placeholder="title">
    			</div>
  			</div>
		</div>
		
		<div class="contentDiv">
		 	<textarea id="txtContent" name="content" rows="30" style="width:100%;"></textarea>
		</div>
		<div class="buttonDiv">
			<!-- 
			<button type="button" class="upload-btn">upload</button>
			<input type="file" name="upFile" class="file" />
			 -->
			 <c:choose>
			<c:when test="${boardVO.boardNo == null}">
			<button type="button" class="btn btn-primary" onclick="onUpdate()">수정</button>
 			</c:when>
 			
 			<c:otherwise>
 			<button type="button" class="btn btn-primary" onclick="onWrite()">쓰기</button>
 			</c:otherwise>
 	</c:choose>
			 
			
			 <button type="button" class="btn btn-primary" onclick="history.go(-1);"> 취소</button>
		</div>
	</form>
</div>
</div>
<div class="clear"></div>

<hr />
<c:import url="/resources/base/footer.jsp"></c:import>
</body>
<script type="text/javascript">
//<![CDATA[
           

var oEditors = [];
	nhn.husky.EZCreator.createInIFrame({
	oAppRef: oEditors,
	elPlaceHolder: document.getElementById('txtContent'), // html editor가 들어갈 textarea id 입니다.
	sSkinURI: '<c:url value="/resources/se/SmartEditor2Skin.html" />', // html editor가 skin url 입니다.
	
	fOnAppLoad: function () { 
        //수정모드를 구현할 때 사용할 부분입니다. 로딩이 끝난 후 값이 체워지게 하는 구현을 합니다.
         
         var title = '${article.title}';
         var content = '${article.content}';
         document.getElementById("title").value = title;   
         oEditors.getById["txtContent"].exec("PASTE_HTML", [content]); //로딩이 끝나면 contents를 txtContent에 넣습니다.
     },
     
     fCreator: "createSEditor2"
 });

var onWrite = function(){
	oEditors.getById["txtContent"].exec("UPDATE_CONTENTS_FIELD", []); // 에디터의 내용이 textarea에 적용합니다.
	
	 var content = document.getElementById("txtContent").value;
	 var title = document.getElementById("title").value;
	 
	 var boardNo = '${boardVO.boardNo}';
	 var reLevel = '${boardVO.reLevel}';
	 var reStep = '${boardVO.reStep}';	 
	 var refNo = '${boardVO.refNo}';
	 var userId = document.getElementById("userId").value;
	 

	var boardWriteForm = document.getElementById("boardWriteForm");  
	boardWriteForm.action ='<c:url value="/board/insert" />';			//저장할 페이지로 쏩니다.              
	boardWriteForm.submit();  
};

var onUpdate = function(){
	oEditors.getById["txtContent"].exec("UPDATE_CONTENTS_FIELD", []); // 에디터의 내용이 textarea에 적용합니다.
	
	 var content = document.getElementById("txtContent").value;
	 var title = document.getElementById("title").value;
	 
	 var userId = document.getElementById("userId").value;
	 
	 

	var boardWriteForm = document.getElementById("boardWriteForm");  
	boardWriteForm.action = '<c:url value="/board/update?bno=${bno}&pageNum=${pageNum}" />';		//저장할 페이지로 쏩니다.              
	boardWriteForm.submit();  
};

var pasteHTML = function(filename){
	var sHTML = '<img src="${pageContext.request.contextPath}/download?fileName='+filename+'">';
    
	oEditors.getById["txtContent"].exec("PASTE_HTML", [sHTML]);
};           
           

jQuery(".file").change(function(){                            //업로드할 파일을 선택 할 경우 동작을 일으킵니다.
	  var form = jQuery('#boardWriteForm');
	  form.ajaxSubmit({
	            url: "${pageContext.request.contextPath}/upload",
	            data: form.serialize(),                         //폼의 값들을 주소화하여 보내게 됩니다.
	            type: 'POST',     
	            success: function(data){
	         jQuery('.file').val('');                           //file input에 들어가 있는 값을 비워줍니다.
	                  console.log(data);                      //업로드 되었다면 결과를 콘솔에 출력해봅니다.
	            }
	  });
	 });
	 
jQuery('.file').hide();                                      //input file을 보이지 않게 합니다.
jQuery('.upload-btn').click(function(){      
	jQuery('.file').click();                            //보여지는 버튼을 누르면​ input file이 작동하게 합니다.

}); 
//]]>
</script>

</html>