<%@ page contentType = "text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<html>
<head>
<c:import url="/resources/base/js.jsp"></c:import>
<style type="text/css">


</style>
<script type="text/javascript">
	var commentList;
	var boardNo = ${boardVO.boardNo};
	var userId = '${sessionScope.userId}';
	var bno = ${bno};
	var currentPage = 1;
	var totalPage = 0;
	var numberStart = 0;
	var numberEnd = 0;
	var csrf= "${_csrf.token}";
	
	
	var prev = 0;
	var next = 0;

	$(document).ready(function(){
		$('a[href="#"]').on("click",function(){
			return ;	
		}) 
		
		$("#total_btn").on("click",function(){
			$("#total_menu").slideToggle("normal");
			return false;
		});
	
		$("#total_close a").on("click",function(){
			$("#total_menu").slideUp("fast");
			return false;
		});
		
		$("#login").on("click", function(){
			location.href='<c:url value="/user/login" />';
		})
		
		pageComment(1);
	
		$('#btnCommentOk').click(function(){
			var bcomment = $('#taComment');
			
			console.log(bcomment[0].textLength);
			
			if (bcomment[0].textLength == 0 || bcomment.val().trim() == '') {
				alert("내용을 입력하세요")
				bcomment.focus();
				return false;
			}
			
			if (bcomment[0].textLength > 100) {
				alert("댓글은 100자 이상 초과할 수 없습니다.")
				bcomment.focus();
				return false;
			}
			
			var comment_url = '<c:url value="/comment/insert" />';
			$.ajax({
				url:comment_url,
				type:'POST',
				data:{
					'userId':userId,
					'bcomment':bcomment.val(),
					'bno'	 :bno,
					'boardNo'	 :boardNo,
					'_csrf' : csrf
				},
				dataType: "json",
				success:function(data){
					currentPage = data.currentPage;
					totalPage = data.totalPage;
					prev = currentPage - 1;
					next = currentPage + 1;
					numberStart = data.numberStart;
					numberEnd = data.numberEnd;
					$('#taComment').val('');
					commentList='<table class="table table-striped">';
					$.each(data.item, setCommentList);
					commentList += '</table>';
					if (totalPage != 0) {
						setPaging();
						console.log("현재페이지:"+currentPage +", prev:"+prev+", next:"+next );				
					}
					$('#commentDisplay').html(commentList);
				}
			});
		});

	
	});
	
	function setCommentList(){
		commentList += '<tr>';					
		commentList += '<td class="commentUserName">'+ this['USERNAME'] +'<br/>('+ this['USERID']+')'+'</td>';						
		commentList += '<td class="commentText" data-cno='+this['CNO'] + '><pre class="commentinsert" data-cno='+this['CNO'] + ' >' + this['BCOMMENT']+'</pre><td/>';
		var date = new Date(this['REGDATE']);
		var result = dateFormat(date);
		commentList += '<td class="commentTime">' + result +'</td><td>';
		if (this['USERID'] == '${sessionScope.userId}') {
			var commentNo = this['CNO'];
			//var _bcomment = this['bcomment'];
			commentList += '<input type="button" class="btnclass" value="수정" ';
			commentList += 'onclick="updateEvent('+ commentNo + ');" /><br />';
			commentList += '<input type="button" class="btnclass" value="삭제" ';
			commentList += 'onclick="deleteComment('+ commentNo + ');" />';
		}
		commentList += '</td></tr>';
	}
	
	function dateFormat(date) {
		var yy = date.getFullYear();
		var mm = date.getMonth()+1;
		var dd = date.getDate();
		var hh = date.getHours();
		var mi = date.getMinutes();
		
		if (mm < 10) {
			mm = "0"+mm;
		}
		if (dd < 10) {
			dd = "0"+dd;
		}
		if (hh < 10) {
			hh = "0"+hh;
		}
		if (mi < 10) {
			mi = "0"+mi;
		}
		
		return  yy + "-"+ mm + "-"+ dd + " " + hh + ":"+ mi;
	}
	
	function setPaging() {
		commentList += '<div class="pagination pagination-centered"><ul>';
		
		if (currentPage <= 1) {
			commentList += '<li class="disabled"><span>&laquo;</span></li>';
		} else {
			commentList += '<li><a href="javascript:pageComment('+ prev + ');" >&laquo;</a></li>'
		}
		
		for (var i = numberStart; i <= numberEnd; i++) {
			//commentList += '<button data-page="comment" class="btn btn-mini" type="button" value="'+i+'">'+i+'</button>';
			if (currentPage == i) {
				commentList += '<li class="active"><span>'+i+'</span></li>';
			} else {
				commentList += '<li><a href="javascript:pageComment('+ i + ');">'+i+'</a></li>';
			}

		}
		
		if (currentPage >= totalPage) {
			commentList += '<li><span>&raquo;</span></li>';
		} else {
			commentList += '<li><a href="javascript:pageComment('+ next + ');">&raquo;</a></li>';
		}
		
		commentList += '</ul></div>';
	}
	
	function pageComment(Number) {
		var pageNumber = Number;
		$.ajax({
			url:'<c:url value="/comment/list" />',
			type:'POST',
			data:{
				'boardNo' : boardNo,
				'pageNumber' : pageNumber,
				'_csrf' : csrf
			},
			dataType: "json",
			success:function(data){
				console.log(data);
				currentPage = data.currentPage;
				totalPage = data.totalPage;
				prev = currentPage - 1;
				next = currentPage + 1;
				numberStart = data.numberStart;
				numberEnd = data.numberEnd;
				console.log("현재페이지:"+currentPage +", prev:"+prev+", next:"+next );
				
				$('#taComment').val('');
				commentList='<table class="table table-striped">';
				$.each(data.item, setCommentList);
				commentList += '</table>';
				
				
				if (totalPage != 0) {
					setPaging();
					console.log("현재페이지:"+currentPage +", prev:"+prev+", next:"+next );				
				}
				
				$('#commentDisplay').html(commentList);
			}
		});
		
	}
	
	function deleteComment(cno) {
		if (confirm('댓글을 삭제 하시겠습니까?')) {
			var bcomment = $('#taComment').val();
			var comment_url = '<c:url value="/comment/delete" />';
			
			$.ajax({
				url: comment_url,
				type:'POST',
				data: {
					'userId' : userId,
					'boardNo' : boardNo,
					'cno' : cno,
					'pageNumber' : currentPage,
					'_csrf' : csrf
				},
				dataType: "json",
				success:function(data){
					currentPage = data.currentPage;
					totalPage = data.totalPage;
					prev = currentPage - 1;
					next = currentPage + 1;
					numberStart = data.numberStart;
					numberEnd = data.numberEnd;
					$('#taComment').val('');
					commentList='<table class="table table-striped">';
					$.each(data.item, setCommentList);
					commentList += '</table>';
					
					if (totalPage != 0) {
						setPaging();
						console.log("현재페이지:"+currentPage +", prev:"+prev+", next:"+next );				
					}
					
					$('#commentDisplay').html(commentList);
				}
			});
		}
		
	}
	
	function updateEvent(cno) {
		var tdbox = $('td[data-cno="'+cno+'"]');
		var str = $('pre[data-cno="'+cno+'"]');
		
		var text = str[0].innerText;
		
		var inputbox = '<textarea rows="5" name="bcomment" id="updateCommentBody" style="width:439px; height:50px"> '+ text + ' </textarea>'
		
		$(tdbox).html(inputbox);
		var subbtn = '<input type="button" onclick="updateComment('+cno+')" value="확인" />';
			subbtn += '<input type="button" onclick="pageComment(1)" value="취소" />';
		$(tdbox).next().next().html(subbtn);
		$('.btnclass').hide();
	}
	
	
	function updateComment(cno) {
		if (confirm('댓글을 수정 하시겠습니까?')) {
			var updateCommentBody = $('#updateCommentBody');
			
			console.dir(updateCommentBody);
			
			if (updateCommentBody[0].textLength == 0 || updateCommentBody.val().trim() == '') {
				alert("내용을 입력하세요")
				updateCommentBody.focus();
				return false;
			}
			
			if (updateCommentBody[0].textLength > 100) {
				alert("댓글은 100자 이상 초과할 수 없습니다.")
				updateCommentBody.focus();
				return false;
			}
		
		
			var comment_url = '<c:url value="/comment/update" />';
			$.ajax({
				url:comment_url,
				type:'POST',
				data:{
					'userId':userId,
					'bcomment':updateCommentBody.val(),
					'cno'	 :cno,
					'bno'	 :bno,
					'boardNo'	 :boardNo,
					'pageNumber' : currentPage,
					'_csrf' : csrf
				},
				dataType: "json",
				success:function(data){
					currentPage = data.currentPage;
					totalPage = data.totalPage;
					numberStart = data.numberStart;
					numberEnd = data.numberEnd;

					prev = currentPage - 1;
					next = currentPage + 1;
					
					$('#taComment').val('');
					commentList='<table class="table table-striped">';
					$.each(data.item, setCommentList);
					commentList += '</table>';


					if (totalPage != 0) {
						setPaging();
						console.log("현재페이지:"+currentPage +", prev:"+prev+", next:"+next );				
					}
					
					$('#commentDisplay').html(commentList);
				}
			});
		}
	}
	
	
	
	
	
</script>
</head>
<sec:csrfMetaTags/>
<body> 
<c:import url="/resources/base/nav.jsp"></c:import>
	<div id="dbody">
		<c:import url="/resources/base/leftnav.jsp"></c:import>
		<div id="container">
			<c:if test="${bno == 1}">
				<h2 id="transferTitle">공지사항</h2>
			</c:if>
			<c:if test="${bno == 2}">
				<h2 id="transferTitle">고객의 소리</h2>
			</c:if>
			<br />
<table class="table">
	<tr>
		<th id="sideboard">글번호</th>
		<td>${boardVO.refNo}</td>
	</tr>
	<tr>
		<th>제목</th>
		<c:if test="${boardVO.state == 0}">
			<td>${boardVO.title}</td>
		</c:if>
		<c:if test="${boardVO.state == 1}">
			<td>[원글이 삭제된 답글] ${boardVO.title}</td>
		</c:if>
	</tr>
	<tr>
		<th>작성자</th>
		<c:set var="id" value="admin" />
		<c:choose>
			<c:when test="${boardVO.userId == id}">
			<td>관리자</td>
			</c:when>
			<c:otherwise>
			<td>${boardVO.userId}</td>
			</c:otherwise>
		</c:choose>
	</tr>
	<tr>
		<th>조회수</th>
		<td>${boardVO.readcount}</td>
	</tr>
	<tr>
		<th>작성시간</th>
		<fmt:parseDate value="${boardVO.regDate}" pattern="yyyy-MM-dd HH:mm" var="date"></fmt:parseDate>
		<td><fmt:formatDate value="${date}" pattern="yyyy-MM-dd HH:mm"></fmt:formatDate></td>
	</tr>
</table>
<pre id="boardContent">
${boardVO.content}
</pre>
<br/>
<div class="right">


<c:choose>
<c:when test="${search != null && searchno != null }">
<input type="button" value="글목록" onclick="document.location.href='<c:url value="/board/list?bno=${bno}&pageNum=${pageNum}&searchno=${searchno}&search=${search}" />'">
</c:when>
<c:otherwise>
<input type="button" value="글목록" onclick="document.location.href='<c:url value="/board/list?bno=${bno}&pageNum=${pageNum}" />'">
</c:otherwise>
</c:choose>

<c:if test="${sessionScope.userId != null && bno != 1}">
<input type="button" value="답글쓰기" onclick="document.location.href='<c:url value="/board/insert?bno=${bno}&boardNo=${boardVO.boardNo}&pageNum=${pageNum}&refNo=${boardVO.refNo}&reStep=${boardVO.reStep}&reLevel=${boardVO.reLevel}" />'">
</c:if>

<sec:authorize access="hasRole('ROLE_ADMIN')">
	<c:if test="${bno == 1 || sessionScope.userId == boardVO.userId}">
	<input type="button" value="글수정" onclick="document.location.href='<c:url value="/board/update?bno=${bno}&boardNo=${boardVO.boardNo}&pageNum=${pageNum}" />'">
	</c:if>
	<input type="button" value="글삭제" onclick="document.location.href='<c:url value="/admin/boardDeleteOne?bno=${bno}&boardNo=${boardVO.boardNo}&pageNum=${pageNum}&refNo=${boardVO.refNo}&reLevel=${boardVO.reLevel}" />'">
</sec:authorize>

<sec:authorize access="hasRole('ROLE_USER')">
<c:if test="${sessionScope.userId == boardVO.userId}">
	<input type="button" value="글수정" onclick="document.location.href='<c:url value="/board/update?bno=${bno}&boardNo=${boardVO.boardNo}&pageNum=${pageNum}" />'">
	<input type="button" value="글삭제" onclick="document.location.href='<c:url value="/board/delete?bno=${bno}&boardNo=${boardVO.boardNo}&pageNum=${pageNum}&refNo=${boardVO.refNo}&reLevel=${boardVO.reLevel}" />'">
</c:if>
</sec:authorize>

</div>
<br>
<c:if test="${sessionScope.userId != null}">
<hr />
<div id="commentWrite">
<table>
<tr>
	<td class="comment">
		${sessionScope.userId}
	</td>
	<td class="commentcontent">
		<textarea rows="5" name="bcomment" id="taComment" style="width:620px; height:50px"></textarea>
	</td>
	<td>
		<input type="button" id="btnCommentOk" value="확인" />
	</td>
</table>
</div>
</c:if>
<div id="commentDisplay"></div>


</div>
	</div>
	<div class="clear"></div>
	<hr />
	<c:import url="/resources/base/footer.jsp"></c:import>
</body>
</html>      
