<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div id=dbody>
	<c:import url="/resources/base/leftnav.jsp"></c:import>
	
	<div id="container">
		
		<div>
			<img alt="" src="<c:url value="/resources/img/goldman.png" />">
		</div>
		<div id=talent>
			<h2>World Class Employer</h2>
			<h3> 최고의 인재가 일하고 싶어하는 세계수준의 직장 </h3>		
			<hr />
				
			<dl class="dl-horizontal">
				<dt class="write">
					<img alt="logo" src="<c:url value="/resources/img/logo.png" />">
					<span class="talentedColor">성과주의 문화정립</span>
				</dt>
				<dd>-책임과 권한의 명확화</dd>
				<dd>-투명하고 공정한 평가</dd>
				<dd>-능력에 따른 보상과 성과에 따른 차별화된 보상</dd>
			</dl>	
			<hr />
						
			<dl class="dl-horizontal">
				<dt class="write">
					<img alt="logo" src="<c:url value="/resources/img/logo.png" />"> 
					<span  class="talentedColor">직원의 가치 극대화</span>
				</dt>
		
				<dd>-개인의 적성과 능력에 따라 성장기회 부여</dd>
				<dd>-지속적인 경력개발 기회 제공 및 적극적 지원</dd>
				<dd>-직원의 경쟁력 향상을 촉진하는 시장원리 확립</dd>
			</dl>	
					
			<hr />
					
			<dl class="dl-horizontal">
				<dt class="write">
					<img alt="logo" src="<c:url value="/resources/img/logo.png" />"> 
					<span  class="talentedColor">조직과 개인의 조화</span>
				</dt>
				<dd>-경쟁과 협력의 가치 동일시</dd>
				<dd>-집단성과와 개인성과의 조화와 균형</dd>
				<dd>-직원의 성장욕구와 조직의 니즈 조화</dd>
			</dl>
					
			<hr />
		
			<img alt="logo" src="<c:url value="/resources/img/logo.png" />" >
			<span>창의적인 사고와 행동으로 변화를 선도하며 고객가치를 향상시키는 프로금융인 </span>
		</div>
	</div>			
</div>		

<div class="clear"></div>		