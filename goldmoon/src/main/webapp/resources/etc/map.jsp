<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>



<style>v\:* { behavior: url(#default#VML); } 
			
</style>
<!-- prevent IE6 flickering -->

		
<script type="text/javascript">
	try {document.execCommand('BackgroundImageCache', false, true);} catch(e) {}
</script>
<script type="text/javascript" src="http://openapi.map.naver.com/openapi/naverMap.naver?ver=2.0&key=d16322b94f50b851b5f0e32c28643b3a"></script>
 <script src="<c:url value="/resources/js/jquery-1.11.3.js" />"></script>

<div id=dbody>
	<c:import url="/resources/base/leftnav.jsp"></c:import>
	<div id="container">
		<h2 id="transferTitle">영업장 위치</h2>
		<hr />
	 	<div id="positionHead">
		 	<h1 id="headH1">오시는 길</h1>
		 	<br />
		 	<h4>고객을 먼저 생각하는 은행, 골드문은행 입니다.</h4>
		 	<hr />
	 	</div>
		
		<div id ="testMap"></div>
		<hr />
		<div id=positionBody>
				
				<h4>주소</h4>
				서울특별시 종로구 돈화문로 5길 24 GoldMoonBank / T.02.538.3618 / 02.3298.6655<br><br> 
				<h4>교통정보</h4>
				<hr/>
				<span id="transport">버스</span><br>
				[지선] 0212 <br>
				[간선] 100, 101, 150, 160, 161, 262, 270, 271, 300, 370 <br> 
				[광역] 6011, 602, 1001, 9301 <br><br>
				<span id="transport">지하철</span><br>
				1호선(종로3가역)<br>
				3호선(종로3가역)<br>
				5호선(종로3가역)<br><br>
				지하철 1, 3, 5호선 종로3가역 1번,2-1번 출구에 위치<br>
			
			
			<img src="<c:url value="/resources/img/goldCompany.png" />" alt="" id=imgCom />
		</div>
	
	
	
 	</div>
  	
</div>	
<div class="clear"></div>

 <script type="text/javascript">
 var oPoint = new nhn.api.map.LatLng(37.5707821, 126.9907216);
 nhn.api.map.setDefaultPoint('LatLng');
 oMap = new nhn.api.map.Map('testMap' ,{
                         point : oPoint,
                         zoom : 13,
                         enableWheelZoom : true,
                         enableDragPan : true,
                         enableDblClickZoom : false,
                         mapMode : 0,
                         activateTrafficMap : false,
                         activateBicycleMap : false,
                         minMaxLevel : [ 1, 14 ],
                         size : new nhn.api.map.Size(700, 450)
                 });
 
 
 var oSize = new nhn.api.map.Size(28, 37);
 var oOffset = new nhn.api.map.Size(14, 37);
 var oIcon = new nhn.api.map.Icon('http://static.naver.com/maps2/icons/pin_spot2.png', oSize, oOffset);

 var oMarker = new nhn.api.map.Marker(oIcon, { title : '골드문' }); 
 oMarker.setPoint(oPoint);
 oMap.addOverlay(oMarker);
 
 // 마커라벨 표시
 var oLabel1 = new nhn.api.map.MarkerLabel(); // 마커 라벨 선언
 oMap.addOverlay(oLabel1);// 마커 라벨 지도에 추가. 기본은 라벨이 보이지 않는 상태로 추가됨
 oLabel1.setVisible(true, oMarker);// 마커 라벨 보이기 
 
                 
 </script>
