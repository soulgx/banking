<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div id=dbody>
	<c:import url="/resources/base/leftnav.jsp"></c:import>
	
	<div id="container">
	
		<h2 id="transferTitle">CEO 인사말</h2>
		<hr />
		
		<div id=imgD>
			<img src="<c:url value="/resources/img/ceo1.png" />" alt="" id=imgCeo />
		</div>
		
		<div id=hello>
			<h4>고객님과<span id="h1_0"> 행복한 미래를 함께 만들어 가는 </span> GOLDMOON BANK 의 은행장 이자성입니다.</h4>
			<hr>
			<p>현재 저희 골드문 은행은 경영 전반에서 대한민국 리딩뱅크로 인정받고 있으며, 글로벌 은행을 향해 힘차게 나아가고 있습니다.<br/><br/>
			그동안 고객님께서 보내주신 한결같은 관심과 애정 덕분입니다. 오늘의 골드문을 있게 한 고객님의 큰 사랑에 진심으로 감사드립니다.<br/><br/>
			앞으로도 저를 비롯한 모든 임직원은 항상 '<span id="h1_2">고객중심</span>'의 <span class="h1_1">핵심가치를 가슴 깊이 새기고 고객님께 친절 이상의 새롭고 차별화된 가치를
			제공하고자 열과 성을 다하겠습니다.</span> 
			그리고 책임있는 기업 시민으로서 사회적 역할도 충실히 수행하겠습니다. 이와 같이 <span class="h1_1">'미래를 함께 하는 따뜻한 금융'</span>을 적극 실천해서
			고객과 함께 성장하고 우리 사회의 건강한 발전에 기여할 수 있도록 최선을 다하겠습니다.<br><br>
			저희 골드문에 변함없는 신뢰와 성원을 부탁드리며 고객님의 가정에 언제나 건강과 행복이 가득하길 기원하겠습니다.<br><br>
			고맙습니다.</p>
			<p id="names"><img src="<c:url value="/resources/img/goldmoon.png" />" alt="" id=logo class="img-rounded"/><span>골드문은행장 이자성</span></p>
		</div>
		
		
	</div>
</div>

<div class="clear"></div>
