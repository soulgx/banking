<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div id=dbody>
	<c:import url="/resources/base/leftnav.jsp"></c:import>
	
	<div id="history">
		<h1 id="historyTitle">Gold Moon</h1>
		<p id="subTitle">- goldmoon history</p>
	
	<hr />


	
	<div class="historyRight">
		<ul>
			<li><h2>2015</h2></li>
			<li>2대 회장 이자성 취임</li>
			<li>하반기 실적상승 200%달성</li>
			<li>고객신뢰회복을 위한 차별화된 고객정보 보호 선언</li>
			<li>『GM Middle 펀드 컬렉션』론칭</li>
		</ul>	
	</div>	

	<div class="historyLeft">
		<ul>
			<li><h4>2012</h4></li>
			<li>GM국민 첫재테크예금」판매</li>
			<li>「GM가맹점우대적금」판매</li>
			<li>'樂star 대학생 장학금' 지급</li>
			<li>기업밀착형 특화점포 3곳개점</li>
		</ul>	
	</div>	
	
	<div class="historyRight">
		<ul>
			<li><h4>2007</h4></li>
			<li>「GM말하는적금」출시</li>
			<li>인도 ICICI은행과 업무협약 체결</li>
			<li>은행 발전을 위한 노사공동 선언</li>
			<li>기업고객 아웃바운드 영업채널 도입</li>
		</ul>	
	</div>	

	<div class="historyLeft">
		<ul>
			<li><h4>2004</h4></li>
			<li>국내 은행업계 1위달성</li>
			<li>대한주택보증과 업무제휴 협약</li>
			<li>「GM사회공헌위원회」출범</li>
		</ul>	
	</div>	

	<div class="historyRight">
		<ul>
			<li><h4>2000</h4></li>
			<li>자산 컨설팅팀 출범</li>
			<li>부산, 광주 지점 설립</li>
			<li>『GM창조금융적금』판매</li>
			<li>『GM마음편한통장』판매</li>
		</ul>	
	</div>	

	<div class="historyLeft">
		<ul>
			<li><h4>1998</h4></li>
			<li>goldmoon bank 설립</li>
			<li>초대 회장 석동출 선임</li>
		</ul>	
	</div>	
	
	<img id="line" src="<c:url value="/resources/img/line.png" />" alt="">
	
	</div>			
	
</div>

<div class="clear"></div>
