<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<div id="secuInfo">
<c:import url="/resources/base/leftnav.jsp"></c:import>
	
<div id="secuRight">
	<h2 id="transferTitle">보안서비스 안내</h2>
		<div class="step">
			<span class="numcolor">01</span><span>.PC보안프로그램 자동설치</span>
			<p>- 고객님의 안전한 거래를 위해 PC보안프로그램이 자동 설치됩니다.</p>
		</div>

		<hr />

		<div class="step">

			<span class="numcolor">02</span>.공인인증서 발급
		

			<p>
			- 전자금융사기 예방서비스 시행으로 인해 공인인증서 (재)발급, 타행(기관)인증서 등록 시 추가 인증
			완료 후 거래가 가능합니다.
			</p>
				
	
			<p>- 인증서는 이동식디스크에 저장하는 것이 더욱 안전합니다.</p>
				
	
			<p>- 인증서 휴대폰 저장서비스를 이용하시면 필요 시 휴대폰에 저장된 인증서를 PC로 전송하여 이용할 수
					있습니다.</p>
	</div>

	<hr />

	<div class="step">
		
			<span class="numcolor">03</span>.피싱방지 개인화 이미지 선택
		
		<p>- 현재 접속하신 홈페이지가 진짜 GOLD MOON홈페이지임을 확인할 수 있는 서비스입니다.</p>
	</div>

	<hr />

	<div class="step">
		
			<span class="numcolor">04</span>.이용PC지정서비스 등록
		
		<p>- 지정한 PC에서만 이체가능하고, 지정하지 않은 PC에서는 추가인증 완료 후 이체할 수 있습니다.</p>
	</div>
	<hr />
	<div class="step">
		
			<span class="numcolor">05</span>.보안SMS 서비스등록
		
		<div>
			
				<p>- 인터넷(모바일)뱅킹을 통한 중요변동사항 발생시 고객님의 휴대폰으로 SMS를 무료로 발송해드립니다.</p>
				<p>- 입출금내역 자동통지서비스를 신청하시면 모든 입출금내역을 SMS로 확인할 수 있습니다.</p>
		
		</div>
	</div>
	<hr />
	<div class="step">
		
			<span class="numcolor">06</span>.자금이체
		
		<div>
		
				<p>- 1일 누적 100만원 이상 이체 시 추가인증 완료 후 거래가 가능합니다. (일회용 비밀번호 생성기
					이용고객은 예외)</p>
				<p>- 인터넷뱅킹 전화승인서비스 가입하시면 이체 금액에 상관없이 ARS인증으로 본인확인 후 이체 완료됩니다.</p>
			

		</div>
	</div>
	<hr />
	<h2>보안매체 비교</h2>
	<table id="table1" class="table table-striped">
		<thead>
		<tr>
			<th id="tableSize1"></th>
			<th id="tableSize2">보안카드</th>
			<th>일회용비밀번호생성기(OTP)</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<th>정의</th>
			<td><img src="<c:url value="/resources/img/img_security_2.gif" />" alt="security" /><br>고정된
				35개 번호 중 임의의 지시번호 앞/뒤 2자리로 인증하는 보안매체</td>
			<td><img src="<c:url value="/resources/img/OTP02.gif" />" alt="otp2" /><br />전자금융 거래
				시마다 고정된 비밀번호 대신 새롭게 생성된 비밀번호로 인증하는 보안매체</td>

		</tr>
		<tr>
			<th>특징</th>
			<td><strong>휴대가 편리하나 피싱사이트에서 보안카드 정보탈취 사고의 위험성</strong>이 있고,
				대부분의 전자금융사기사고가 보안카드 이용 고객 중에서 발생하오니 각별한 주의가 필요합니다.</td>
			<td>보안카드에 고정된 비밀번호 대신 32초~1분마다 <strong>새로운 번호가 자동 생성</strong>되기
				때문에 더욱 안전합니다.
			</td>

		</tr>
		<tr>
			<th>발급비용</th>
			<td>무료</td>
			<td>5000원(개인 MVP/로얄 스타클럽 고객은 면제)</td>
		</tr>
		</tbody>
	</table>
	
</div>
</div>
<div class="clear"></div>