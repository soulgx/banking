<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<div id="secuInfo">
<c:import url="/resources/base/leftnav.jsp"></c:import>
	
	<div id="secuRight">
		<h2 id="transferTitle">보안서비스 종류</h2>
			<div class="step">
				<img src="<c:url value="/resources/img/img_security_7.gif" />"  />
				<span class="numcolor">인터넷뱅킹 전화승인서비스</span>
				<span>★★★★★(5점)</span>
				<p>- 사전에 등록된 전화번호로 이체승인이 요청되면 전화기에 승인번호(4자리)입력 후 거래하는 서비스</p>
			</div>
	
			<hr />
	
			<div class="step">
				<img src="<c:url value="/resources/img/img_security_1.gif" />">
				<span class="numcolor">일회용 비밀번호 생성기(OTP)</span>
				<span>★★★★★(5점)</span>
				<p>- 보안카드에 고정된 비밀번호 대신 32초마다 새로운 번호를 자동 생성하는 OTP 번호를 PC 또는 스마트폰에 입력</p>
			</div>
	
		<hr />
	
			<div class="step">
				<img src="<c:url value="/resources/img/img_security_8_0124.gif" />"  />
				<span class="numcolor">이용PC(폰) 지정서비스</span>
				<span>★★☆☆☆(2점)</span>
				<p>- 사전에 등록된 PC(폰)에서만 이체가 가능
				<p>- PC(폰)가 악성코드 감염으로 해킹되면 이용PC(폰)지정 서비스가 무력화될 위험이 있습니다.</p>
				<p>- 이용PC(폰) 지정서비스와 인터넷뱅킹 전화승인서비스 또는 OTP 병행 이용 권장</p>
			</div>
	
		<hr />
	
			<div class="step">
				<img src="<c:url value="/resources/img/img_security_4.gif" />" />
				<span class="numcolor">보안 SMS 서비스</span>
				<span>★★☆☆☆(2점)</span>
				<p>- 인터넷뱅킹 중요거래(공인인증서 발급, 출금거래 등) 휴대폰 SMS발송 서비스</p>
				<p>- 거래 완료 후 SMS 통지되므로 사전 불법 거래 차단을 위해서는 인터넷뱅킹 전화승인서비스 등 병행 이용 권장</p>
			</div>
			
		<hr />
		
			<div class="step">
				<img src="<c:url value="/resources/img/img_security_2.gif" />" />
				<span class="numcolor">보안카드</span>
				<span>★☆☆☆☆(1점)</span>
				<p>- 35개 번호가 각각 4자리의 숫자로 구성되어 있으며, 임의의 지시번호 앞/뒤 2자리로 인증하는 보안매체</p>
				<p>- 보안카드를 PC나 스마트폰에 파일, 사진형태로 저장할 경우 정보탈취 사고의 위험성이 있습니다.</p>
				<p>- 국민은행에서는 보안승급, 보안강화 등을 이유로 <strong id="textcolor">보안카드 번호 35개 전체 입력을 요구하지 않습니다.</strong></p>
			</div>
		
		<hr />
		
		<h5>추가 보안 강화 방법</h5>
		
			<div class="step">
				<img src="<c:url value="/resources/img/img_security_6.gif" />" />
				<span class="numcolor">피싱방지 개인화이미지</span>
				<p>- 8가지 이미지와 4가지 색상으로 직접 선택하신 이미지와 개인인식 문자로 피싱사이트를 구별</p>
			</div>
			
		<hr />	
			<div class="step">
				<img src="<c:url value="/resources/img/img_security_9.gif" />"  />
				<span class="numcolor">녹색주소창(EV SSL 인증서) 확인</span>
				<p>- KB국민은행 접속시 진짜 홈페이지임을 증명하는 녹색주소창</p>
			</div>
		<hr />	
			<div class="step">
				<img src="<c:url value="/resources/img/img_security_11.gif" />" alt="" />
				<span class="numcolor">파밍차단 보안프로그램</span>
				<p>- 실시간으로 고객PC를 감시하여 위조변조 내역을 알려주고, 기존상태로 복원할 수 있어 파밍을 사전에 예방할 수 있는 프로그램</p>
			</div>
		<hr />
			<div class="step">
				<img src="<c:url value="/resources/img/img_security_5.gif" />" alt="" />
				<span class="numcolor">인증서 휴대폰 저장서비스</span>
				<p>- 공인인증서를 휴대폰에 저장하여, 필요시 저장된 인증서를 PC로 전송하여 활용할 수 있는 서비스</p>
			</div>
			
		<hr />
			<div class="step">
				<img src="<c:url value="/resources/img/img_security03_5.gif" />" alt="" />
				<span class="numcolor">입출금내역 자동통지 서비스</span>
				<p>- 입금/출금 내역을 문자메시지 또는 팩스로 통지하는 서비스</p>
			</div>

	</div>
</div>
<div class="clear"></div>