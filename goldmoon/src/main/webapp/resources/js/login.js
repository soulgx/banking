$(document).ready(function(){
	$('#regist').bind('click',function(){		
		location.href="regist";
	});
	// 저장된 쿠키값을 읽어오기
	var c_user_id = $.cookie("user_id");
	
	// 저장된 값이 있다면?
	if (c_user_id) {
		// 입력 요소에 값을 출력해 준다.
		$("#user_id").val(c_user_id);
		// 체크박스는 체크해 준다.
		$("#setid").prop("checked", true);
	}
	
	// 체크박스의 상태가 변경된 경우.
	$("#setid").change(function() {
		if ($(this).is(":checked")) {
			//
			if (!$("#user_id").val()) {
                alert("아이디를 입력하세요.");
                $("#user_id").focus();
                $("#setid").prop("checked", false);
                return false;
        	}
			
			// 체크되었다면, 사용자에게 저장여부를 물어본다.
			if (confirm("공용 PC에서 로그인정보를 저장할 경우, 다른사람에게 노출될 위험이 있습니다. 정보를 저장하시겠습니까?")) {
				// 사용자가 승인했다면, 값을 1일간 모든 경로에서 유효하도록 저장한다.
				$.cookie("user_id", $("#user_id").val(), {"expire": 1, "path": "/"});
			}
		} else {
			// 체크가 해제되었다면 쿠키 삭제.
			$.removeCookie("user_id");
			// 입력요소의 내용은 삭제
			$("#user_id").val("");
		}
	});
});