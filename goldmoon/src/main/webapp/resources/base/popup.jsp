<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div id=topbar>

		<div id="topbar1">
			<img class="imgTopBar" src="<c:url value="/resources/img/topbarimg1.png" />" alt="" />
		</div>
		<div id="txt1">
			<h4>
				<strong class="strColor">스마트OTP</strong>한시적<strong class="strColor">무료</strong>발급
			</h4>

			<hr />

			<p>
				편리하고 보안이 강화된 스마트OTP를<br /> 9월30일까지 10만개 무료 발급합니다.
			</p>
		</div>

		<div id="topbar2">
			<img class="imgTopBar" src="<c:url value="/resources/img/topbarimg2.png" />" alt="" />
		</div>

		<div id="txt2">
			<h4>
				자동화기기<strong class="strColor">지연인출제도</strong>변경
			</h4>

			<hr />

			<p>
				9월 2일부터 계좌기준 1회 100만원 이상<br /> 입금된 경우 자동화기기를 통한 인출 및<br /> 이체가 30분간
				제한됩니다.
			</p>
		</div>

		<div id="topbar3">
			<img class="imgTopBar" src="<c:url value="/resources/img/topbarimg3.png" />" alt="" />
		</div>

		<div id="txt3">
			<h4 id="txt3Title">
				사진<strong class="strColor">촬영 - QR스캔</strong>절대금지
			</h4>

			<hr />

			<p>
				OTP 번호 / 보안카드<strong class="strColor">전체·일부(2개 초과)</strong> <br /> 요구 시 금융사기이오니 절대
				응하지 마십시오.
			</p>
		</div>
		
		<div id=close>
			<button id="popbtnColor" class="btn btn-warning" type="button"><i class="icon-chevron-down"></i>1일동안 열지 않기</button>
			<button id="popbtnColor2" class="btn btn-warning" type="button"><i class="icon-remove"></i>닫기</button>
			<!-- <input type="button" value="1일동안열지않기" name="cookie"/>
			<input type="button" value="닫기" /> -->
			</div>
			
			</div>