<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<meta charset="utf-8" />
<title>GoldMoonBank</title>
<link rel="icon" href="<c:url value="/resources/img/favicon.ico" />" type="image/x-icon" />
<link rel="shortcut icon" href="<c:url value="/resources/img/favicon.ico" />" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<c:url value="/resources/img/favicon.ico" />" />
<link rel="apple-touch-icon-precomposed" href="<c:url value="/resources/img/favicon.ico" />" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap.css" />" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap-responsive.css" />" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/main.css" />" />
<script src="<c:url value="/resources/js/jquery-1.11.3.js" />"></script>
<script src="<c:url value="/resources/js/bootstrap.js" />"></script>
<script src="<c:url value="/resources/js/jquery.cookie.js" />"></script>



	