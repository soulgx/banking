<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<div id="sidebar" class="well" >
	<ul class="nav nav-list">
		<li class="nav-header">골드문 뱅크</li>
		<li><a href="<c:url value="/company/info?no=1" />">CEO 인사말</a></li>
		<li><a href="<c:url value="/company/info?no=2" />">영업장 위치</a></li>
		<li><a href="<c:url value="/company/info?no=3" />">인재상</a></li>
		<li><a href="<c:url value="/company/info?no=4" />">연혁</a></li>
		<li class="nav-header">인터넷 뱅킹</li>
		<li><a href="<c:url value="/account/regist" />">계좌생성</a></li>
		<li><a href="<c:url value="/account/paying" />">예금/출금</a></li>
		<li><a href="<c:url value="/account/CreditTransfer" />">계좌이체</a></li>
		<li><a href="<c:url value="/account/accountList" />">계좌상세내역</a></li>
		<li><a href="<c:url value="/account/choice" />">거래내역조회</a></li>
		<li class="nav-header">보안센터</li>
		<li><a href="<c:url value="/security/info?num=1" />">보안서비스 종류</a></li>
		<li><a href="<c:url value="/security/info?num=2" />">보안서비스 안내</a></li>
		<li class="nav-header">고객센터</li>
		<li><a href="<c:url value="/board/list?bno=1" />">공지사항</a></li>
		<li><a href="<c:url value="/board/list?bno=2" />">고객의 소리</a></li>
		
		<sec:authorize access="hasAnyRole('ROLE_ADMIN', 'ROLE_USER')">
			<li><a href="<c:url value="/user/update" />">회원정보 수정</a></li>
		</sec:authorize>
			
		<sec:authorize access="hasRole('ROLE_ADMIN')">
			<li><a href="<c:url value="/admin/indexPage" />">관리자 페이지</a></li>
		</sec:authorize>
		
		
	</ul>
</div>