<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags"  prefix="sec"%>
<div id="navigation" class="navbar navbar-inverse">

	<div class="navbar-inner">
                <div class="container">
                  <a id="navBtn" class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </a>
                  <a class="brand" href="<c:url value="/" />
                  "> <img
				id="logoImg" src="<c:url value="/resources/img/logo.png" />" alt="logo" /> <span
				id="logoName">GoldMoonBank</span></a>
                  <div class="nav-collapse collapse navbar-inverse-collapse">
                    <ul class="nav">
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">골드문 뱅크</a>
                        <ul class="dropdown-menu">
	                        <li><a href="<c:url value="/company/info?no=1" />">CEO 인사말</a></li>
							<li><a href="<c:url value="/company/info?no=2" />">영업장 위치</a></li>
							<li class="divider"></li>
							<li><a href="<c:url value="/company/info?no=3" />">인재상</a></li>
							<li><a href="<c:url value="/company/info?no=4" />">연혁</a></li>
                        </ul>
                      </li>
                    </ul>
                    
                    <ul class="nav">
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">인터넷 뱅킹</a>
                        <ul class="dropdown-menu">
                        	<li><a href="<c:url value="/account/regist" />">계좌 생성</a></li>
							<li><a href="<c:url value="/account/paying" />">예금/출금</a></li>
							<li><a href="<c:url value="/account/CreditTransfer" />">계좌 이체</a></li>
							<li class="divider"></li>
							<li class="nav-header">내역</li>
							<li><a href="<c:url value="/account/accountList" />">계좌 상세 내역</a></li>
							<li><a href="<c:url value="/account/choice" />">거래 내역 조회</a></li>
                        </ul>
                      </li>
                    </ul>
                    
                    <ul class="nav">
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">보안센터</a>
                        <ul class="dropdown-menu">
                        	<li><a href="<c:url value="/security/info?num=1" />">보안서비스 종류</a></li>
							<li><a href="<c:url value="/security/info?num=2" />">보안서비스 안내</a></li>
                        </ul>
                      </li>
                    </ul>
                    
                    <ul class="nav">
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">고객센터</a>
                        <ul class="dropdown-menu">
                        	<li><a href="<c:url value="/board/list?bno=1" />">공지사항</a></li>
							<li><a href="<c:url value="/board/list?bno=2" />">고객의 소리</a></li>
							
							<%-- <c:if test="${sessionScope.userInfo.getUsername() != null}">
								<li><a href="<c:url value="/user/update" />">회원정보 수정</a></li>
							</c:if> --%>
							<sec:authorize access="hasAnyRole('ROLE_ADMIN', 'ROLE_USER')">
								<li><a href="<c:url value="/user/update" />">회원정보 수정</a></li>
							</sec:authorize>
							<sec:authorize access="hasRole('ROLE_ADMIN')">
								<li><a href="<c:url value="/admin/indexPage" />">관리자 페이지</a></li>
							</sec:authorize>
                        </ul>
                      </li>
                      <li><a href="#" id="total_btn">전체 메뉴 보기</a>
                    </ul>
                    
                    
                   
                    <ul class="nav pull-right">
                      <sec:authorize access="hasRole('ROLE_ANONYMOUS')">
							<li><button id="login" class="btn btn-inverse" type="button">로그인</button></li>
						</sec:authorize>
						<sec:authorize access="hasAnyRole('ROLE_ADMIN', 'ROLE_USER')">
							<li>
								<form action='<c:url value="/logout" />' method="post">
								<sec:csrfInput/>
								<button id="logout" class="btn btn-inverse" type="submit">로그아웃</button>
								</form>	
							</li>
						</sec:authorize>
                    </ul>
                  </div><!-- /.nav-collapse -->
                </div>
              </div><!-- /navbar-inner -->
</div>
<!-- /navbar -->


<div id="total_menu" class="img-rounded">

	<div>
		<ul>
			<li class="menu_title">골드문 뱅크</li>
			<li><a href="<c:url value="/company/info?no=1" />">CEO 인사말</a></li>
			<li><a href="<c:url value="/company/info?no=2" />">영업장 위치</a></li>
			<li><a href="<c:url value="/company/info?no=3" />">인재상</a></li>
			<li><a href="<c:url value="/company/info?no=4" />">연혁</a></li>
		</ul>
	</div>


	<div>
		<ul>
			<li class="menu_title">인터넷 뱅킹</li>
			<li><a href="<c:url value="/account/regist" />">계좌생성</a></li>
			<li><a href="<c:url value="/account/paying" />">예금/출금</a></li>
			<li><a href="<c:url value="/account/CreditTransfer" />">계좌이체</a></li>
			<li><a href="<c:url value="/account/accountList" />">계좌 상세 내역</a></li>
			<li><a href="<c:url value="/account/choice" />">거래 내역 조회</a></li>
		</ul>
	</div>

	<div>
		<ul>
			<li class="menu_title">보안센터</li>
			<li><a href="<c:url value="/security/info?num=1" />">보안서비스 종류</a></li>
			<li><a href="<c:url value="/security/info?num=2" />">보안서비스 안내</a></li>
		</ul>
	</div>

	<div>
		<ul>
			<li class="menu_title">고객센터</li>
			<li><a href="<c:url value="/board/list?bno=1" />">공지사항</a></li>
			<li><a href="<c:url value="/board/list?bno=2" />">고객의소리</a></li>
			
			<sec:authorize access="hasAnyRole('ROLE_ADMIN', 'ROLE_USER')">
				<li><a href="<c:url value="/user/update" />">회원정보 수정</a></li>
			</sec:authorize>
			
			<sec:authorize access="hasRole('ROLE_ADMIN')">
				<li><a href="<c:url value="/admin/indexPage" />">관리자 페이지</a></li>
			</sec:authorize>
			
		</ul>
	</div>

	<p id="total_close">
		<a class="btn btn-warning" href="#">닫기</a>
	</p>
</div>