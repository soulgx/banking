<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div id="myCarousel" class="carousel slide">
	<ol class="carousel-indicators">
		<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
   		<li data-target="#myCarousel" data-slide-to="1"></li>
   		<li data-target="#myCarousel" data-slide-to="2"></li>
   		<li data-target="#myCarousel" data-slide-to="3"></li>
	</ol>
	<!-- Carousel items -->
	<div class="carousel-inner">
		<div class="active item">
			<img src="<c:url value="/resources/img/banner01.jpg" />" alt="">
			<div class="carousel-caption">
				<h4>금융권 최초! GM 스마트 주택청약</h4>
				<p>이제 모바일로 언제 어디서나 내집 마련의 꿈이 이루어 집니다.</p>
			</div>
		</div>


		<div class="item">
			<img src="<c:url value="/resources/img/banner02.jpg" />" alt="">
			<div class="carousel-caption">
				<h4>e-파워 정기 예금</h4>
				<p>온라인 전용 대표 정기 예금, GM Star 통장/ GM 국군 장병 우대 통장있으면 우대이율 연 0.3%!!</p>
			</div>
		</div>
		
		<div class="item">
			<img src="<c:url value="/resources/img/banner04.jpg" />" alt="">
			<div class="carousel-caption">
				<h4>GM 평생 한가족 통장</h4>
				<p>급여이체, 자동이체, 대출보유 등 당행 주거래 고객에게 우대 금리 제공</p>
			</div>
		</div>
		
		<div class="item">
			<img src="<c:url value="/resources/img/banner03.jpg" />" alt="">
			<div class="carousel-caption">
				<h4>GM 네트워크 환전 서비스</h4>
				<p> GoldMoon은행 인터넷(GM스타)뱅킹을 통해 환전 신청 후 영업점을 방문하여 
				외화를 수령하는 서비스 </p>
			</div>
		</div>
		
		

	</div>
	<!-- Carousel nav -->
	<a class="carousel-control left" href="#myCarousel" data-slide="prev">
		&lsaquo;</a> <a id="caroNextBtn" class="carousel-control right" href="#myCarousel"
		data-slide="next"> &rsaquo;</a>
</div>